# ELL Game #

A mobile game facilitating adaptive learning, for pre-school kids to teach letter writing, reading, speaking, listening. The game incorporated unprecedented testing techniques such as written letter recognition(neural network trained model) and speech recognition(google api) on mobile phone(offline), and recommends tasks to help the user learn letters.

### Motivation ###

Induction of English letters into a child's development using personalized training and recommendations.

### Main Files ###
```
/www/src
```