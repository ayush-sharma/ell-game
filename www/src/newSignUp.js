newSignUp = function(game, loginTypeInput) {
    console.log('newSignUp called');
    new_signUp = function() {};
    loginResult = 0;
    timeOut = 0;

    loginType = loginTypeInput;

    new_signUp.prototype = {

        preload: function() {

            game.load.image('enterButton', 'asset/Log In/enterButton.png');
            game.load.image('textInput', 'asset/Log In/textBox.png');
            game.load.image('loginEgg', 'asset/Log In/loginEgg.png');
            game.load.image('back', 'asset/crumpled_white_paper.jpg');
            game.load.image('static', 'asset/Static.png');
            game.load.spritesheet('loading', 'asset/loading.png', 50, 50, 8);
        },

        create: function() {

            reduceAlphaGroup = game.add.group();

            back = game.add.sprite(w / 2, h / 2, 'back');
            back.anchor.setTo(0.5, 0.5);
            back.width = w;
            back.height = h;
            reduceAlphaGroup.add(back);

            loginEgg = game.add.sprite(w / 4, h / 2, 'loginEgg');
            loginEgg.anchor.set(0.5);
            loginEgg.scale.set(0.3 * w / loginEgg.width, 0.78 * h / loginEgg.height);
            reduceAlphaGroup.add(loginEgg);

            usernameBox = game.add.sprite((5 * w / 8), h / 4, 'textInput');
            usernameBox.anchor.set(0.5);
            usernameBox.scale.setTo((0.4 * w) / usernameBox.width, (0.4 * w) / usernameBox.width);
            reduceAlphaGroup.add(usernameBox);

            ageBox = game.add.sprite((5 * w / 8), usernameBox.y + usernameBox.height * (1.5), 'textInput');
            ageBox.anchor.set(0.5);
            ageBox.scale.setTo((0.4 * w) / ageBox.width, (0.4 * w) / ageBox.width);
            reduceAlphaGroup.add(ageBox);

            pageTitle = game.add.text((5 * w / 8), h / 8, "Login", {
                font: (usernameBox.height).toString() + "px Arial",
                fill: "#0000FF",
                // wordWrap: true,
                align: "center"
            });
            if (loginType != "logIn") {
                pageTitle.text = "Sign Up";
            }
            pageTitle.anchor.set(0.5);
            reduceAlphaGroup.add(pageTitle);

            loading = game.add.sprite(w / 2, h / 2, 'loading');
            loading.animations.add('walk');
            loading.animations.play('walk', 20, true);
            loading.visible = false;

            submitBox = game.add.sprite((5 * w / 8), (3 * h / 4), 'enterButton');
            submitBox.anchor.set(0.5);
            submitBox.scale.setTo((0.4 * w) / submitBox.width, (0.4 * w) / submitBox.width);
            reduceAlphaGroup.add(submitBox);


            submitBoxText = game.add.text(submitBox.x, submitBox.y, "Login", {
                font: (submitBox.height - 10).toString() + "px Arial",
                fill: "#FFFFFF",
                // wordWrap: true,
                align: "center"
            });
            if (loginType != "logIn") {
                submitBoxText.text = "Sign Up";
            }
            submitBoxText.anchor.set(0.5);
            reduceAlphaGroup.add(submitBoxText);


            changeNavigation = game.add.text(submitBox.x - (submitBox.width / 2), submitBox.y + (3 * submitBox.height / 4), "New User ?", {
                font: (0.5 * submitBox.height).toString() + "px Arial",
                fill: "#000000",
                // wordWrap: true,
                align: "center"
            });
            if (loginType != "logIn") {
                changeNavigation.text = "Existing User ?";
            }
            reduceAlphaGroup.add(changeNavigation);

            changeNavigationButton = game.add.text(changeNavigation.x + (changeNavigation.width), submitBox.y + (3 * submitBox.height / 4), " SignUp", {
                font: (0.65 * submitBox.height).toString() + "px Arial",
                fill: "#0000FF",
                // wordWrap: true,
                align: "center"
            });
            changeNavigationButton.y = changeNavigationButton.y - ((changeNavigationButton.height - changeNavigation.height) / 2);
            if (loginType != "logIn") {
                changeNavigationButton.text = " Login"
            }
            reduceAlphaGroup.add(changeNavigationButton);

            changeNavigationButton.inputEnabled = true;
            changeNavigationButton.events.onInputDown.add(function() {
                if (loginType == "logIn") {
                    newSignUp(game, "signUp");
                } else {
                    newSignUp(game, "logIn")
                }
                game.state.start("newSignUp", true, true);
            }, this);
            reduceAlphaGroup.add(changeNavigationButton);




            if (document.getElementById("username") == null) {
                usernameInput = document.createElement("input");
                usernameInput.id = "username"
                usernameInput.type = "text";
                usernameInput.placeholder = "Name";
                usernameInput.style.cssText = "position:absolute; left:-1px; top: -1px; width:1px; height:1px; opacity:0";
                document.body.appendChild(usernameInput);
            }
            usernameInput.value = "";

            if (document.getElementById("age") == null) {
                ageInput = document.createElement("input");
                ageInput.id = "age";
                ageInput.type = "number";
                ageInput.placeholder = "Age";
                ageInput.style.cssText = "position:absolute; left:-1px; top: -1px; width:1px; height:1px; opacity:0";
                document.body.appendChild(ageInput);
            }
            ageInput.value = "";

            usernameBox.inputEnabled = true;
            usernameBox.events.onInputDown.add(function() {
                usernameInput.focus();
            }, this);

            ageBox.inputEnabled = true;
            ageBox.events.onInputDown.add(function() {
                ageInput.focus();
            }, this);

            back.inputEnabled = true;
            back.events.onInputDown.add(function() {
                usernameInput.blur();
                ageInput.blur();
            }, this);

            usernameText = game.add.text(usernameBox.x - (usernameBox.width / 2), usernameBox.y - (usernameBox.height / 2), "", {
                font: (usernameBox.height - 10).toString() + "px Arial",
                fill: "#000000",
                wordWrap: true,
                align: "center"
            });
            reduceAlphaGroup.add(usernameText);

            ageText = game.add.text(ageBox.x - (ageBox.width / 2), ageBox.y - (ageBox.height / 2), "", {
                font: (ageBox.height - 10).toString() + "px Arial",
                fill: "#000000",
                wordWrap: true,
                align: "center"
            });
            reduceAlphaGroup.add(ageText);
            // divide by 15
            // loginResultText = game.add.text(w / 2, h / 2, "", {
            //     font: "32px Arial",
            //     fill: "#ff0044",
            //     wordWrap: true,
            //     align: "center"
            // });

            submitBox.inputEnabled = true;
            submitBox.events.onInputDown.add(function() {
                reduceAlphaGroup.alpha = 0.3;
                timeOut = Date.now()
                signUpEvent();
            }, this);
            reduceAlphaGroup.add(submitBox);

            // zayaLogo = game.add.sprite(w / 2, h / 2, 'static');
            // zayaLogo.anchor.set(0.5);
            // zayaLogo.width = w;
            // zayaLogo.height = h;
            // zayaLogo.bringToTop();

            // game.time.events.add(3000, function() {
            //     zayaLogo.destroy();
            //     usernameBox.inputEnabled = true;
            //     ageBox.inputEnabled = true;
            //     back.inputEnabled = true;
            //     submitBox.inputEnabled = true;
            //     changeNavigationButton.inputEnabled = true;
            // }, this);

        },

        update: function() {

            if (document.getElementById("username").value == "") {
                usernameText.text = document.getElementById("username").placeholder;
                usernameText.fill = "#999999";
            } else {
                usernameText.text = document.getElementById("username").value;
                usernameText.fill = "#000000";
            }

            if (document.getElementById("age").value == "") {
                ageText.text = document.getElementById("age").placeholder;
                ageText.fill = "#999999";
            } else {
                ageText.text = document.getElementById("age").value;
                ageText.fill = "#000000";
            }

            if (timeOut != 0) {
                var newTime = Date.now();
                var difference = newTime - timeOut;
                if (difference > 10000) {
                    alert('Could not login. Making a guest Session');
                    var userDataGeneratedLocally = createNewUserDataManually(document.getElementById("username").value);
                    saveUserData(userDataGeneratedLocally);
                    Menu(game);
                    game.state.start('Menu');
                }
            }







            if (loginResult != 0) {
                loading.visible = false;
                gotResult();
            }
        }
    }

    game.state.add("newSignUp", new_signUp);
}


function signUpEvent() {
    // loading = game.add.sprite(w / 2, h / 2, 'submit');
    // console.log('**********************');
    loading.visible = true;
    // console.log('##################################');
    usernameInput.blur();
    ageInput.blur();
    var username = document.getElementById("username").value;
    var age = document.getElementById("age").value;

    validateUser(true, loginType, username, age);
    // console.log('res val', res);

}

function clearText(game) {
    usernameBox.destroy;
    ageBox.destroy;
    submitBox.destroy;
    usernameText.destroy;
    ageText.destroy;
}

// createNewUserDataManually = function(username) {
//     var user = {};
//     user.data = {};

//     user.data.guest = 1;
//     user.data.username = username;
//     user.data.threshholdMulti = {
//         "puzzle": 5,
//         "gameSelect": [5, 2, 2, 2],
//         "alphabetAnimation": 5,
//         "alphabetTraceRef": 5,
//         "alphabetTrace": 5
//     };
//     user.data.letterStates = {};
//     // for (var key = 0; key < 26; key++) {
//     //     user.data.letterStates[key] = {
//     //         "speak": 0,
//     //         "read": 0,
//     //         "listen": 0,
//     //         "write": 0
//     //     };
//     // }

//     var letterVSCount = {};
//     for (var i = 0; i < 25; i++) {
//         var chr = String.fromCharCode(65 + i);
//         letterVSCount[chr] = 0;
//     }

//     user.data.letterStates["speak"] = JSON.parse(JSON.stringify(letterVSCount));
//     user.data.letterStates["read"] = JSON.parse(JSON.stringify(letterVSCount));
//     user.data.letterStates["listen"] = JSON.parse(JSON.stringify(letterVSCount));
//     user.data.letterStates["write"] = JSON.parse(JSON.stringify(letterVSCount));

//     // for (var i = 0; i < GameStates.length; i++) {
//     //     user.data.letterStates[GameStates[i]] = Array.apply(null, Array(26)).map(Number.prototype.valueOf, 0);
//     // }
//     user.data.difficulty = {};
//     user.data.difficulty["speak"] = 0;
//     user.data.difficulty["read"] = 0;
//     user.data.difficulty["listen"] = 0;
//     user.data.difficulty["write"] = 0;

//     user.data.wrong = {};
//     user.data.correct = {};
//     user.data.wrong["speak"] = [];
//     user.data.wrong["read"] = [];
//     user.data.wrong["listen"] = [];
//     user.data.correct["listen"] = {};
//     user.data.wrong["write"] = [];

//     user.data.menu = 1;

//     return user;
// }


function createNewUserDataManually(username) {
    user = {};
    user.data = {};

    user.data.username = username;
    user.data.threshholdMulti = {
        // "puzzle": 5,
        // "gameSelect": [4, 2, 2, 2],
        // "alphabetAnimation": 5,
        // "alphabetTraceRef": 5,
        // "alphabetTrace": 5,
        // "alphabetTracingTraining": 5,
        // "alphabetTracingTest": 5
        "puzzle": (menuVSLetters[1][1] - menuVSLetters[1][0]) + 1,
        "gameSelect": [(menuVSLetters[1][1] - menuVSLetters[1][0]) + 1, 2, 2, 2],
        "alphabetAnimation": (menuVSLetters[1][1] - menuVSLetters[1][0]) + 1,
        "alphabetTraceRef": (menuVSLetters[1][1] - menuVSLetters[1][0]) + 1,
        "alphabetTrace": (menuVSLetters[1][1] - menuVSLetters[1][0]) + 1,
        "alphabetTracingTraining": (menuVSLetters[1][1] - menuVSLetters[1][0]) + 1,
        "alphabetTracingTest": (menuVSLetters[1][1] - menuVSLetters[1][0]) + 1
    };
    user.data.letterStates = {};

    var letterVSCount = {};
    for (var i = 0; i < 25; i++) {
        var chr = String.fromCharCode(65 + i);
        letterVSCount[chr] = 0;
    }

    user.data.letterStates["speak"] = JSON.parse(JSON.stringify(letterVSCount));
    user.data.letterStates["read"] = JSON.parse(JSON.stringify(letterVSCount));
    user.data.letterStates["listen"] = JSON.parse(JSON.stringify(letterVSCount));
    user.data.letterStates["write"] = JSON.parse(JSON.stringify(letterVSCount));

    user.data.gameCounters = {};
    user.data.gameCounters[1] = {};
    user.data.gameCounters[2] = {};
    user.data.gameCounters[3] = {};
    user.data.gameCounters[4] = {};

    user.data.difficulty = {};
    user.data.difficulty[1] = {};
    user.data.difficulty[2] = {};
    user.data.difficulty[3] = {};
    user.data.difficulty[4] = {};

    user.data.wrong = {};
    user.data.wrong[1] = {};
    user.data.wrong[2] = {};
    user.data.wrong[3] = {};
    user.data.wrong[4] = {};


    user.data.correct = {};
    user.data.correct[1] = {};
    user.data.correct[2] = {};
    user.data.correct[3] = {};
    user.data.correct[4] = {};

    user.data.skillBasedUnlocked = {
        'micSkill': 0,
        'writeSkill': 0
    };

    resetMenuParams(1);
    resetMenuParams(2);
    resetMenuParams(3);
    resetMenuParams(4);

    // user.data.gameCounters[num].GameEgg = 0;
    // user.data.gameCounters[num].GameSelect = 0;
    // user.data.gameCounters[num].alphabetTracingTraining = 0;
    // user.data.gameCounters[num].alphabetTracingTest = 0;


    // user.data.difficulty[num]["speak"] = 0;
    // user.data.difficulty[num]["read"] = 0;
    // user.data.difficulty[num]["listen"] = 0;
    // user.data.difficulty[num]["write"] = 0;


    // user.data.wrong[num]["speak"] = [];
    // user.data.wrong[num]["read"] = [];
    // user.data.wrong[num]["listen"] = [];
    // user.data.correct[num]["read"] = {};
    // user.data.wrong[num]["write"] = [];

    user.data.flow = {};
    user.data.flow[1] = ["GameEgg", "GameSelect", "alphabetTracingTraining", "alphabetTracingTest"];
    // user.data.flow[1] = ["alphabetTracingTraining", "alphabetTracingTest"];
    user.data.flow[2] = ["GameEgg", "GameSelect", "alphabetTracingTraining", "alphabetTracingTest"];
    user.data.flow[3] = ["GameEgg", "GameSelect", "alphabetTracingTraining", "alphabetTracingTest"];
    user.data.flow[4] = ["GameEgg", "GameSelect", "alphabetTracingTraining", "alphabetTracingTest"];
    // user.data.menu = 1;
    user.data.menuGroupClicked = null;

    user.data.maxLevel = 1;

    return user;
}



function gotResult() {
    var res = loginResult;
    console.log('::::::::::::::::::::::::::::::::::::::::::::; ', loginResult);
    loginResult = 0;
    loading.visible = false;
    reduceAlphaGroup.alpha = 1;

    if (res == "validation failed") {
        console.log('validation failed');
        clearText(game);
        // loginResultText.text = "validation failed";
        alert("validation failed");
    } else {

        try {
            var json = JSON.parse(res);
        } catch (e) {
            alert('Could not login. Making a guest Session');
            var userDataGeneratedLocally = createNewUserDataManually(document.getElementById("username").value);
            saveUserData(userDataGeneratedLocally);
            Menu(game);
            game.state.start('Menu');
            return 0;
        }

        if (loginType != "logIn") {
            console.log('in SignUp');
            if (res == "0") {
                console.log('user already exists');
                clearText(game);
                // loginResultText.text = "user already exists";
                alert("user already exists");
            } else if (res == "") {
                clearText(game);
                // loginResultText.text = "error while signing in";
                // alert("error while signing in");
                alert("not connected to internet");
            } else if (res == "Unexpected Error") {
                clearText(game);
                // loginResultText.text = "Unexpected Error.. Try Again !!";
                alert("Unexpected Error.. Try Again !!");
            } else {
                console.log('logged in successfully');
                clearText(game);
                // loginResultText.text = "logged in successfully";
                saveUserData(res);
                Menu(game);
                game.state.start("Menu");
            }

        } else {
            console.log('in login');
            if (res == "0") {
                console.log('invalid age');
                alert("invalid age");
            } else if (res == "-1") {
                console.log('username does not exist');
                clearText(game);
                // loginResultText.text = "username does not exist";
                alert("username does not exist");
            } else if (res == "") {
                clearText(game);
                // loginResultText.text = "error while signing in";
                // alert("error while signing in");
                alert("not connected to internet");
            } else if (res == "Unexpected Error") {
                clearText(game);
                // loginResultText.text = "Unexpected Error.. Try Again !!";
                alert("Unexpected Error.. Try Again !!");
            } else {
                console.log('logged in successfully');
                clearText(game);
                // loginResultText.text = "logged in successfully";
                saveUserData(res);
                Menu(game);
                game.state.start("Menu");
            }
        }
    }
    document.getElementById("username").value = '';
    document.getElementById("age").value = '';
}











// function gotResult() {
//     var res = loginResult;
//     console.log('::::::::::::::::::::::::::::::::::::::::::::; ', loginResult);
//     loginResult = 0;
//     loading.visible = false;
//     reduceAlphaGroup.alpha = 1;

//     try {
//         var json = JSON.parse(res);
//     } catch (e) {
//         alert('Could not login. Making a guest Session');
//         var userDataGeneratedLocally = createNewUserDataManually(document.getElementById("username").value);
//         saveUserData(userDataGeneratedLocally);
//         Menu(game);
//         game.state.start('Menu');
//         return 0;
//     }


//     if (res == "validation failed") {
//         console.log('validation failed');
//         clearText(game);
//         // loginResultText.text = "validation failed";
//         alert("validation failed");
//     } else {

//         if (loginType != "logIn") {
//             console.log('in SignUp');
//             if (res == "0") {
//                 console.log('user already exists');
//                 clearText(game);
//                 // loginResultText.text = "user already exists";
//                 alert("user already exists");
//             } else if (res == "") {
//                 clearText(game);
//                 // loginResultText.text = "error while signing in";
//                 // alert("error while signing in");
//                 alert("not connected to internet");
//             } else if (res == "Unexpected Error") {
//                 clearText(game);
//                 // loginResultText.text = "Unexpected Error.. Try Again !!";
//                 alert("Unexpected Error.. Try Again !!");
//             } else {
//                 console.log('logged in successfully');
//                 clearText(game);
//                 // loginResultText.text = "logged in successfully";
//                 saveUserData(res);
//                 Menu(game);
//                 game.state.start("Menu");
//             }

//         } else {
//             console.log('in login');
//             if (res == "0") {
//                 console.log('invalid age');
//                 alert("invalid age");
//             } else if (res == "-1") {
//                 console.log('username does not exist');
//                 clearText(game);
//                 // loginResultText.text = "username does not exist";
//                 alert("username does not exist");
//             } else if (res == "") {
//                 clearText(game);
//                 // loginResultText.text = "error while signing in";
//                 // alert("error while signing in");
//                 alert("not connected to internet");
//             } else if (res == "Unexpected Error") {
//                 clearText(game);
//                 // loginResultText.text = "Unexpected Error.. Try Again !!";
//                 alert("Unexpected Error.. Try Again !!");
//             } else {
//                 console.log('logged in successfully');
//                 clearText(game);
//                 // loginResultText.text = "logged in successfully";
//                 saveUserData(res);
//                 Menu(game);
//                 game.state.start("Menu");
//             }
//         }
//     }
//     document.getElementById("username").value = '';
//     document.getElementById("age").value = '';
// }
