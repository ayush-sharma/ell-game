selectObjects = function(game, level) {
    var options = [];
    var n = level.length;
    var rows = Math.ceil(n / 2);
    var columns = Math.ceil(n / rows);
    var randomLevel = shuffle(level);
    var c = 0;

    var innerWidth = w - (backButton.width + backButton.x);
    var innerHeight = h - (backButton.height + backButton.y);

    for (var i = 0; i < rows; i++) {
        for (var j = 0; j < columns; j++) {
            var temp = {};
            temp.x = ((backButton.width + backButton.x)/2) + (j * innerWidth / columns);
            temp.y = ((backButton.height + backButton.y)/2) + (i * innerHeight / rows);
            temp.letter = level[c];
            temp.w = innerWidth / columns;
            temp.h = innerHeight / rows;
            temp.size = 0.7 * innerHeight / rows;
            temp.color = "green";
            c++;
            options.push(temp);
        };
    }
    return options;
}
