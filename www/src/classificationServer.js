GameStates = ["Menu", "GameSelect",  "GameEgg"];
if (Meteor.isServer) {

    WebApp.connectHandlers.use(function(req, res, next) {
        res.setHeader("Access-Control-Allow-Origin", "*");
        return next();
    });

    Meteor.methods({
        initialiseProgeression: function() {
            for (var i = 0; i < GameStates.length; i++) {
                var temp = [];
                for (var j = 0; j < GameStates.length; j++) {
                    if (i !== j) {
                        temp.push({
                            otherState: GameStates[j],
                            success: 0
                        });
                    }

                };

                Progression.insert({
                    state: GameStates[i],
                    pre: temp
                });
            };
        }

    });
    Meteor.startup(function() {
        Transactions.remove({});
        // Progression.insert({pre:{}})
        Progression.remove({});

        if (!Progression.findOne()) {
            Meteor.call('initialiseProgeression');

        }
    });
}
