if (Meteor.isClient) {
    Template.hello.rendered = function() {
        w = window.innerWidth;
        h = window.innerHeight;
        var game = new Phaser.Game(w, h, Phaser.AUTO, '');
        user = initialiseUser("Ayush");
        callAll(game, user)
    }
}

// if (Meteor.isServer) {
//     Meteor.startup(function() {});
// }
callAll = function(game, user) {
    GameEgg(game);
    Menu(game)
    GameSelect(game, user)
    alphabetTrace(game)
    alphabetAnimation(game)
    alphabetTraceRef(game)
    game.state.start("Menu");
}
