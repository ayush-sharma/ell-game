alphabetAnimationByBMD = function(game, letter, skillBased) {

    var thisGameCounter = 0;
    var startTime;
    var touchTimeArray;

    function generateLevel() {

        var letterVSCount = user.data.letterStates["write"];
        letterVSCount = getMenuAlphabets(letterVSCount);

        var lettersSorted = Object.keys(letterVSCount).sort(function(a, b) {
            return letterVSCount[a] - letterVSCount[b]
        });

        postUserData();

        var threshold = user.data.threshholdMulti["alphabetAnimation"];

        if (thisGameCounter >= threshold) {
            alphabetTraceRef(game);
            game.state.start("alphabetTraceRef", true, true);
            return 0;
        }

        var letter = lettersSorted[0];
        thisGameCounter += 1;

        // user.data.letterStates["write"][letter] += 1;
        // saveUserData(user.data);

        return letter;

    }

    function saveLevel() {
        console.log('&&&&&&&&&&&&& ', draw_alphabet);
        user.data.letterStates["write"][draw_alphabet] += 1;
        saveUserData(user.data);
    }


    Game = {};

    alphabet_masking_bmd = function() {};

    function drawUpdate(graphics, strokeWidth, strokeColor, alphabet_tween_data, index) {



        // graphics.clear();
        // graphics.lineStyle(strokeWidth, strokeColor);

        // graphics.moveTo(alphabet_tween_data["pointsArray"][0][0], alphabet_tween_data["pointsArray"][0][1]);

        // for (var j = 0; j < alphabet_tween_data[index]; j++) {
        //     graphics.lineTo(alphabet_tween_data["pointsArray"][j][0], alphabet_tween_data["pointsArray"][j][1]);
        // }

        // // graphics.clear();
        // graphics.beginFill();

        // // graphics.arc(alphabet_tween_data["pointsArray"][index][0], alphabet_tween_data["pointsArray"][index][1], 100, 0, 2 * Math.PI);
        // // graphics.fill();

        // // for (var j = 0; j < alphabet_tween_data[index]; j++) {
        // //     graphics.arc(alphabet_tween_data["pointsArray"][j][0], alphabet_tween_data["pointsArray"][j][1], 3, 0, 2 * Math.PI);
        // // }


        // for (var i = alpha_tween_data[current_dia]["dragIndexLast"]; i <= alpha_tween_data[current_dia]["dragIndex"]; i++) {
        //     graphics.arc(alphabet_tween_data["pointsArray"][i][0], alphabet_tween_data["pointsArray"][i][1], 3, 0, 2 * Math.PI);
        //     alpha_tween_data[current_dia]["dragIndexLast"] = alpha_tween_data[current_dia]["dragIndex"]
        // }

    }

    function changeDiamondPosSetHist(index, alphabet_tween_data) {
        alphabet_tween_data["dragIndex"] = index;
        // console.log('changeDiamondPosSetHist to ', index);
        dia.position.setTo(alphabet_tween_data["pointsArray"][index][0], alphabet_tween_data["pointsArray"][index][1]);
    }

    function findClosestPoint(input_x, input_y, alphabet_tween_data) {
        var allDistace = [];
        // console.log('alphabet_tween_data["dragIndex"] ', alphabet_tween_data["dragIndex"]);
        for (var i = alphabet_tween_data["dragIndex"]; i < alphabet_tween_data["pointsArray"].length; i++) {
            var x = alphabet_tween_data["pointsArray"][i][0];
            var y = alphabet_tween_data["pointsArray"][i][1];

            var distance = Math.sqrt(((input_x - x) * (input_x - x)) + ((input_y - y) * (input_y - y)));
            allDistace.push(distance);
        }
        var min = allDistace[0];
        var minIndex = 0;

        for (var i = 1; i < allDistace.length; i++) {
            if (allDistace[i] < min) {
                minIndex = i;
                min = allDistace[i];
            }
        }
        // console.log('minIndex ', minIndex);
        return [min, minIndex + alphabet_tween_data["dragIndex"]];
    }

    function diamondBeingDragged(alphabet_tween_data) {

        // alphabet_tween_data["dragIndex"]++;
        // dia.position.setTo(alphabet_tween_data["pointsArray"][alphabet_tween_data["dragIndex"]][0], alphabet_tween_data["pointsArray"][alphabet_tween_data["dragIndex"]][1]);

        var input_x = game.input.x;
        var input_y = game.input.y;

        // console.log('input_x ', input_x, input_y);

        var closestData = findClosestPoint(input_x, input_y, alphabet_tween_data);
        var minDistace = closestData[0];
        var minIndex = closestData[1];

        if (minDistace < 100) {

            if (alphabet_tween_data["dragIndex"] < 0.3 * alphabet_tween_data["pointsArray"].length) {
                var arrayDistancePercentage = (minIndex - alphabet_tween_data["dragIndex"]) / alphabet_tween_data["pointsArray"].length;
                if (arrayDistancePercentage > 0.6) {
                    return 0;
                }
            }

            changeDiamondPosSetHist(minIndex, alphabet_tween_data);
        } else {
            dragged = 0;
            negativeAudio.play();
        }
    }


    function bmdDragDraw(alphabet_tween_data, index, lineType) {
        bmdDrag.ctx.beginPath();
        bmdDrag.ctx.lineJoin = "round";

        for (var i = alphabet_tween_data["dragIndexLast"]; i <= alphabet_tween_data["dragIndex"]; i++) {
            bmdDrag.ctx.arc(alphabet_tween_data["pointsArray"][i][0] - Game.graphics_x_offset, alphabet_tween_data["pointsArray"][i][1] - Game.graphics_y_offset, lineType.radius, 0, 2 * Math.PI, false);
            bmdDrag.ctx.fillStyle = "rgba(0,0,0,255)";
        }
        alphabet_tween_data["dragIndexLast"] = alphabet_tween_data["dragIndex"];



        // bmdDrag.ctx.arc(alpha_tween_data[i]["pointsArray"][j][0], alpha_tween_data[i]["pointsArray"][j][1], lineType.radius, 0, 2 * Math.PI, false);
        // bmdDrag.ctx.arc(alphabet_tween_data["pointsArray"][index][0] - Game.graphics_x_offset, alphabet_tween_data["pointsArray"][index][1] - Game.graphics_y_offset, lineType.radius, 0, 2 * Math.PI, false);
        // bmdDrag.ctx.fillStyle = "rgba(0,0,0,255)";
        // bmdDrag.ctx.fillStyle = '';
        bmdDrag.ctx.fill();
        bmdDrag.dirty = true;
    }



    function completePath(alphabet_tween_data) {

        if (alphabet_tween_data["dragIndex"] > alphabet_tween_data["pointsArray"].length - 4) {
            changeDiamondPosSetHist(alphabet_tween_data["pointsArray"].length - 1, alphabet_tween_data);

            bmdDragDraw(alphabet_tween_data, alphabet_tween_data["dragIndex"], {
                radius: 20,
                color: 'black'
            });

            positiveAudio.play();
            dragged = 0;
            var touchTime = (Date.now() - startTime) / 1000;
            startTime = Date.now();
            touchTimeArray.push(touchTime);
            current_dia++;

            alphabetForTween = displayAlphabetFor(draw_alphabet, current_dia / totalSegments);
            if (current_dia < totalSegments) {
                animate_diamond(current_dia);
            }
        }
    }

    function scaffoldDrawUpdate() {

        // animateAlphabet();

        if (Game.all_animation_stopped == 1 && current_dia > -1 && current_dia < totalSegments) {
            var i = current_dia;

            if (dragged == 1) {

                diamondBeingDragged(alpha_tween_data[i]);
                completePath(alpha_tween_data[i]);

                bmdDragDraw(alpha_tween_data[i], alpha_tween_data[i]["dragIndex"], {
                    radius: 20,
                    color: 'black'
                });

            }

            if (Game.all_animation_stopped == 1 && current_dia == totalSegments) {

                console.log('finish');
                success();
            }
        }
    }

    function alphabetAnimationCreate() {
        draw_alphabet = generateLevel();

        totalSegments = 3;
        segmentNumber = null;

        if (draw_alphabet == "C") {
            totalSegments = 1;
        } else if (draw_alphabet == "D") {
            totalSegments = 2;
        } else if (draw_alphabet == "E") {
            totalSegments = 4;
        }

        var loader = new Phaser.Loader(game);
        loader.image(draw_alphabet, 'asset/' + draw_alphabet + '.png');
        loader.image(draw_alphabet + 'For', 'asset/' + draw_alphabet + 'For.png');
        loader.image(draw_alphabet + 'ForOutline', 'asset/' + draw_alphabet + 'ForOutline.png');
        for (var i = 0; i < totalSegments; i++) {
            loader.image(draw_alphabet + '_' + i.toString(), 'asset/' + draw_alphabet + '_' + i.toString() + '.png');
        }
        loader.audio('letter' + draw_alphabet, 'asset/Name' + draw_alphabet + '.wav');
        loader.start();

        loader.onLoadComplete.addOnce(function() {
            if (draw_alphabet != 0) {
                playLetterSound(draw_alphabet, 1);
                ScaffoldDrawCreate(draw_alphabet, 2000);
            }
        });
    }


    function setStage(animateTime) {
        animation_no = 0;
        animation_time = animateTime;

        breakPoints = [];

        Game.all_animation_stopped = 0;

        if (typeof(tracingBack) != "undefined") {
            tracingBack.destroy();
        }
        if (typeof(alphabetWhole) != "undefined") {
            alphabetWhole.destroy();
        }
        // if (typeof(graphics_group) != "undefined") {
        //     graphics_group.destroy();
        // }
        // if (typeof(drag_graphics_group) != "undefined") {
        //     drag_graphics_group.destroy();
        // }
        if (typeof(dia) != "undefined") {
            dia.destroy();
        }
        if (typeof(finger) != "undefined") {
            finger.destroy();
        }
        if (typeof(backButton) != "undefined") {
            backButton.destroy();
        }
        if (typeof(repeatAudioButton) != "undefined") {
            repeatAudioButton.destroy();
        }
        if (typeof(AlphabetFor) != "undefined") {
            AlphabetFor.destroy();
            AlphabetFor.height = 0;
        }

        tracingBack = game.add.sprite(0, 0, 'tracingBack');
        tracingBack.scale.setTo(w / tracingBack.width, h / tracingBack.height);

        alphabetWhole = game.add.sprite(0, 0, draw_alphabet);

        backButton = game.add.sprite((0.1 * w), (0.12 * h), 'backButton');
        backButton.inputEnabled = true;
        backButton.anchor.set(0.5);
        backButton.scale.setTo((0.08 * w) / backButton.width, (0.08 * h) / backButton.height);
        backButton.events.onInputDown.add(function() {
            Menu(game);
            game.state.start('Menu');
        }, this);

        forwardButton = game.add.sprite((0.9 * w), (0.3 * h), 'backButton');
        forwardButton.inputEnabled = true;
        forwardButton.anchor.set(0.5);
        forwardButton.scale.setTo(-(0.08 * w) / forwardButton.width, (0.08 * h) / forwardButton.height);
        forwardButton.events.onInputDown.add(function() {
            success();
        }, this);

        repeatAudioButton = game.add.sprite((0.9 * w), (0.1 * h), 'repeatAudioButton');
        repeatAudioButton.inputEnabled = true;
        repeatAudioButton.anchor.set(0.5);
        // repeatAudioButton.scale.setTo((0.1 * w) / repeatAudioButton.width, (0.1 * h) / repeatAudioButton.height);
        repeatAudioButton.scale.setTo((0.08 * w) / repeatAudioButton.width, (0.08 * w) / repeatAudioButton.width);
        repeatAudioButton.events.onInputDown.add(function() {
            playLetterSound(draw_alphabet, 1);
        }, this);

        alphabetScale = {
            x: Math.round((0.8 * h) / alphabetWhole.height),
            y: Math.round((0.8 * h) / alphabetWhole.height)
        };

        alphabetWhole.scale.setTo(alphabetScale.x, alphabetScale.y);
        // alphabetWhole.alpha = 0.5;

        Game.graphics_x_offset = Math.round(w / 2 - (alphabetWhole.width / 2));
        Game.graphics_y_offset = Math.round(h / 2 - (alphabetWhole.height / 2));
        alphabetWhole.position.setTo(Game.graphics_x_offset, Game.graphics_y_offset);

        current_dia = 0;
        if (segmentNumber != null) {
            current_dia = segmentNumber - 1;
        }
        dragged = 0;

        dia = game.add.button(0, 0, 'diamond');
        dia.visible = false;
        dia.inputEnabled = true;
        // dia.scale.set(0.05);
        dia.scale.setTo((0.16 * w) / dia.width, (0.16 * h) / dia.height);

        dia.anchor.set(0.5);

        if (typeof(finger) != "undefined") {
            finger.destroy();
        }

        finger = game.add.sprite(0, 0, 'finger');
        finger.visible = false;
        // finger.scale.setTo(0.2, 0.2);
        finger.width = 0.07 * w;
        finger.height = 0.19 * h;
        finger.alpha = 0.8;
        finger.anchor.setTo(0.25, 0);

        dia.events.onInputDown.add(function() {
            dragged = 1;
            stopHints();
        }, this);

        dia.events.onInputUp.add(function() {
            dragged = 0;
            stopHints();
            gameHintTimer.loop(5000, function() {
                traceHintBMD();
            }, this);
            gameHintTimer.start();
        }, this);

        dia.events.onInputOut.add(function() {
            dragged = 0;
            stopHints();
            gameHintTimer.loop(5000, function() {
                traceHintBMD();
            }, this);
            gameHintTimer.start();
        }, this);

        startTime = Date.now();
        touchTimeArray = [];

        displayAlphabetFor(draw_alphabet, 0);
    }

    function ScaffoldDrawCreate(draw_alphabet, animateTime) {

        // Game.dragStrokeWidth = 2;
        // Game.dragStrokeColor = 0x000000;
        // Game.animationStrokeWidth = 5;
        // Game.animationStrokeColor = 0xffffff;

        setStage(animateTime);

        createBmdForMasking();

        if (typeof draw_alphabet != 'undefined') {
            getAlphabetData(draw_alphabet, alphabetScale, animation_time, true, totalSegments, segmentNumber, originPoint);

        }

    }




    function createBmdForMasking() {
        // console.log('i am here in masking');

        bmdDrag = game.make.bitmapData(alphabetWhole.width, alphabetWhole.height);
        bmdDrag.ctx.beginPath();
        bmdDrag.ctx.rect(0, 0, alphabetWhole.width, alphabetWhole.height);
        bmdDrag.dirty = true;

        displayImg = game.add.sprite(Game.graphics_x_offset, Game.graphics_y_offset, bmdDrag);
    }

    function success() {
        stopHints();

        Game.all_animation_stopped = 0;
        dia.inputEnabled = false;
        dia.visible = false;
        // graphics_group.removeAll();
        var transaction = {
            "gameName": game.state.current,
            "draw_alphabet": draw_alphabet,
            "touchTime": touchTimeArray
        };
        postTransaction(transaction);

        // saveLevel();

        if (typeof(alphabetForTween) == "undefined") {
            // var ainvayi = {
            //     val: 0
            // };
            // alphabetForTween = game.add.tween(ainvayi).to({
            //     val: 1
            // }, 1, Phaser.Easing.Default, true);

            console.log('alphabetForTween undefined');

            alphabetTraceRef(game, letter, skillBased);
            game.state.start('alphabetTraceRef');
            return 0;
        }

        alphabetForTween.onComplete.add(function() {
            console.log('here 0');
            alphabetWhole.alpha = 1;
            // graphics_group.destroy();
            // drag_graphics_group.destroy();
            AlphabetForOutline.destroy();
            if (typeof(displayImg) != "undefined") {
                // displayImg.destroy();
                displayImg.visible = false;
                console.log('here 1');
            }



            var finalLetterWidth = (0.8 * alphabetWhole.width);
            var finalLetterHeight = (0.8 * alphabetWhole.height);

            // console.log('finalHeight #######', finalHeight);
            // console.log('finalWidth #######', finalWidth);

            var finalAlphabetForWidth;
            var finalAlphabetForHeight;

            if (AlphabetFor.width > AlphabetFor.height) {
                finalAlphabetForWidth = finalLetterWidth;
                finalAlphabetForHeight = AlphabetFor.height * (finalAlphabetForWidth / AlphabetFor.width);
            } else {
                finalAlphabetForHeight = finalLetterHeight;
                finalAlphabetForWidth = AlphabetFor.width * (finalAlphabetForHeight / AlphabetFor.height);
            }



            // var tweenFinish = game.add.tween(alphabetWhole.scale).to({
            //     x: (0.6 * h) / alphabetWhole.height,
            //     y: (0.6 * h) / alphabetWhole.height
            // }, 1000, Phaser.Easing.Default, true);

            var tweenFinish = game.add.tween(alphabetWhole).to({
                width: finalLetterWidth,
                height: finalLetterHeight
            }, 1000, Phaser.Easing.Default, true);

            game.add.tween(alphabetWhole.anchor).to({
                x: 0.5,
                y: 0.5
            }, 1000, Phaser.Easing.Default, true);

            game.add.tween(alphabetWhole).to({
                x: w / 4,
                y: h / 2
            }, 1000, Phaser.Easing.Default, true);



            // game.add.tween(AlphabetFor.scale).to({
            //     x: (0.6 * h) / alphabetWhole.height,
            //     y: (0.6 * h) / alphabetWhole.height
            // }, 1000, Phaser.Easing.Default, true);

            game.add.tween(AlphabetFor).to({
                width: finalAlphabetForWidth,
                height: finalAlphabetForHeight
            }, 1000, Phaser.Easing.Default, true);

            game.add.tween(AlphabetFor.anchor).to({
                x: 0.5,
                y: 0.5
            }, 1000, Phaser.Easing.Default, true);

            game.add.tween(AlphabetFor).to({
                x: (3 * w) / 4,
                y: h / 2
            }, 1000, Phaser.Easing.Default, true);

            // console.log('before tweenFinish');

            tweenFinish.onComplete.add(function() {
                console.log('tweenFinish onComplete');
                var previousAudio = playLetterSound(draw_alphabet, 1);
                previousAudio.onStop.add(function() {
                    playLetterForSound(draw_alphabet, 1);
                }, this);
                game.time.events.add(1500, function() {
                    console.log('game over');
                    // game.time.events.add(2000, function() {
                    // alphabetTraceRef(game);
                    // game.state.start('alphabetTraceRef');
                    // alphabetAnimation(game);
                    // game.state.start("alphabetAnimation");
                    // stopHints();
                    // draw_alphabet = 0;
                    // myCounter++;
                    // dropNew();
                    alphabetTraceRef(game, letter, skillBased);
                    game.state.start('alphabetTraceRef');
                    // }, this);
                }, this);
            }, this);

        }, this);
    }

    function dropNew() {
        // draw_alphabet = Object.keys(inferringParametersGroup)[myCounter + 1];

        console.log('draw_alphabet ', draw_alphabet);

        // totalSegments = 3;
        totalSegments = inferringParametersGroup[draw_alphabet].length;
        segmentNumber = null;
        originPoint = inferringParametersGroup[draw_alphabet];

        // console.log('totalSegments originPoint ', totalSegments, originPoint);

        // var loader = new Phaser.Loader(game);
        // loader.image(draw_alphabet, 'asset/' + draw_alphabet + '.png');
        // loader.image(draw_alphabet + 'For', 'asset/' + draw_alphabet + 'For.png');
        // loader.image(draw_alphabet + 'ForOutline', 'asset/' + draw_alphabet + 'ForOutline.png');
        // loader.audio('letter' + draw_alphabet, 'asset/Name' + draw_alphabet + '.wav');
        // loader.audio(draw_alphabet + 'ForAudio', 'asset/objectName/' + draw_alphabet + 'ForAudio.mp3');
        // for (var i = 0; i < totalSegments; i++) {
        //     loader.image(draw_alphabet + '_' + i.toString(), 'asset/' + draw_alphabet + '_' + i.toString() + '.png');
        // }
        // loader.start();

        // loader.onLoadComplete.addOnce(function() {
        //     if (draw_alphabet != 0) {
        //         playLetterSound(draw_alphabet, 1);
        //         ScaffoldDrawCreate(draw_alphabet, 1);
        //     }
        // });

        var loader = loadLetterAssets(draw_alphabet);
        loader.onLoadComplete.addOnce(function() {
            playLetterSound(draw_alphabet, 1);
            ScaffoldDrawCreate(draw_alphabet, 1);
        });


    }




    alphabet_masking_bmd.prototype = {
        preload: function() {

            game.load.image('finger', 'asset/finger.png');
            game.load.image('diamond', 'asset/Egg0.png');

            game.load.image('black', 'asset/black.png');

            game.load.image('tracingBack', 'asset/tracingbg.png');
            game.load.image('fullBlack', 'asset/fullBlack.png');

            game.load.audio('positive', 'asset/positive2.wav');
            game.load.audio('negative', 'asset/negativeAudio.wav');
            game.load.image('backButton', 'asset/backButton.png');
            game.load.image('repeatAudioButton', 'asset/repeatAudioButton.png');

        },

        create: function() {
            AAudio = game.add.audio('letterA');
            BAudio = game.add.audio('letterB');
            CAudio = game.add.audio('letterC');
            DAudio = game.add.audio('letterD');
            EAudio = game.add.audio('letterE');
            negativeAudio = game.add.audio('negative');
            positiveAudio = game.add.audio('positive');

            changeDispIndex = {};

            draw_alphabet = letter;

            // myCounter = -1;

            dropNew();

        },

        update: function() {

            if (typeof(draw_alphabet) != "undefined" && draw_alphabet != 0) {
                scaffoldDrawUpdate();
            }
            if (typeof(AlphabetFor) != "undefined" && AlphabetFor._frame != null) {
                AlphabetFor.updateCrop();
            }

            if (typeof(dia) != "undefined") {
                dia.bringToTop();
            }
            if (typeof(finger) != "undefined") {
                finger.bringToTop();
            }

            if(typeof(changeDispIndex.val) != "undefined"){
                dia.position.setTo(alpha_tween_data[current_dia]["pointsArray"][Math.round(changeDispIndex.val)][0], alpha_tween_data[current_dia]["pointsArray"][Math.round(changeDispIndex.val)][1]);
                finger.position.setTo(dia.x,dia.y);
            }
        }


    }

    game.state.add("alphabetAnimationByBMD", alphabet_masking_bmd);
}


inferringParametersGroup = {
    // //number of segments
    // //transpose for each segment
    // //smoothing for each segment
    // // Invert axis
    // // starting point
    // "Dnew": [2, [1, 1],
    //     [3, 5],
    //     [0, 0],
    //     [0, 0]
    // ],
    "A": [0, 0, 0],
    "B": [0, 0, 0],
    "C": [1],
    "D": [0, 0],
    "E": [0, 0, 0, 0],
    "F": [0, 0, 0],
    "G": [
        [1, 0.5], 3, 0
    ],
    "H": [0, 0, 0],
    "I": [0],
    "J": [0],
    "K": [0, 1, 0],
    "L": [0, 0],
    "M": [0, 0, 3, 1],
    // "N": [0, 1, 2],
    "O": [
        [0.5, 0, 1]
    ],
    "P": [0, 0],
    "Q": [
        [0.5, 0, 1], 0
    ],
    "R": [0, 0, 0],
    "S": [
        [1, 0.25]
    ],
    "T": [0, 0],
    "U": [0],
    "V": [0, 3],
    "W": [0, 3, 0, 3],
    "X": [0, 1],
    "Y": [0, 1, 0],
    "Z": [0, 1, 3],
    "nBulky": [3],
    "n": [0, 3]
};
