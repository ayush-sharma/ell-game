// initialiseGame = function() {
//     w = window.innerWidth;
//     h = window.innerHeight;
//     var game = new Phaser.Game(w, h, Phaser.AUTO, '');
//     return game;
// }

// game = initialiseGame();

function getLocalStorage() {
    try {
        if (window.localStorage) {
            return window.localStorage;
        }
    } catch (e) {
        console.log(e);
        return undefined;
    }
}

clientDB = getLocalStorage();

user = {
    data: {}
    // ,
    // transaction: {}
};

getUserData = function(username) {
    if (typeof(clientDB[username + "/userData"]) != "undefined") {
        console.log('in if');
        var parsedData = JSON.parse(clientDB[name]);
        console.log('parsedData ', parsedData);
        user.data = parsedData;
    } else {
        var postStr = "username=" + username.toString();
        console.log('postStr', postStr);
        var result = sendApiRequest(false, 'getUserData', postStr);

        if (result != "0") {
            user.data = JSON.parse(result);
            console.log('user data ', user.data);
        } else {
            console.log('no user found');
        }
    }
}

saveUserData = function(input) {
    if (typeof(input) == "string") {
        input = JSON.parse(input);
    }
    if (typeof(input.data) != "undefined") {
        input.data["device"] = {
            "manufacturer": window.device.manufacturer,
            "model": window.device.model,
            "platform": window.device.platform,
            "uuid": window.device.uuid
        };
        clientDB[input.data.username + "/userData"] = input.data;

        user.data = input.data;
    } else {
        input["device"] = {
            "manufacturer": window.device.manufacturer,
            "model": window.device.model,
            "platform": window.device.platform,
            "uuid": window.device.uuid
        };
        clientDB[input.username + "/userData"] = input;
        user.data = input;
    }
}

postUserData = function() {
    var data = user.data;
    var postStr = "userData=" + JSON.stringify(data);
    console.log('postStr', postStr);
    sendApiRequest(true, 'postUserData', postStr);
}

modifyUserData = function() {
    user.data = {
        "username": user.data.username,
        1: 2,
        3: 4,
        5: 6
    }; // change the user dtata
    clientDB[user.data.username + "/userData"] = user.data;
    postUserData(user.data); // immediate server update required
}

generateTransaction = function() {
    user.transaction = {
        "username": user.data.username,
        1: 2,
        3: 4,
        5: 6
    };
    clientDB[user.data.username + "/transaction"] = user.transaction;
    postTransaction(user.transaction); // // immediate server update required
}

postTransaction = function(transaction) {
    transaction["username"] = user.data.username;
    var pendingTransactions = "[]";
    if (typeof(clientDB["pendingTransactions"]) != "undefined") {
        pendingTransactions = clientDB["pendingTransactions"];
    }
    pendingTransactions = JSON.parse(pendingTransactions);
    pendingTransactions.push(transaction);
    clientDB["pendingTransactions"] = JSON.stringify(pendingTransactions);

    var postStr = "transaction=" + JSON.stringify(pendingTransactions);
    console.log('postStr', postStr);
    sendApiRequest(true, 'postTransaction', postStr);
}

validateUser = function(mode, route, username, age, game) {

    if (username == "" || age == "") {
        alert('Enter username and age');
        loginResult = "validation failed";
        return "validation failed";
    } else {
        var postStr = "username=" + username.toString() + "&age=" + age.toString();
        console.log('postStr', postStr);

        // console.log('sendapi res ', sendApiRequest(mode, postStr));
        var res = sendApiRequest(mode, route, postStr, game);

        console.log('sendapi res ', res);
        return res;
    }
}

sendApiRequest = function(mode, route, postStr) {

    var api = "http://serverApp.meteor.com/api/" + route.toString();
    console.log('api test ', api);
    // xhttp.open("POST", "http://localhost:3000/api/signUp", true);
    var xhttp = new XMLHttpRequest();

    xhttp.onload = function() {
        console.log('internet connected((((((((((((((((((((((((((((((');
    };

    xhttp.onerror = function() {
        console.log('error))))))))))))))))))))))))');
        loginResult = "";
    };

    if (mode == true) {
        xhttp.onreadystatechange = function() {
            if (xhttp.readyState == 4 && xhttp.status == 200) {
                console.log('xhttp.responseText ', xhttp.responseText);
                if (route == "postTransaction") {
                    var result = xhttp.responseText;
                    if (result == "1") {
                        clientDB["pendingTransactions"] = "[]";
                    }
                } else {
                    loginResult = xhttp.responseText;
                }
                // else if (route == "signUp") {
                //     loginResult = xhttp.responseText;
                // }
            }
        }
    } else {
        console.log('in else');
        xhttp.onreadystatechange = function() {
            console.log('in onreadystatechange else');
            if (xhttp.readyState >= 1 && xhttp.readyState <= 3) {
                console.log('loading ...');
            } else if (xhttp.readyState == 4 && xhttp.readyState != 0) {
                console.log('stop loading');
                // loginResult = 3;
                // loading.destroy();
            }

            // if ((typeof(loading) != "undefined") && mode == false) {
            //        console.log('loading over');
            //        loading.destroy();
            //    }
        }
    }

    // loading = game.add.sprite(w / 2, h / 2, 'loading ');
    // loading.animations.add('walk');
    // loading.animations.play('walk', 20, true);

    xhttp.open("POST", api, mode);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    try {
        var fer = xhttp.send(postStr);
        console.log('123###########          ', fer);
    } catch (err) {
        console.log(err);
        return "Unexpected Error";
    }

    console.log('post sent');

    return (xhttp.responseText);
}

playLetterSound = function(letter, delay) {
    var Audio = game.add.audio("letter" + letter);
    game.time.events.add(delay, function() {
        // eval(letter + "Audio.play();");
        Audio.play();
    }, this);
    return Audio;
}

playLetterForSound = function(letter, delay) {
    var Audio = game.add.audio(letter + "ForAudio");
    game.time.events.add(delay, function() {
        Audio.play();
    }, this);
    return Audio;
}

highlight = function(sprite, scaleShift, duration, delay, repeatNo, finger, durationFinger) {
    var initialScaleX = sprite.scale.x;
    var initialScaleY = sprite.scale.y;

    finger.position.setTo(w / 2, h / 2);
    finger.bringToTop();

    var tweenHighlight = game.add.tween(sprite.scale).to({
        x: initialScaleX + scaleShift,
        y: initialScaleX + scaleShift
    }, duration, Phaser.Easing.Default, true, delay, repeatNo, true);

    var tweenFinger = game.add.tween(finger).to({
        x: sprite.x,
        y: sprite.y
    }, durationFinger, Phaser.Easing.Default, false, duration, 0);

    var ainvayi = {
        val: 0
    };
    var tweenFingerStay = game.add.tween(ainvayi).to({
        val: 1
    }, 500);


    tweenHighlight.onComplete.add(function() {
        sprite.scale.x = initialScaleX;
        sprite.scale.y = initialScaleY;
        finger.visible = true;
        tweenFinger.start();
    }, this);

    tweenFinger.onComplete.add(function() {
        tweenHighlight.stop();
        tweenFingerStay.start();
        // finger.visible = false;
    }, this);

    tweenFingerStay.onComplete.add(function() {
        tweenHighlight.stop();
        tweenFinger.stop();
        finger.visible = false;
    }, this);

    return tweenFingerStay;
}

highlightDrag = function(sprite, scaleShift, durationHighlight, delay, repeatNo, xPos, yPos, durationDrag1, durationDrag2, finger) {
    var initialScaleX = sprite.scale.x;
    var initialScaleY = sprite.scale.y;

    var initialXPos = sprite.initialX;
    var initialYPos = sprite.initialY;

    finger.position.setTo(initialXPos + sprite.width / 2, initialYPos + sprite.height / 2);
    finger.bringToTop();

    var tweenHighlight = game.add.tween(sprite.scale).to({
        x: initialScaleX + scaleShift,
        y: initialScaleX + scaleShift
    }, durationHighlight, Phaser.Easing.Default, true, delay, repeatNo, true);

    var tweenDrag1 = game.add.tween(sprite).to({
        x: xPos,
        y: yPos
    }, durationDrag1, Phaser.Easing.Default, false, 0, 0);

    var fingerTweenDrag1 = game.add.tween(finger).to({
        x: xPos + sprite.width / 2,
        y: yPos + sprite.height / 2
    }, durationDrag1, Phaser.Easing.Default, false, 0, 0);

    var tweenDrag2 = game.add.tween(sprite).to({
        x: initialXPos,
        y: initialYPos
    }, durationDrag2, Phaser.Easing.Default, false, 0, 0);

    tweenHighlight.onComplete.add(function() {
        sprite.scale.x = initialScaleX;
        sprite.scale.y = initialScaleY;

        tweenDrag1.start();
        finger.visible = true;
        fingerTweenDrag1.start();

    }, this);

    tweenDrag1.onComplete.add(function() {
        sprite.scale.x = initialScaleX;
        sprite.scale.y = initialScaleY;

        tweenDrag2.start();
        fingerTweenDrag1.stop();
        finger.visible = false;

    }, this);

    tweenDrag2.onComplete.add(function() {

        tweenHighlight.stop();
        tweenDrag1.stop();
        fingerTweenDrag1.stop();
        finger.visible = false;

        sprite.scale.x = initialScaleX;
        sprite.scale.y = initialScaleY;

    }, this);

    return tweenDrag2;
}

highlightDragEggOnly = function(sprite, scaleShift, durationHighlight, delay, repeatNo, current_dia, durationDrag1, durationDrag2, finger) {

    if (current_dia < Object.keys(alpha_tween_data).length) {
        var initialScaleX = sprite.scale.x;
        var initialScaleY = sprite.scale.y;

        // var initialXPos = sprite.initialX;
        // var initialYPos = sprite.initialY;

        finger.position.setTo(sprite.x, sprite.y);
        finger.bringToTop();
        finger.visible = true;



        var initialDragIndex = alpha_tween_data[current_dia]["dragIndex"];

        var tweenHighlight = game.add.tween(sprite.scale).to({
            x: initialScaleX + scaleShift,
            y: initialScaleX + scaleShift
        }, durationHighlight, Phaser.Easing.Default, true, delay, repeatNo, true);

        var ainvayi1 = {};
        ainvayi1.val = 0;

        var tweenDrag1 = game.add.tween(ainvayi1).to({
            val: 1
                // x: alpha_tween_data[current_dia]["pointsArray"][alpha_tween_data[current_dia]["dragIndex"]][0],
                // y: alpha_tween_data[current_dia]["pointsArray"][alpha_tween_data[current_dia]["dragIndex"]][1]
        }, durationDrag1, Phaser.Easing.Default, false, 0, 0);

        var ainvayi2 = {};
        ainvayi2.val = 0;

        var tweenDrag2 = game.add.tween(ainvayi2).to({
            val: 1
                // x: alpha_tween_data[current_dia]["pointsArray"][alpha_tween_data[current_dia]["dragIndex"]][0],
                // y: alpha_tween_data[current_dia]["pointsArray"][alpha_tween_data[current_dia]["dragIndex"]][1]
        }, durationDrag1, Phaser.Easing.Default, false, 0, 0);

        tweenHighlight.onComplete.add(function() {
            sprite.scale.x = initialScaleX;
            sprite.scale.y = initialScaleY;

            tweenDrag1.start();

        }, this);

        tweenDrag1.onComplete.add(function() {
            sprite.scale.x = initialScaleX;
            sprite.scale.y = initialScaleY;

            alpha_tween_data[current_dia]["dragIndex"] += 5;

            if (alpha_tween_data[current_dia]["dragIndex"] < Math.round(initialDragIndex + 0.4 * (alpha_tween_data[current_dia]["pointsArray"].length - 6 - initialDragIndex))) {
                // game.add.tween(sprite).to({
                sprite.x = alpha_tween_data[current_dia]["pointsArray"][alpha_tween_data[current_dia]["dragIndex"]][0];
                sprite.y = alpha_tween_data[current_dia]["pointsArray"][alpha_tween_data[current_dia]["dragIndex"]][1];
                finger.visible = true;
                finger.x = alpha_tween_data[current_dia]["pointsArray"][alpha_tween_data[current_dia]["dragIndex"]][0];
                finger.y = alpha_tween_data[current_dia]["pointsArray"][alpha_tween_data[current_dia]["dragIndex"]][1];
                // }, durationDrag1, Phaser.Easing.Default, false, 0, 0);
                tweenDrag1.start();
            } else {
                tweenDrag2.start();
            }
        }, this);

        tweenDrag2.onComplete.add(function() {

            tweenHighlight.stop();
            tweenDrag1.stop();
            finger.visible = false;

            sprite.scale.x = initialScaleX;
            sprite.scale.y = initialScaleY;

            sprite.x = alpha_tween_data[current_dia]["pointsArray"][initialDragIndex][0];
            sprite.y = alpha_tween_data[current_dia]["pointsArray"][initialDragIndex][1];

            alpha_tween_data[current_dia]["dragIndex"] = initialDragIndex;

        }, this);

        return tweenDrag2;
    }
}

traceRefHintAnimation = function(durationDrag) {

    var animateTweenManager = [];

    for (var i = 0; i < 1; i++) { //Object.keys(alpha_tween_data).length
        alpha_tween_data[i].dispIndex = 0;
        var tween = game.add.tween(alpha_tween_data[i]).to({
            dispIndex: alpha_tween_data[i].pointsArray.length - 1
        }, durationDrag, Phaser.Easing.Default);
        if (animateTweenManager.length > 0) {
            animateTweenManager[animateTweenManager.length - 1].chain(tween);
        }
        // else {
        //     tween.delay(1000);
        // }
        animateTweenManager.push(tween);
        console.log('animateTweenManager ', animateTweenManager.length);
    }

    animateTweenManager[animateTweenManager.length - 1].onComplete.add(function() {
        for (var i = 0; i < Object.keys(alpha_tween_data).length; i++) {
            alpha_tween_data[i].dispIndex = 0;
            if (typeof(animateTweenManager[i]) != "undefined") {
                animateTweenManager[i].stop();
                finger.visible = false;
            }
        }
    }, this);

    if (typeof(animateTweenManager[0]) != "undefined") {
        animateTweenManager[0].start();
        finger.visible = true;
    }

    // for (var i = 0; i < animateTweenManager.length; i++) {
    //     alpha_tween_data[i].dispIndex = 0;
    //     animateTweenManager[i].start();
    // }
    return animateTweenManager[animateTweenManager.length - 1];
}

stopHints = function() {
    console.log('in stopHints#######################');
    if (typeof(hintTween) != "undefined") {
        hintTween.stop(true);
    }
    gameHintTimer.destroy();
}

displayAlphabetFor = function(alphabet, fraction, init) {
    // console.log('qwrertfg ', alphabet, fraction, init);
    if (typeof(AlphabetForOutline) != "undefined") {
        AlphabetForOutline.destroy();
    }
    AlphabetForOutline = game.add.sprite(0.85 * w, 0.75 * h, alphabet + 'ForOutline');
    AlphabetForOutline.scale.setTo(0.1 * w / AlphabetForOutline.width, 0.1 * w / AlphabetForOutline.width);

    var initAlphabetForAndOutlineScaleX = AlphabetForOutline.scale.x;
    var initAlphabetForAndOutlineScaleY = AlphabetForOutline.scale.y;

    var previousFraction = 0;
    if (typeof(AlphabetFor) != "undefined") {
        if (AlphabetFor.key == alphabet + "For") {
            previousFraction = AlphabetFor.height / AlphabetForOutline.height;
            if (isNaN(previousFraction)) {
                previousFraction = 0;
            }
        } else {
            previousFraction = 0;
        }
        AlphabetFor.destroy();
    }
    // console.log('previousFraction ', previousFraction);
    AlphabetFor = game.add.sprite(0.85 * w, 0.75 * h, alphabet + 'For');

    if (typeof(init) == "undefined" || init == false) {
        // console.log('i9n if');

        var initAlphabetForWidth = AlphabetFor.width;
        var initAlphabetForHeight = AlphabetFor.height;

        var cropRect = new Phaser.Rectangle(0, AlphabetFor.height * (1 - previousFraction), AlphabetFor.width, previousFraction * AlphabetFor.height);
        AlphabetFor.crop(cropRect);
        AlphabetFor.position.setTo(AlphabetForOutline.x, AlphabetForOutline.y + (1 - previousFraction) * AlphabetForOutline.height);
        AlphabetFor.width = AlphabetForOutline.width;
        AlphabetFor.height = previousFraction * AlphabetForOutline.height;

        var tween = game.add.tween(cropRect).to({
            y: initAlphabetForHeight * (1 - fraction),
            height: fraction * initAlphabetForHeight
        }, 1000, Phaser.Easing.Default, true);

        game.add.tween(AlphabetFor).to({
            height: fraction * AlphabetForOutline.height,
            y: AlphabetForOutline.y + ((1 - fraction) * AlphabetForOutline.height)
        }, 1000, Phaser.Easing.Default, true);
        // .onComplete.add(function() {
        //     var AlphabetForHeight = AlphabetFor.height;
        //     var AlphabetForWidth = AlphabetFor.width;
        //     var AlphabetForOutlineHeight = AlphabetForOutline.height;
        //     var AlphabetForOutlineWidth = AlphabetForOutline.width;
        //     game.add.tween(AlphabetFor).to({
        //         height: AlphabetForHeight + 0.2 * AlphabetForOutlineHeight,
        //         width: AlphabetForWidth + 0.2 * AlphabetForOutlineWidth
        //     }, 250, Phaser.Easing.Default, true, 0, 1, true).onComplete.add(function() {
        //         AlphabetFor.height = AlphabetForHeight;
        //         AlphabetFor.width = AlphabetForWidth;
        //     }, this);
        //     game.add.tween(AlphabetForOutline).to({
        //         height: AlphabetForOutlineHeight + 0.2 * AlphabetForOutlineHeight,
        //         width: AlphabetForOutlineWidth + 0.2 * AlphabetForOutlineWidth
        //     }, 250, Phaser.Easing.Default, true, 0, 1, true).onComplete.add(function() {
        //         AlphabetForOutline.height = AlphabetForOutlineHeight;
        //         AlphabetForOutline.width = AlphabetForOutlineWidth;
        //     }, this);
        // }, this);

        return tween;
    } else {
        // console.log('in nelse');
        var cropRect = new Phaser.Rectangle(0, AlphabetFor.height * (1 - fraction), AlphabetFor.width, fraction * AlphabetFor.height);
        AlphabetFor.crop(cropRect);
        AlphabetFor.position.setTo(AlphabetForOutline.x, AlphabetForOutline.y + (1 - fraction) * AlphabetForOutline.height);
        AlphabetFor.width = AlphabetForOutline.width;
        AlphabetFor.height = fraction * AlphabetForOutline.height;
    }
}


loadLetterAssets = function(letter) {
    console.log('loading assets args ', letter, typeof(letter));
    var loader = new Phaser.Loader(game);
    if (typeof(letter) == "string") {
        loader.image(letter, 'asset/' + letter + '.png');
        loader.image(letter + 'For', 'asset/' + letter + 'For.png');
        loader.image(letter + 'ForOutline', 'asset/' + letter + 'ForOutline.png');
        loader.audio('letter' + letter, 'asset/objectName/Name of ' + letter.toUpperCase() + '.wav');
        loader.audio(letter + 'ForAudio', 'asset/objectName/' + letter.toUpperCase() + 'ForAudio.wav');
        var totalSegments = inferringParametersGroup[letter].length;
        for (var j = 0; j < totalSegments; j++) {
            loader.image(letter + '_' + j.toString(), 'asset/' + letter + '_' + j.toString() + '.png');
        }
    } else {
        for (var i = 0; i < letter.length; i++) {
            loader.image(letter[i], 'asset/' + letter[i] + '.png');
            loader.image(letter[i] + 'For', 'asset/' + letter[i] + 'For.png');
            loader.image(letter[i] + 'ForOutline', 'asset/' + letter[i] + 'ForOutline.png');
            loader.audio('letter' + letter[i], 'asset/objectName/Name of ' + letter[i].toUpperCase() + '.wav');
            // loader.audio(letter[i] + 'ForAudio', 'asset/objectName/' + letter[i].toUpperCase() + 'ForAudio.mp3');
            loader.audio(letter[i] + 'ForAudio', 'asset/objectName/' + letter[i].toUpperCase() + 'ForAudio.wav');
            var totalSegments = inferringParametersGroup[letter[i]].length;
            for (var j = 0; j < totalSegments; j++) {
                loader.image(letter[i] + '_' + j.toString(), 'asset/' + letter[i] + '_' + j.toString() + '.png');
            }
        }
    }
    loader.start();
    return loader;
}

getMenuAlphabets = function(lettersObject, menuNumber) {
    var returnLettersObject = {};
    // var menuNumber = user.data.menu;

    for (var i = 0; i < Object.keys(lettersObject).length; i++) {
        if (Object.keys(lettersObject)[i].charCodeAt() - 65 >= menuVSLetters[menuNumber][0] && Object.keys(lettersObject)[i].charCodeAt() - 65 <= menuVSLetters[menuNumber][1]) {
            returnLettersObject[Object.keys(lettersObject)[i]] = lettersObject[Object.keys(lettersObject)[i]];
        }
    }
    return returnLettersObject;
}

getSkillAlphabets = function(lettersObject, maxMenuNumber){
    var returnLettersObject = {};

    for (var i = 0; i < Object.keys(lettersObject).length; i++) {
        if (Object.keys(lettersObject)[i].charCodeAt() - 65 <= menuVSLetters[maxMenuNumber][1] && lettersObject[Object.keys(lettersObject)[i]] > 0) {
            returnLettersObject[Object.keys(lettersObject)[i]] = lettersObject[Object.keys(lettersObject)[i]];
        }
    }
    return returnLettersObject;
}

menuVSLetters = {
    1: [0, 4],
    2: [6, 12],
    3: [13, 19],
    4: [20, 25]
};






























tweenTint = function(obj, startColor, endColor, time) {
    // console.log("tween called")
    // create an object to tween with our step value at 0
    var colorBlend = {
        step: 0
    };

    // create the tween on this object and tween its step property to 100
    var colorTween = game.add.tween(colorBlend).to({
        step: 100
    }, time);

    // run the interpolateColor function every time the tween updates, feeding it the
    // updated value of our tween each time, and set the result as our tint
    colorTween.onUpdateCallback(function() {
        obj.tint = Phaser.Color.interpolateColor(startColor, endColor, 100, colorBlend.step);
    });

    // set the object to the start color straight away
    obj.tint = startColor;

    // start the tween
    colorTween.start();
}

speakTTS = function() {
    // console.log("inside general functions speakTTS")
    var ua = navigator.userAgent.toLowerCase();
    var isAndroid = ua.indexOf("android") > -1; //&& ua.indexOf("mobile");
    if (isAndroid) {
        // console.log("on mobile")
        var word = arguments[0];
        var success;
        var rate;
        // console.log(arguments[1])
        if (arguments[1] != undefined) {
            success = arguments[1]
        } else {
            success = function() {};
        }
        if (arguments[2] != undefined) {
            rate = 0.8;
        } else {
            rate = arguments[2];
        }
        TTS
            .speak({
                text: word,
                locale: 'en-IN',
                rate: rate
            }, function() {
                success();
            }, function(reason) {
                console.log("Error message: " + reason);

            });
    } else {
        // console.log("on desktop")
        var word = arguments[0];
        console.log("TTS word " + word)

    }

}
listenSTT = function() {
    // console.log("inside general functions listenSTT")
    var ua = navigator.userAgent.toLowerCase();
    var isAndroid = ua.indexOf("android") > -1; //&& ua.indexOf("mobile");
    if (isAndroid) {
        var word = arguments[0];
        var success;
        if (arguments[1] != undefined) {
            success = arguments[1]
        } else {
            success = function() {};
        }
        var maxMatches = 5;
        var promptString = "Say the letter name"; // optional
        var language = "en-US"; // optional
        window.plugins.speechrecognizer.startRecognize(function(result) {
            if (result[0].toUpperCase() === word) {
                // console.log("correctly spoken" + word + result[0])
                success();
            } else {
                speakTTS("Try Again", function() {
                    listenSTT(word, success)
                })
            }
        }, function(errorMessage) {
            console.log("Error message: " + errorMessage);
        }, maxMatches, promptString, language);
    } else {
        // console.log("on desktop")
        Meteor.call('speechjs');
        var word = arguments[0];
        console.log("STT word " + word)

    }
}

shuffle = function(array) {
    var currentIndex = array.length,
        temporaryValue, randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {

        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }

    return array;
}

sum = function(array) {
    var s = 0;
    for (var i = 0; i < array.length; i++) {
        s = s + array[i];
    };
    return s;
}
