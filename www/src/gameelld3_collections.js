Student = new Mongo.Collection("student");
Student.attachSchema(new SimpleSchema({
    symbolLevel: {
        label: "Symbol Level",
        type: Number,
        optional: true
    },
    phoneticsLevel: {
        label: "Phonetics Level",
        type: Number,
        optional: true
    }
}));
// Transactions = new Mongo.Collection("transactions");
Transactions = new Ground.Collection('transactions');
Progression = new Ground.Collection('progression');
ZayaUsers = new Ground.Collection('zayaUsers');