getAlphabetData = function(alphabet, alphabetScale, animation_time, calledFromTraceGame, alphabet_data, segmentNumber, originPoint) {
    // console.log('in getAlphabetData %5', alphabet, alphabetScale, animation_time, calledFromTraceGame, alphabet_data, segmentNumber);

    // console.log('alphabet ', alphabet);

    alpha_tween_data = {};
    var alphabetTweenManager = [];
    var iStart = 0;

    if (typeof(segmentNumber) != "undefined" && segmentNumber != null) {
        // console.log('chaning iStart');
        iStart = segmentNumber - 1;
        alphabet_data = segmentNumber;
    }

    for (var i = iStart; i < alphabet_data; i++) {
        // console.log('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`');
        // var g = game.add.graphics(0, 0);
        // graphics_group.add(g);

        // if (calledFromTraceGame != false) {
        //     if (typeof(drag_graphics_group) != "undefined") {
        //         var g_drag = game.add.graphics(0, 0);
        //         drag_graphics_group.add(g_drag);
        //     }
        // }

        // if (typeof(segmentNumber) != "undefined" && segmentNumber != null) {
        //     var g_drag = game.add.graphics(0, 0);
        //     drag_graphics_group.children.push(g_drag);
        // }

        // console.log('i ', i);
        var pointsArray = [];
        var pointsArray = getAlphabetPoints(alphabet, i, alphabetScale, originPoint[i]);

        // console.log('pointsArray ', i, pointsArray.length, pointsArray[0].length);

        alpha_tween_data[i] = {
            "pointsArray": pointsArray,
            "dragIndex": 0,
            "dragIndexLast": 0,
            "dispIndex": 0
        };

        if (calledFromTraceGame != false) {
            var tween = game.add.tween(alpha_tween_data[i]).to({
                dispIndex: alpha_tween_data[i].pointsArray.length - 1
            }, animation_time, Phaser.Easing.Default);
            if (alphabetTweenManager.length > 0) {
                alphabetTweenManager[alphabetTweenManager.length - 1].chain(tween);
            }
            alphabetTweenManager.push(tween);
        }
    }


    if (calledFromTraceGame != false) {

        alphabetTweenManager[alphabetTweenManager.length - 1].onComplete.add(function() {
            dia.visible = true;
            animate_diamond(current_dia);

            if (typeof(gameHintTimer) != "undefined") {
                gameHintTimer.destroy();
            }
            gameHintTimer = game.time.create(true);
            stopHints();
            gameHintTimer.loop(5000, function() {
                // traceHint(finger);
                traceHintBMD();
            }, this);
            gameHintTimer.start();
        }, this);

        alphabetTweenManager[0].start();
    }

}


traceHintBMD = function() {
    playLetterSound(draw_alphabet, 1);
    // hintTween = highlightEggTraceBMDHint(dia, current_dia, finger);
}

highlightEggTraceBMDHint = function(sprite, current_dia, finger) {
    if (current_dia < Object.keys(alpha_tween_data).length) {

        var initialScaleX = sprite.scale.x;
        var initialScaleY = sprite.scale.y;

        finger.position.setTo(sprite.x, sprite.y);
        finger.bringToTop();
        finger.visible = true;

        var initialDragIndex = alpha_tween_data[current_dia]["dragIndex"];
        changeDispIndex = {
            val: alpha_tween_data[current_dia]["dragIndex"]
        };

        var tweenHighlight = game.add.tween(sprite.scale).to({
            x: initialScaleX + 0.05,
            y: initialScaleX + 0.05
        }, 300, Phaser.Easing.Default, true, 0, 1, true);

        var tweenChangeDispIndex = game.add.tween(changeDispIndex).to({
            val: ((alpha_tween_data[current_dia].pointsArray.length - 1 - alpha_tween_data[current_dia]["dragIndex"]) * 0.4) + alpha_tween_data[current_dia]["dragIndex"]
        }, 700);

        tweenChangeDispIndex.onComplete.add(function() {
            changeDispIndex = {};
            tweenHighlight.stop();
            finger.visible = false;

            sprite.scale.x = initialScaleX;
            sprite.scale.y = initialScaleY;

            sprite.position.setTo(alpha_tween_data[current_dia]["pointsArray"][alpha_tween_data[current_dia]["dragIndex"]][0], alpha_tween_data[current_dia]["pointsArray"][alpha_tween_data[current_dia]["dragIndex"]][1]);
        }, this);

        tweenHighlight.onComplete.add(function() {
            sprite.scale.x = initialScaleX;
            sprite.scale.y = initialScaleY;

            console.log('tweenHighlight completed');

            tweenChangeDispIndex.start();
        }, this);

        return tweenChangeDispIndex;

    }
}

NNTraceHintBMD = function() {
    playLetterSound(draw_alphabet, 1);
    // hintTween = highlightFingerNNTraceTestHint(finger);
}


highlightFingerNNTraceTestHint = function(finger) {

    finger.position.setTo(alpha_tween_data[0].pointsArray[0][0],alpha_tween_data[0].pointsArray[0][1]);
    finger.bringToTop();
    finger.visible = true;

    changeDispIndex = {
        val: 0
    };

    var tweenChangeDispIndex = game.add.tween(changeDispIndex).to({
        val: (alpha_tween_data[0].pointsArray.length - 1)
    }, 1000, Phaser.Easing.Default, true);

    tweenChangeDispIndex.onComplete.add(function() {
        changeDispIndex = {};
        finger.visible = false;
    }, this);

    return tweenChangeDispIndex;

}


traceHint = function(finger) {
    playLetterSound(draw_alphabet, 1);
    highlightDragEggOnly(dia, 0.05, 300, 0, 1, current_dia, 2, 1, finger);
}

drawUpdate = function(graphics, strokeWidth, strokeColor, alphabet_tween_data, index) {

    graphics.clear();
    graphics.lineStyle(strokeWidth, strokeColor);

    if (typeof(alphabet_tween_data) != "undefined") {

        graphics.moveTo(alphabet_tween_data["pointsArray"][0][0], alphabet_tween_data["pointsArray"][0][1]);

        for (var j = 0; j < alphabet_tween_data[index]; j++) {
            graphics.lineTo(alphabet_tween_data["pointsArray"][j][0], alphabet_tween_data["pointsArray"][j][1]);
        }
    }
}

drawCircle = function(graphics, strokeWidth, strokeColor, alphabet_tween_data, index) {
    graphics.clear();
    // graphics.fillStyle = "#FF0000";
    graphics.beginFill();

    // graphics.moveTo(alphabet_tween_data["pointsArray"][index][0], alphabet_tween_data["pointsArray"][index][1]);

    graphics.arc(alphabet_tween_data["pointsArray"][index][0], alphabet_tween_data["pointsArray"][index][1], 100, 0, 2 * Math.PI);
    graphics.fill();


    // for (var j = 0; j < alphabet_tween_data[index]; j++) {
    //     graphics.lineTo(alphabet_tween_data["pointsArray"][j][0], alphabet_tween_data["pointsArray"][j][1]);
    // }
}

animateAlphabet = function() {
    // if (typeof(graphics_group) != "undefined") {
    //     for (var i = 0; i < graphics_group.children.length; i++) {
    //         drawUpdate(graphics_group.children[i], Game.animationStrokeWidth, Game.animationStrokeColor, alpha_tween_data[i], "dispIndex");
    //         // drawCircle(graphics_group.children[i], Game.animationStrokeWidth, Game.animationStrokeColor, alpha_tween_data[i], "dispIndex");


    //     }
    // }

}

function animationDraw(lineType, bmdDrag, index, alphabet_tween_data) {
    for (var i = 0; i < Object.keys(alpha_tween_data).length; i++) {
        for (var j = 0; j < alpha_tween_data[i].dragIndex; j++) {
            bmdDrag.ctx.beginPath();
            bmdDrag.ctx.lineJoin = "round";
            bmdDrag.ctx.arc(alpha_tween_data[i]["pointsArray"][j][0], alpha_tween_data[i]["pointsArray"][j][1], lineType.radius, 0, 2 * Math.PI, false);
            // bmdDrag.ctx.arc(alpha_tween_data[i]["pointsArray"][j][0] - Game.graphics_x_offset, alpha_tween_data[i]["pointsArray"][j][1] - Game.graphics_y_offset, lineType.radius, 0, 2 * Math.PI, false);
            bmdDrag.ctx.fillStyle = "rgba(0,0,0,255)";
            // bmdDrag.ctx.fillStyle = '';
            bmdDrag.ctx.fill();
            bmdDrag.dirty = true;
        }
    }

    // bmdDrag.ctx.beginPath();
    // bmdDrag.ctx.lineJoin = "round";
    // bmdDrag.ctx.arc(alphabet_tween_data["pointsArray"][index][0], alphabet_tween_data["pointsArray"][index][1], lineType.radius, 0, 2 * Math.PI, false);
    // // bmdDrag.ctx.arc(alpha_tween_data[i]["pointsArray"][j][0] - Game.graphics_x_offset, alpha_tween_data[i]["pointsArray"][j][1] - Game.graphics_y_offset, lineType.radius, 0, 2 * Math.PI, false);
    // bmdDrag.ctx.fillStyle = "rgba(0,0,0,255)";
    // // bmdDrag.ctx.fillStyle = '';
    // bmdDrag.ctx.fill();
    // bmdDrag.dirty = true;
}

animate_diamond = function(current_dia_value) {

    Game.all_animation_stopped = 0;
    game.add.tween(dia).to({
        x: alpha_tween_data[current_dia_value]["pointsArray"][0][0],
        y: alpha_tween_data[current_dia_value]["pointsArray"][0][1]
    }, 500, Phaser.Easing.Default, true).onComplete.add(function() {
        Game.all_animation_stopped = 1;
    }, this);

}

getAlphabetPoints = function(alphabet, num, alphabetScale, originPointSegment) {

    // console.log('###### originPointSegment ', originPointSegment);

    var spriteCheck = game.make.sprite(0, 0, alphabet + '_' + num.toString());
    bmdWidth = spriteCheck.width;
    bmdHeight = spriteCheck.height;
    spriteCheck.destroy();
    bmd = game.make.bitmapData(bmdWidth, bmdHeight);
    bmd.draw(game.cache.getImage(alphabet + '_' + num.toString()), 0, 0);
    bmd.update();

    function returnStartPoint(x, y) {
        var minDist = 10000;
        var minIndex;
        for (var i = 0; i < pointsArray.length; i++) {
            var dist = Math.sqrt((pointsArray[i][0] - x) * (pointsArray[i][0] - x) + (pointsArray[i][1] - y) * (pointsArray[i][1] - y));
            if (dist < minDist) {
                minDist = dist;
                minIndex = i;
            }
        }
        return pointsArray[minIndex];
    }


    function checkArrayInArray(point) {
        for (var i = 0; i < pointsArray.length; i++) {
            if (point[0] == pointsArray[i][0] && point[1] == pointsArray[i][1]) {
                return 1;
            }
        }
    }

    // function getNeighbours(point) {
    //     var returnPoints = [];
    //     for (var j = point[1] - 1; j <= point[1] + 1; j++) {
    //         for (var i = point[0] - 1; i <= point[0] + 1; i++) {
    //             if ((i == point[0] && j == point[1]) || i < 0 || i >= bmdWidth || j < 0 || j >= bmdHeight) {
    //                 continue;
    //             }
    //             var hex = bmd.getPixelRGB(i, j);
    //             if (hex.r == 0 && hex.g == 0 && hex.b == 0) {
    //                 var check = checkArrayInArray([i, j]);
    //                 if (check != 1) {
    //                     pointsArray.push([i, j]);
    //                     returnPoints.push([i, j]);
    //                 }
    //             }
    //         }
    //     }
    //     // return returnPoints;
    //     for (var i = 0; i < returnPoints.length; i++) {
    //         getNeighbours(returnPoints[i]);
    //     }
    // }

    function deletePointFromArray(point) {
        for (var i = 0; i < pointsArray.length; i++) {
            if (pointsArray[i][0] == point[0] && pointsArray[i][1] == point[1]) {
                pointsArray.splice(i, 1);
                // break;
            }
        }
    }


    function getNearestPoint() {
        var consi = consideredPoint;
        deletePointFromArray(consideredPoint);
        var minDist = 10000;
        var minIndex;
        for (var i = 0; i < pointsArray.length; i++) {
            var dist = Math.sqrt((pointsArray[i][0] - consi[0]) * (pointsArray[i][0] - consi[0]) + (pointsArray[i][1] - consi[1]) * (pointsArray[i][1] - consi[1]));
            if (dist < minDist) {
                minDist = dist;
                minIndex = i;
            }
        }
        if (pointsArray.length > 0) {
            myFinalPointsArray.push(pointsArray[minIndex]);
            consideredPoint = pointsArray[minIndex];
            console.log('consideredPoint ', consi, 'nearest point', consideredPoint);
        }
    }

    function getNeighbours() {
        var point = pendingPoints[0];
        // var returnPoints = [];

        if (oCounter < 20) {
            for (var j = point[1] - 1; j <= point[1] + 1; j++) {
                for (var i = point[0] - 1; i <= point[0]; i++) {
                    if (i == point[0] && j == point[1]) {
                        continue;
                    }
                    if (i >= 0 && i < cArray[0].length) {
                        if (j >= 0 && j < cArray.length) {
                            // var hex = bmd.getPixelRGB(i, j);
                            var hex = cArray[j][i];
                            // if (hex.a == 255) {
                            // if (hex.r == 0 && hex.g == 0 && hex.b == 0) {
                            if (hex == 1) {
                                var check = checkArrayInArray([i, j]);
                                if (check != 1) {
                                    oCounter++;
                                    pointsArray.push([i, j]);
                                    pendingPoints.push([i, j]);
                                    // returnPoints.push([i, j]);
                                }
                            }
                        }
                    }
                }
            }
        } else {

            for (var j = point[1] - 1; j <= point[1] + 1; j++) {
                for (var i = point[0] - 1; i <= point[0] + 1; i++) {
                    if (i == point[0] && j == point[1]) {
                        continue;
                    }
                    if (i >= 0 && i < cArray[0].length) {
                        if (j >= 0 && j < cArray.length) {
                            // var hex = bmd.getPixelRGB(i, j);
                            var hex = cArray[j][i];
                            // if (hex.a == 255) {
                            // if (hex.r == 0 && hex.g == 0 && hex.b == 0) {
                            if (hex == 1) {
                                var check = checkArrayInArray([i, j]);
                                if (check != 1) {
                                    pointsArray.push([i, j]);
                                    pendingPoints.push([i, j]);
                                    // returnPoints.push([i, j]);
                                }
                            }
                        }
                    }
                }
            }
        }

        pendingPoints.splice(0, 1);
    }












    // qw = we;


    letterMat = [];
    pointsArray = [];
    // qw = we;

    for (var j = 0; j < bmdHeight; j++) {
        var row = [];
        for (var i = 0; i < bmdWidth; i++) {
            var hex = bmd.getPixelRGB(i, j);
            // if (hex.r == 0 && hex.g == 0 && hex.b == 0) {
            if (hex.a == 255) {
                // console.log('got red');
                row.push(1);
                pointsArray.push([i, j]);
            } else {
                row.push(0);
            }
        }
        letterMat.push(row);
    }

    cArray = letterMat;

    // startPoint = [112, 1];
    // endPoint = [112, 189];


    // startPoint = returnStartPoint(bmdWidth - 1, 0);
    // pointsArray = [];
    // getNeighbours(startPoint);

    var startX;
    var startY;

    if (originPointSegment == 0) {
        startX = 0;
        startY = 0;
    } else if (originPointSegment == 1) {
        startX = bmdWidth - 1;
        startY = 0;
    } else if (originPointSegment == 2) {
        startX = bmdWidth - 1;
        startY = bmdHeight - 1;
    } else if (originPointSegment == 3) {
        startX = 0;
        startY = bmdHeight - 1;
    } else if (typeof(originPointSegment) == "object") {

        startX = originPointSegment[0] * (bmdWidth - 1);
        startY = originPointSegment[1] * (bmdHeight - 1);
    }

    // console.log('originPointSegment ', originPointSegment, startX, startY);

    startPoint = returnStartPoint(startX, startY);
    // console.log('startPoint ', startPoint);
    pointsArray = [];
    pendingPoints = [startPoint];
    // getNeighbours(startPoint);


    oCounterThreshold = 20;
    oCounter = oCounterThreshold;

    if (typeof(originPointSegment) == "object") {
        if (originPointSegment.length > 2) {
            oCounter = 0;
        }
    }

    while (pendingPoints.length != 0) {
        getNeighbours();
    }


    // myFinalPointsArray = [];
    // consideredPoint = pointsArray[0];

    // while (pointsArray.length > 0) {
    //     getNearestPoint();
    // }

    // pointsArray = myFinalPointsArray;


    // myFinalPointsArray = [];
    // consideredPoint = pointsArray[0];


    function forCircleLetters() {
        var consi = consideredPoint;
        deletePointFromArray(consideredPoint);

        for (var i = 0; i < pointsArray.length; i++) {

            if (consi[0] <= pointsArray[i][0]) {
                // var minDist = 10000;
                // var minIndex;
                // var dist = Math.sqrt((pointsArray[i][0] - consi[0]) * (pointsArray[i][0] - consi[0]) + (pointsArray[i][1] - consi[1]) * (pointsArray[i][1] - consi[1]));
                // if (dist < minDist) {
                //     minDist = dist;
                //     minIndex = i;
                // }

                circlePoints.push(pointsArray[i]);



                consideredPoint = pointsArray[i];
            }
        }

    }


    // circlePoints = [];
    // consideredPoint = pointsArray[0];

    // forCircleLetters();












    // myStartPoint = returnStartPoint(bmdWidth-1, 0);
    // consideredPoint = myStartPoint;

    // myFinalPointsArray = [];

    // while (pointsArray.length > 0) {
    //     getNearestPoint();
    // }

    // pointsArray = myFinalPointsArray;




    var scaledPointsArray = [];

    var testScale = alphabetScale;

    if (alphabet == "nBulky") {
        var testImg = game.make.sprite(0, 0, 'nBulky_0');
        console.log('testImg ', testImg.width, testImg.height);
        testScale = {
            x: ((0.8 * h) / testImg.height),
            y: ((0.8 * h) / testImg.height)
        };
    }

    // console.log('testScale ', testScale);

    for (var i = 0; i < pointsArray.length; i++) {
        var point = pointsArray[i];
        scaledPointsArray.push([(Game.graphics_x_offset + (point[0] * testScale.x)), (Game.graphics_y_offset + (point[1] * testScale.y))]);
    }
    return scaledPointsArray;








    // var scaledPointsArray = [];

    // for (var i = 0; i < pointsArray.length; i++) {
    //     var point = pointsArray[i];
    //     scaledPointsArray.push([Math.round(Game.graphics_x_offset + (point[0] * alphabetScale.x)), Math.round(Game.graphics_y_offset + (point[1] * alphabetScale.y))]);
    // }



    // return scaledPointsArray;
    // return pointsArray;

}
