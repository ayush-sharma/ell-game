sortPoints = function(start) {

    var myFinalPointsArray = [];
    var tempPointsArray = pointsArray;
    // tempPointsArray.pop();
    // tempPointsArray.pop();
    // tempPointsArray.pop();
    // tempPointsArray.pop();
    // tempPointsArray.pop();
    myFinalPointsArray = buildPath(myFinalPointsArray, tempPointsArray, start);
    // console.log(myFinalPointsArray.length)
    // myFinalPointsArray.pop();
    // myFinalPointsArray.pop();
    // myFinalPointsArray.pop();
    // myFinalPointsArray.pop();
    // myFinalPointsArray.pop();
    return myFinalPointsArray;

}

function returnStartPoint(tempPointsArray) {
    var minDist = 10000;
    var minIndex;
    //        var xLimit = letter[0].length;
    var xLimit = 0;
    //        var yLimit = letter.length;
    var yLimit = 0;
    for (var i = 0; i < tempPointsArray.length; i++) {
        var dist = Math.sqrt((tempPointsArray[i][0] - xLimit) * (tempPointsArray[i][0] - xLimit) + (tempPointsArray[i][1] - yLimit) * (tempPointsArray[i][1] - yLimit));
        if (dist < minDist) {
            minDist = dist;
            minIndex = i;
        }
    }


    return tempPointsArray[minIndex];
}

function returnStartPointBegin(tempPointsArray, start) {

    if (start === 0) {
        var xLimit = 0;
        var yLimit = 0;
    } else if (start === 1) {
        var xLimit = letterMat[0].length;
        var yLimit = 0;
    } else if (start === 2) {
        var xLimit = letterMat[0].length;
        var yLimit = letterMat.length;

    } else if (start == 5) {
        var xLimit = letterMat[0].length;
        var yLimit = letterMat.length / 2;
    } else {
        var xLimit = 0;
        var yLimit = letterMat.length;
    }

    var minDist = 10000;
    var minIndex;

    for (var i = 0; i < tempPointsArray.length; i++) {
        var dist = Math.sqrt((tempPointsArray[i][0] - xLimit) * (tempPointsArray[i][0] - xLimit) + (tempPointsArray[i][1] - yLimit) * (tempPointsArray[i][1] - yLimit));
        if (dist < minDist) {
            minDist = dist;
            minIndex = i;
        }
    }


    return tempPointsArray[minIndex];
}

function deletePointFromArray(point, tempPointsArray) {
    for (var i = 0; i < tempPointsArray.length; i++) {
        if (tempPointsArray[i][0] == point[0] && tempPointsArray[i][1] == point[1]) {
            tempPointsArray.splice(i, 1);
        }
    }
}

function getNextPoint(tempPointsArray, consi) {
    var minDist = 10000;
    var minIndex;
    for (var i = 0; i < tempPointsArray.length; i++) {
        var dist = Math.sqrt((tempPointsArray[i][0] - consi[0]) * (tempPointsArray[i][0] - consi[0]) + (tempPointsArray[i][1] - consi[1]) * (tempPointsArray[i][1] - consi[1]));
        if (dist < minDist) {
            minDist = dist;
            minIndex = i;
        }
    }
    return minIndex;
}

function buildPath(myFinalPointsArray, tempPointsArray, start) {
    if (myFinalPointsArray.length === 0) {
        var myStartPoint = returnStartPointBegin(tempPointsArray, start);
        var consideredPoint = myStartPoint;
    } else {
        var consideredPoint = myFinalPointsArray[myFinalPointsArray.length - 1];
    }
    deletePointFromArray(consideredPoint, tempPointsArray);
    var nextIndex = getNextPoint(tempPointsArray, consideredPoint)
    myFinalPointsArray.push(tempPointsArray[nextIndex]);

    if (tempPointsArray.length > 2) {
        buildPath(myFinalPointsArray, tempPointsArray)
    } else if (tempPointsArray.length === 1) {
        myFinalPointsArray.push(tempPointsArray[0]);
    }
    return myFinalPointsArray;

}
