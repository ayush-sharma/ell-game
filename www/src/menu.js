function openFirstLevel() {
    console.log('in openFirstLevel');
    popShowNewLevel(0);
}

function openNewLevel() {
    console.log('in openNewLevel');

    user.data.maxLevel = poppedEggGroup.length;
    popShowNewLevel(poppedEggGroup.length - 1);
}

function openUnlockedLevel(openLevel) {
    console.log('in openUnlockedLevel ', openLevel);

    for (var i = 0; i < poppedEggGroup.children.length; i++) {

        if (user.data.menuGroupClicked == null && i == 0) {
            continue;
        } else if (typeof(openLevel) == "undefined") {
            console.log('here gdhsa ', i);
            popShowUnlockedLevels(i);
        } else if (typeof(openLevel) != "undefined") {
            if (openLevel != (i + 1)) {
                popShowUnlockedLevels(i);
            }
        }
    }
}

function popShowUnlockedLevels(num) {
    console.log('popShowUnlockedLevels ', num);
    poppedEggGroup.children[num].animations.add('walk', [4], 100);
    poppedEggGroup.children[num].animations.play('walk');
}

function popShowNewLevel(num) {
    poppedEggGroup.children[num].animations.add('walk', [0, 1, 2, 3, 4], 5);
    poppedEggGroup.children[num].animations.play('walk');

    var audio = crackAudio.play();

    poppedEggGroup.children[num].animations.currentAnim.onComplete.add(function() {
        console.log('animation complete ', num);
        spriteEr = game.add.sprite(poppedEggGroup.children[num].x, poppedEggGroup.children[num].y - (poppedEggGroup.children[num].height / 2), 'group' + (num + 1).toString());
        spriteEr.anchor.setTo(0.5, 0);
        spriteEr.scale.setTo(0.85 * poppedEggGroup.children[num].width / spriteEr.width);
        spriteEr.y += 0.15 * spriteEr.height;
        spriteEr.alpha = 0;

        game.add.tween(spriteEr).to({
            alpha: 1
        }, 500, Phaser.Easing.Default, true);

    }, this);
}

Menu = function(game, openLevel) {

    var extremeAngle = {
        val: 0
    };

    function deviceMotionHandler(eventData) {
        var acceleration = eventData.accelerationIncludingGravity;

        var yAcceleration = acceleration.y;

        game.add.tween(extremeAngle).to({
            val: ((yAcceleration)).toFixed(0) * 10
        }, 200, Phaser.Easing.Default, true);
    }

    if (window.DeviceMotionEvent) {
        window.addEventListener('devicemotion', deviceMotionHandler, false);
    }

    function menuClicked(eggNumber) {
        console.log('eggNumber ', eggNumber);

        // if (eggNumber < Object.keys(user.data.flow)[0]) {
        if (eggNumber < user.data.maxLevel) {
            user.data.menuGroupClicked = eggNumber;
            user.data.flow[eggNumber] = ["GameEgg", "GameSelect", "alphabetTracingTraining", "alphabetTracingTest"];
            resetMenuParams(user.data.menuGroupClicked);
            controlCenterGenerateLevel();
            // } else if(eggNumber == Object.keys(user.data.flow)[0]) {
        } else if (eggNumber == user.data.maxLevel) {
            user.data.menuGroupClicked = eggNumber;
            user.data.goneBackward = 1;
            // user.data.flow[eggNumber] = ["GameEgg", "GameSelect", "alphabetTracingTraining", "alphabetTracingTest"];
            controlCenterGenerateLevel();
        } else {
            negativeAudio.play();
        }

    }

    function eggInputEnabled(flag) {
        for (var i = 0; i < unpoppedEggGroup.children.length; i++) {
            unpoppedEggGroup.children[i].inputEnabled = flag;
        }
    }


    menu = function() {};
    menu.prototype = {
        preload: function() {

            game.load.image('group1', 'asset/group1.png');
            game.load.image('group2', 'asset/group2.png');
            game.load.image('group3', 'asset/group3.png');
            game.load.image('group4', 'asset/group4.png');

            game.load.spritesheet('egg', 'asset/3834-updated.png', 639, 1080, 6);
            game.load.image('back', 'asset/crumpled_white_paper.jpg');
            game.load.audio('crack', 'asset/crack.wav');
            game.load.audio('positive', 'asset/positive2.wav');
            game.load.audio('negative', 'asset/negativeAudio.wav');

            game.load.image('black', 'asset/black.png');
            game.load.image('white', 'asset/white.png');

            game.load.image('micSkill', 'asset/micSkill.png');
            game.load.image('writeSkill', 'asset/writeSkill.png');
            game.load.image('skillBasedBG', 'asset/skillBasedBG.png');

        },
        create: function() {

            positiveAudio = game.add.audio('positive');
            negativeAudio = game.add.audio('negative');

            crackAudio = game.add.audio('crack');

            back = game.add.sprite(0, 0, 'back');
            back.scale.setTo(w / back.width, h / back.height);

            poppedEggGroup = game.add.group();
            unpoppedEggGroup = game.add.group();

            for (var i = 0; i < 4; i++) {

                var sprite = game.add.sprite((w * (i + 0.5) / 4), (5 * h / 8), 'egg');

                // if (Object.keys(user.data.flow)[0] != null && Object.keys(user.data.flow)[0] > i) {
                if (user.data.maxLevel != null && user.data.maxLevel > i) {
                    console.log('here1 ', i);
                    // sprite = poppedEggGroup.create((w * (i + 0.5) / 4), (5 * h / 8), 'egg');
                    poppedEggGroup.add(sprite);
                } else if (user.data.menuGroupClicked == null && i == 0) {
                    console.log('here2 ', i);
                    poppedEggGroup.add(sprite);
                } else if (typeof(openLevel) != "undefined" && (openLevel - 1) == i) {
                    console.log('here3 ', i);
                    poppedEggGroup.add(sprite);
                } else {
                    console.log('here4 ', i);
                    // sprite = unpoppedEggGroup.create((w * (i + 0.5) / 4), (5 * h / 8), 'egg');
                    unpoppedEggGroup.add(sprite);
                }
                sprite.anchor.setTo(0.5, 0.75);
                sprite.scale.setTo(0.8 * h / sprite.height);
                sprite.index = i + 1;

                sprite.inputEnabled = true;

                function closure(index) {
                    sprite.events.onInputDown.add(function() {
                        menuClicked(index);
                    }, this);
                }(sprite.index);

                closure(sprite.index);

            }


            if (user.data.menuGroupClicked == null) {
                game.time.events.add(1000, function() {
                    openFirstLevel();
                }, this);
            }

            if (poppedEggGroup.children.length >= 1 && user.data.menuGroupClicked != null) {
                openUnlockedLevel(openLevel);

                if (typeof(openLevel) == "undefined") {

                    for (var i = 0; i < poppedEggGroup.length; i++) {
                        var sprite = game.add.sprite(poppedEggGroup.children[i].x, poppedEggGroup.children[i].y - (poppedEggGroup.children[i].height / 2), 'group' + (i + 1).toString());
                        sprite.anchor.setTo(0.5, 0);
                        sprite.scale.setTo(0.85 * poppedEggGroup.children[i].width / sprite.width);
                        sprite.y += 0.15 * sprite.height;
                    }
                } else if (typeof(openLevel) != "undefined") {
                    for (var i = 0; i < poppedEggGroup.length; i++) {

                        if (openLevel == i + 1) {
                            continue;
                        }

                        var sprite = game.add.sprite(poppedEggGroup.children[i].x, poppedEggGroup.children[i].y - (poppedEggGroup.children[i].height / 2), 'group' + (i + 1).toString());
                        sprite.anchor.setTo(0.5, 0);
                        sprite.scale.setTo(0.85 * poppedEggGroup.children[i].width / sprite.width);
                        sprite.y += 0.15 * sprite.height;
                    }
                }
            }

            if (typeof(openLevel) != "undefined") {
                game.time.events.add(1000, function() {
                    openNewLevel();
                }, this);
            }

            // eye1 = game.add.sprite(unpoppedEggGroup.children[0].x - (unpoppedEggGroup.children[0].width / 4), unpoppedEggGroup.children[0].y - (unpoppedEggGroup.children[0].height / 2), 'white');
            // eye1.anchor.set(0.5);













            // skillBased = game.add.group();

            skillBasedBG1 = game.add.sprite(0.375 * w, 0.9 * h, 'skillBasedBG');
            skillBasedBG1.scale.set(0.17 * h / skillBasedBG1.height);
            skillBasedBG1.anchor.set(0.5);
            // skillBased.add(skillBasedBG1);

            skillBasedBG2 = game.add.sprite(skillBasedBG1.x + (1.5 * skillBasedBG1.width), 0.9 * h, 'skillBasedBG');
            skillBasedBG2.scale.set(0.17 * h / skillBasedBG2.height);
            skillBasedBG2.anchor.set(0.5);
            // skillBased.add(skillBasedBG2);

            skillBasedBG3 = game.add.sprite(skillBasedBG2.x + (1.5 * skillBasedBG2.width), 0.9 * h, 'skillBasedBG');
            skillBasedBG3.scale.set(0.17 * h / skillBasedBG3.height);
            skillBasedBG3.anchor.set(0.5);
            skillBasedBG3.alpha = 0.3;
            // skillBased.add(skillBasedBG3);

            skillBasedMic = game.add.sprite(skillBasedBG1.x, skillBasedBG1.y, 'micSkill');
            skillBasedMic.scale.set(0.8 * skillBasedBG1.height / skillBasedMic.height);
            skillBasedMic.anchor.set(0.5);
            // skillBased.add(skillBasedMic);
            skillBasedMic.inputEnabled = true;


            skillBasedTrace = game.add.sprite(skillBasedBG2.x, skillBasedBG2.y, 'writeSkill');
            skillBasedTrace.scale.set(0.8 * skillBasedBG2.height / skillBasedTrace.height);
            skillBasedTrace.anchor.set(0.5);
            // skillBased.add(skillBasedTrace);
            skillBasedTrace.inputEnabled = true;


            if (user.data.skillBasedUnlocked.micSkill == 0) {
                skillBasedMic.alpha = 0.3;
                skillBasedBG1.alpha = 0.3;

                skillBasedMic.events.onInputDown.add(function() {
                    negativeAudio.play();
                }, this);

            } else {
                skillBasedMic.events.onInputDown.add(function() {
                    skillBasedMic.events.onInputDown.add(function() {
                        GameEgg(game, "skillBased");
                        game.state.start("GameEgg", true, true);
                    }, this);
                }, this);
            }

            if (user.data.skillBasedUnlocked.writeSkill == 0) {
                skillBasedTrace.alpha = 0.3;
                skillBasedBG2.alpha = 0.3;

                skillBasedTrace.events.onInputDown.add(function() {
                    negativeAudio.play();
                }, this);
            } else {
                skillBasedTrace.events.onInputDown.add(function() {
                    skillBasedTrace.events.onInputDown.add(function() {
                        alphabetTracingTrainingCall("skillBased");
                    }, this);
                }, this);
            }




        },
        update: function() {
            for (var i = 0; i < unpoppedEggGroup.children.length; i++) {
                unpoppedEggGroup.children[i].angle = extremeAngle.val;
            }
        }
    }
    game.state.add("Menu", menu);
}
