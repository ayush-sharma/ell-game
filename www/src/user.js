 initialiseUser = function(name) {
     var user = {};

     // if (!ZayaUsers.findOne({
     //         name: name
     //     })) {
         user.name = name;
         user.threshholdMulti = [0, 0, 0];
         user.letterStates = {};
         for (var i = 0; i < GameStates.length; i++) {
             user.letterStates[GameStates[i]] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
         };
         user.difficulty = 0;
         ZayaUsers.insert(user);
     // } else {
     //     user = ZayaUsers.findOne({
     //         name: name
     //     })
     // }

     return user;
 }

 updateUser = function(user) {
     Transactions.find({
         updated: 0
     }).forEach(function(eachTr) {
         user.letterStates[eachTr.gameName][eachTr.letter.charCodeAt(0) - 97] = user.letterStates[eachTr.gameName][eachTr.letter.charCodeAt(0) - 97] + 1;
         user.threshholdMulti[eachTr.difficulty] = user.threshholdMulti[eachTr.difficulty] + 1;
         Transactions.update(eachTr._id, {
             $set: {
                 updated: 1
             }
         })
     })

     //update this once every once in a while 
     ZayaUsers.update(ZayaUsers.findOne({
         name: user.name
     })._id, {
         $set: {
             letterStates: user.letterStates,
             threshholdMulti: user.threshholdMulti,
             difficulty: user.difficulty
         }
     })
 }
 generateTransaction = function(gameName, user, levelObject) {
     Transactions.insert({
         userName: user.name,
         gameName: gameName,
         difficulty: levelObject.difficulty,
         letter: levelObject.correctAnswer,
         updated: 0
     });

     updateUser(user);
 }
