controlCenterGenerateLevel = function() {
    var currentGameState = game.state.current;

    switch (currentGameState) {
        case "Menu":
            chooseGame();
            break;
        case "GameEgg":
            return (gameEggGenerateLevel());
            break;
        case "GameSelect":
            return (gameSelectGenerateLevel());
            break;
    }
}

controlCenterSaveLevel = function() {
    var currentGameState = game.state.current;

    switch (currentGameState) {
        case "Menu":
            // chooseGame();
            break;
        case "GameEgg":
            gameEggSaveLevel();
            break;
        case "GameSelect":
            gameSelectSaveLevel();
            break;
    }
}

chooseGame = function() {
    var gameToBeCalled = user.data.flow[user.data.menuGroupClicked][0];

    if (typeof(gameToBeCalled) != "undefined") {
        switch (gameToBeCalled) {
            case "GameEgg":
                GameEgg(game);
                game.state.start("GameEgg", true, true);
                break;
            case "GameSelect":
                GameSelect(game);
                game.state.start("GameSelect", true, true);
                break;
            case "alphabetTracingTraining":
                // alphabetTracingTrainingCall();
                console.log('calling alphabetTracingTrainingCall');
                alphabetTracingTrainingCall();
                // different function to call alphaAni and alphaTraceRef alternatively
                break;
            case "alphabetTracingTest":
                alphabetTrace(game);
                game.state.start("alphabetTrace", true, true);
                break;
        }
    } else {
        // GameEgg(game);
        // game.state.start("GameEgg", true, true);
        // update user.data.flow[user.data.menuGroupClicked]
    }
}

function alphabetTracingTrainingCall() {

    var letterVSCount = user.data.letterStates["write"];
    letterVSCount = getMenuAlphabets(letterVSCount, user.data.menuGroupClicked);

    var lettersSorted = Object.keys(letterVSCount).sort(function(a, b) {
        return letterVSCount[a] - letterVSCount[b]
    });

    // postUserData();

    var threshold = user.data.threshholdMulti["alphabetTracingTraining"];

    // if (thisGameCounter >= threshold) {
    if (user.data.gameCounters.alphabetTracingTraining >= threshold) {
        user.data.flow[user.data.menuGroupClicked].splice(0, 1);
        // alphabetTrace(game);
        // game.state.start("alphabetTrace", true, true);
        chooseGame();
        return 0;
    }

    var letter = lettersSorted[0];

    // thisGameCounter += 1;
    // user.data.gameCounters.alphabetTracingTraining++;

    // if (letter == "G" || letter == "K" || letter == "L" || letter == "M" || letter == "N" || letter == "O" || letter == "P" || letter == "Q" || letter == "Z") {
    //     user.data.letterStates["write"][letter] += 1;
    //     saveUserData(user.data);
    // }

    // alphabetAnimation(game, letter);
    // game.state.start("alphabetAnimation", true, true);

    // alphabetAnimationByMasking(game, letter);
    // game.state.start("alphabetAnimationByMasking", true, true);

    alphabetAnimationByBMD(game, letter);
    game.state.start("alphabetAnimationByBMD", true, true);

}

function alphabetTracingTrainingSaveLevel(alphabet) {
    // console.log('&&&&&&&&&&&&& ', draw_alphabet);
    user.data.gameCounters.alphabetTracingTraining++;
    user.data.letterStates["write"][alphabet] += 1;
    saveUserData(user.data);
}

function alphabetTraceSaveLevel(alphabet) {
    user.data.gameCounters.alphabetTracingTest++;
    user.data.letterStates["write"][alphabet] += 1;
    saveUserData(user.data);
}


function generateNewLevelParameters() {

    user.data.maxLevel++;

    user.data.threshholdMulti = {
        "puzzle": (menuVSLetters[user.data.maxLevel][1] - menuVSLetters[user.data.maxLevel][0]) + 1,
        "gameSelect": [(menuVSLetters[user.data.maxLevel][1] - menuVSLetters[user.data.maxLevel][0]) + 1, 2, 2, 2],
        "alphabetAnimation": (menuVSLetters[user.data.maxLevel][1] - menuVSLetters[user.data.maxLevel][0]) + 1,
        "alphabetTraceRef": (menuVSLetters[user.data.maxLevel][1] - menuVSLetters[user.data.maxLevel][0]) + 1,
        "alphabetTrace": (menuVSLetters[user.data.maxLevel][1] - menuVSLetters[user.data.maxLevel][0]) + 1,
        "alphabetTracingTraining": (menuVSLetters[user.data.maxLevel][1] - menuVSLetters[user.data.maxLevel][0]) + 1,
        "alphabetTracingTest": (menuVSLetters[user.data.maxLevel][1] - menuVSLetters[user.data.maxLevel][0]) + 1
    };

    user.data.gameCounters.GameEgg = 0;
    user.data.gameCounters.GameSelect = 0;
    user.data.gameCounters.alphabetTracingTraining = 0;
    user.data.gameCounters.alphabetTracingTest = 0;
}

function alphabetTraceGenerateLevel() {

    var letterVSCount = user.data.letterStates["write"];
    letterVSCount = getMenuAlphabets(letterVSCount, user.data.menuGroupClicked);

    var lettersSorted = Object.keys(letterVSCount).sort(function(a, b) {
        return letterVSCount[a] - letterVSCount[b]
    });

    // postUserData();

    var threshold = user.data.threshholdMulti["alphabetTracingTest"];

    if (user.data.gameCounters.alphabetTracingTest >= threshold) {
        // group over
        user.data.flow[user.data.menuGroupClicked].splice(0, 1);

        if (user.data.menuGroupClicked == user.data.maxLevel) {

            generateNewLevelParameters();

            Menu(game, user.data.menuGroupClicked + 1);
            // game.state.start("Menu", true, true);
        } else {
            Menu(game);
            // game.state.start("Menu", true, true);
        }

        // Menu(game, user.data.menuGroupClicked + 1);
        game.state.start("Menu", true, true);
        return 0;
    }

    var letter = lettersSorted[0];

    console.log('hereeee####### ', letter, threshold);
    // user.data.gameCounters.alphabetTracingTest += 1;

    // if (letter == "G" || letter == "K" || letter == "L" || letter == "M" || letter == "N" || letter == "O" || letter == "P" || letter == "Q" || letter == "Z") {
    //     user.data.letterStates["write"][letter] += 1;
    //     saveUserData(user.data);
    //     alphabetTraceRefGenerateLevel();
    // }

    return letter;

}


function gameEggGenerateLevel() {

    var letterVSCount = user.data.letterStates["speak"];
    letterVSCount = getMenuAlphabets(letterVSCount, user.data.menuGroupClicked);

    var lettersSorted = Object.keys(letterVSCount).sort(function(a, b) {
        return letterVSCount[a] - letterVSCount[b]
    });

    var threshold = user.data.threshholdMulti["puzzle"];

    // if (thisGameCounter >= threshold - 3) {
    // if (thisGameCounter >= threshold) {
    if (user.data.gameCounters.GameEgg >= threshold) {
        user.data.flow[user.data.menuGroupClicked].splice(0, 1);
        // GameSelect(game);
        // game.state.start("GameSelect", true, true);
        chooseGame();
        postUserData();
        return 0;
    }

    var letter = lettersSorted[0];
    // thisGameCounter += 1;
    // user.data.gameCounters.GameEgg++;

    // user.data.letterStates["speak"][letter] += 1;
    // saveUserData(user.data);

    return letter;

}

function gameEggSaveLevel() {
    user.data.gameCounters.GameEgg++;
    postUserData();
    user.data.letterStates["speak"][letterToSolve] += 1;
    saveUserData(user.data);
}


function gameSelectGenerateLevel() {

    // if (typeof(levelObject) != "undefined") {
    //     console.log('getting last correctAnswer');
    //     lastCorrectAnswer = levelObject.correctAnswer;
    // }
    console.log('lastCorrectAnswer ', lastCorrectAnswer);
    var levelObject = {};

    var difficulty = user.data.difficulty["read"];
    var threshold = user.data.threshholdMulti["gameSelect"][difficulty];

    answerHintDelay = 5000 + (difficulty * 5000);

    postUserData();

    if (user.data.gameCounters.GameSelect >= threshold) {
        // if (user.data.wrong["read"].length == 0) {
        if (user.data.difficulty["read"] == 3) {
            optionSprites.destroy();
            AlphabetFor.destroy();
            AlphabetForOutline.destroy();
            multipleReward();

            game.time.events.add(5000, function() {
                user.data.flow[user.data.menuGroupClicked].splice(0, 1);
                postUserData();
                chooseGame();
            }, this);


            return 0;
        }
        user.data.gameCounters.GameSelect = 0;
        user.data.difficulty["read"] += 1;
        console.log('difficulty upgraded ', user.data.difficulty["read"]);
        postUserData(user.data);
        gameSelectGenerateLevel();
        // } else {
        //     user.data.gameCounters.GameSelect--;
        // }
    }

    // user.data.gameCounters.GameSelect += 1;

    var letterVSCount = user.data.letterStates["read"];
    letterVSCount = getMenuAlphabets(letterVSCount, user.data.menuGroupClicked);

    var lettersSorted = Object.keys(letterVSCount).sort(function(a, b) {
        return letterVSCount[a] - letterVSCount[b]
    });

    if (user.data.wrong["read"].length > 0) {
        var wrongAnsweredAlphabet = user.data.wrong["read"][0];
        if (wrongAnsweredAlphabet != lastCorrectAnswer) {
            console.log('coming from wrong list');
            levelObject.correctAnswer = wrongAnsweredAlphabet;
            // var deleteIndex = user.data.wrong["read"].indexOf(levelObject.correctAnswer);
            // if (deleteIndex > -1) {
            //     user.data.wrong["read"].splice(deleteIndex, 1);
            // }
            // saveUserData(user.data);
        } else {
            console.log('coming from least seen list 1');
            levelObject.correctAnswer = lettersSorted[0];
            // user.data.letterStates["read"][levelObject.correctAnswer] += 1;
            // saveUserData(user.data);
            // gameSelectSaveLevel();
        }
    } else {
        console.log('coming from least seen list 2');
        levelObject.correctAnswer = lettersSorted[0];
        // user.data.letterStates["read"][levelObject.correctAnswer] += 1;
        // saveUserData(user.data);
        // gameSelectSaveLevel();
    }

    // levelObject.correctAnswer = lettersSorted[0];
    // user.data.letterStates["read"][levelObject.correctAnswer] += 1;
    // saveUserData(user.data);


    levelObject.currentOptions = [];
    levelObject.currentOptions.push(levelObject.correctAnswer);

    console.log('levelObject.currentOptions ', levelObject.currentOptions, lettersSorted);


    for (var i = 0; i < lettersSorted.length; i++) {
        console.log('in for loop ');
        if (levelObject.currentOptions.length >= difficulty + 1) {
            console.log('breaking ', difficulty, user.data.difficulty["read"]);
            break;
        }
        if (lettersSorted[i] != levelObject.correctAnswer) {
            levelObject.currentOptions.push(lettersSorted[i]);
        }
    }

    // for (var i = 0; i <= difficulty; i++) {
    //     levelObject.currentOptions.push(lettersSorted[i]);
    // }

    levelObject.currentOptions = shuffle(levelObject.currentOptions);

    return levelObject;
}


function gameSelectSaveLevel() {
    // var deleteIndex = user.data.wrong["read"].indexOf(levelObject.correctAnswer);
    // if (deleteIndex > -1) {
    //     user.data.wrong["read"].splice(deleteIndex, 1);
    // } else {
    //     user.data.letterStates["read"][levelObject.correctAnswer] += 1;
    // }
    user.data.gameCounters.GameSelect += 1;
    user.data.letterStates["read"][levelObject.correctAnswer] += 1;
    saveUserData(user.data);
}


function multipleReward() {

    if (typeof(optionSprites) != "undefined") {
        optionSprites.destroy();
    }
    if (typeof(AlphabetFor) != "undefined") {
        AlphabetFor.destroy();
    }
    if (typeof(AlphabetForOutline) != "undefined") {
        AlphabetForOutline.destroy();
    }
    if (typeof(repeatAudioButton) != "undefined") {
        repeatAudioButton.destroy();
    }

    var currentMenuLetters = menuVSLetters[user.data.menuGroupClicked];

    var alphabets = [];
    for (var i = currentMenuLetters[0]; i <= currentMenuLetters[1]; i++) {
        alphabets.push(String.fromCharCode(i + 65));
    }

    // var alphabets = ["A", "B", "C", "D", "E"];
    alphabetsGroup = game.add.group();
    alphabetForOutlineGroup = game.add.group();
    alphabetsForGroup = game.add.group();

    for (var i = 0; i < alphabets.length; i++) {
        var alphabet = alphabetsGroup.create(w * (i + 0.5) / alphabets.length, (5 * h) / 12, alphabets[i]);
        alphabet.anchor.setTo(0.5, 0.5);
        alphabet.scale.x = ((w / alphabets.length) * 0.6 / alphabet.width); //, (w / alphabets.length) / alphabet.width);
        alphabet.height = alphabet.width;

        // var alphabetForOutline = alphabetForOutlineGroup.create(w * (i + 0.5) / alphabets.length, (h) / 2, alphabets[i]+"ForOutline");
        var outY = alphabet.y + (alphabet.height) + 10;
        var outX = alphabet.x;
        var alphabetForOutline = alphabetForOutlineGroup.create(outX, outY, alphabets[i] + "ForOutline");
        alphabetForOutline.anchor.setTo(0.5, 0.5);
        alphabetForOutline.scale.x = ((w / alphabets.length) * 0.6 / alphabetForOutline.width); //, (w / alphabets.length) / alphabet.width);
        alphabetForOutline.height = alphabetForOutline.width;

        // fillMultipleReward(alphabets, alphabets[i], 0.7);

        getFractionRough(alphabets, alphabets[i]);

    }

}

function getFractionRough(alphabets, alphabet) {
    var correctAnswered = user.data.correct["read"][alphabet];
    var wrongAnswered = 0;
    for (var i = 0; i < user.data.wrong["read"].length; i++) {
        if (user.data.wrong["read"][i] == alphabet) {
            wrongAnswered++;
        }
    }
    fillMultipleReward(alphabets, alphabet, (correctAnswered) / (correctAnswered + wrongAnswered));

}

function fillMultipleReward(alphabets, alphabet, fraction) {
    var AlphabetForOutline = alphabetForOutlineGroup.children[alphabets.indexOf(alphabet)];
    var AlphabetFor = alphabetsForGroup.create(AlphabetForOutline.x - (AlphabetForOutline.width / 2), AlphabetForOutline.y - (AlphabetForOutline.height / 2), alphabet + 'For');
    // AlphabetFor.width = AlphabetForOutline.width;
    // AlphabetFor.height = AlphabetForOutline.height;


    // AlphabetFor = game.add.sprite(0.85 * w, 0.75 * h, alphabet + 'For');

    var initAlphabetForWidth = AlphabetFor.width;
    var initAlphabetForHeight = AlphabetFor.height;

    var previousFraction = 0;

    var cropRect = new Phaser.Rectangle(0, AlphabetFor.height * (1 - previousFraction), AlphabetFor.width, previousFraction * AlphabetFor.height);
    AlphabetFor.crop(cropRect);
    AlphabetFor.position.setTo(AlphabetForOutline.x - (AlphabetForOutline.width / 2), AlphabetForOutline.y - (AlphabetForOutline.height / 2) + (1 - previousFraction) * AlphabetForOutline.height);
    AlphabetFor.width = AlphabetForOutline.width;
    AlphabetFor.height = previousFraction * AlphabetForOutline.height;
    // AlphabetFor.updateCrop();

    var tween = game.add.tween(cropRect).to({
        y: initAlphabetForHeight * (1 - fraction),
        height: fraction * initAlphabetForHeight
    }, 1000, Phaser.Easing.Default, true);

    game.add.tween(AlphabetFor).to({
        height: fraction * AlphabetForOutline.height,
        y: AlphabetForOutline.y + ((1 - fraction) * AlphabetForOutline.height) - (AlphabetForOutline.height / 2)
    }, 1000, Phaser.Easing.Default, true);
}
