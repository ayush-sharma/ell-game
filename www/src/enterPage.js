enterPage = function(game) {
    console.log('enterPage called');
    enter_page = function() {};

    createNewUserData = function(username) {
        user = {};
        user.data = {};

        user.data.username = username;
        user.data.threshholdMulti = {
            // "puzzle": 5,
            // "gameSelect": [4, 2, 2, 2],
            // "alphabetAnimation": 5,
            // "alphabetTraceRef": 5,
            // "alphabetTrace": 5,
            // "alphabetTracingTraining": 5,
            // "alphabetTracingTest": 5
            "puzzle": (menuVSLetters[1][1] - menuVSLetters[1][0]) + 1,
            "gameSelect": [(menuVSLetters[1][1] - menuVSLetters[1][0]) + 1, 2, 2, 2],
            "alphabetAnimation": (menuVSLetters[1][1] - menuVSLetters[1][0]) + 1,
            "alphabetTraceRef": (menuVSLetters[1][1] - menuVSLetters[1][0]) + 1,
            "alphabetTrace": (menuVSLetters[1][1] - menuVSLetters[1][0]) + 1,
            "alphabetTracingTraining": (menuVSLetters[1][1] - menuVSLetters[1][0]) + 1,
            "alphabetTracingTest": (menuVSLetters[1][1] - menuVSLetters[1][0]) + 1
        };
        user.data.letterStates = {};

        var letterVSCount = {};
        for (var i = 0; i < 25; i++) {
            var chr = String.fromCharCode(65 + i);
            letterVSCount[chr] = 0;
        }

        user.data.letterStates["speak"] = JSON.parse(JSON.stringify(letterVSCount));
        user.data.letterStates["read"] = JSON.parse(JSON.stringify(letterVSCount));
        user.data.letterStates["listen"] = JSON.parse(JSON.stringify(letterVSCount));
        user.data.letterStates["write"] = JSON.parse(JSON.stringify(letterVSCount));

        user.data.gameCounters = {};
        user.data.gameCounters[1] = {};
        user.data.gameCounters[2] = {};
        user.data.gameCounters[3] = {};
        user.data.gameCounters[4] = {};

        user.data.difficulty = {};
        user.data.difficulty[1] = {};
        user.data.difficulty[2] = {};
        user.data.difficulty[3] = {};
        user.data.difficulty[4] = {};

        user.data.wrong = {};
        user.data.wrong[1] = {};
        user.data.wrong[2] = {};
        user.data.wrong[3] = {};
        user.data.wrong[4] = {};


        user.data.correct = {};
        user.data.correct[1] = {};
        user.data.correct[2] = {};
        user.data.correct[3] = {};
        user.data.correct[4] = {};

        user.data.skillBasedUnlocked = {
            'micSkill': 0,
            'writeSkill': 0
        };

        resetMenuParams(1);
        resetMenuParams(2);
        resetMenuParams(3);
        resetMenuParams(4);

        // user.data.gameCounters[num].GameEgg = 0;
        // user.data.gameCounters[num].GameSelect = 0;
        // user.data.gameCounters[num].alphabetTracingTraining = 0;
        // user.data.gameCounters[num].alphabetTracingTest = 0;


        // user.data.difficulty[num]["speak"] = 0;
        // user.data.difficulty[num]["read"] = 0;
        // user.data.difficulty[num]["listen"] = 0;
        // user.data.difficulty[num]["write"] = 0;


        // user.data.wrong[num]["speak"] = [];
        // user.data.wrong[num]["read"] = [];
        // user.data.wrong[num]["listen"] = [];
        // user.data.correct[num]["read"] = {};
        // user.data.wrong[num]["write"] = [];

        user.data.flow = {};
        // user.data.flow[1] = ["GameEgg", "GameSelect", "alphabetTracingTraining", "alphabetTracingTest"];
        // user.data.flow[1] = ["alphabetTracingTest"];
        // user.data.flow[1] = ["alphabetTracingTest"];
        user.data.flow[1] = ["GameEgg", "alphabetTracingTraining", "alphabetTracingTest"];
        user.data.flow[2] = ["GameEgg", "GameSelect", "alphabetTracingTraining", "alphabetTracingTest"];
        user.data.flow[3] = ["GameEgg", "GameSelect", "alphabetTracingTraining", "alphabetTracingTest"];
        user.data.flow[4] = ["GameEgg", "GameSelect", "alphabetTracingTraining", "alphabetTracingTest"];
        // user.data.menu = 1;
        user.data.menuGroupClicked = null;

        user.data.maxLevel = 1;

        return user;
    }

    enter_page.prototype = {

        preload: function() {
            game.load.spritesheet('loading', 'asset/loading.png', 50, 50, 8);
        },

        create: function() {

            loginText = game.add.text(w / 2, h / 4, "Login", {
                font: "50px Arial",
                fill: "#ff0044",
                // wordWrap: true,
                // align: "center"
            });

            signupText = game.add.text(w / 2, (3 * h) / 4, "SignUp", {
                font: "50px Arial",
                fill: "#ff0044",
                // wordWrap: true,
                // align: "center"
            });

            var userDataGeneratedLocally = createNewUserData("ayush");
            saveUserData(userDataGeneratedLocally);

            Menu(game);
            game.state.start('Menu', true, true);




            loginText.inputEnabled = true;
            loginText.events.onInputDown.add(function() {
                // loginPage(game);
                // game.state.start('loginPage');
                var userDataGeneratedLocally = createNewUserData("ayush");
                saveUserData(userDataGeneratedLocally);

                // user.data.flow[1].splice(0,1);
                // user.data.flow[1].splice(0,1);

                Menu(game);
                game.state.start('Menu');
                // alphabetTraceRef(game, true, true);
                // game.state.start("alphabetTraceRef");
                // GameEgg(game);
                // game.state.start("GameEgg");
                // GameSelect(game);
                // game.state.start("GameSelect");
                // alphabetAnimation(game);
                // game.state.start("alphabetAnimation");
                // alphabetAnimationByMasking(game);
                // game.state.start("alphabetAnimationByMasking");
                // alphabetTrace(game, true, true);
                // game.state.start("alphabetTrace");
            }, this);

            signupText.inputEnabled = true;
            signupText.events.onInputDown.add(function() {
                // signupPage(game);
                // game.state.start('signupPage');
                newSignUp(game);
                game.state.start('newSignUp');
            }, this);

        },

        update: function() {
            // if(w<h){
            //     console.log('in resize');
            //     w = window.innerWidth;
            //     h = window.innerHeight;
            //     var currentState = game.state.current;
            //     game.destroy();
            //     game = new Phaser.Game(w, h, Phaser.AUTO, 'game');
            //     enterPage(game);
            //     game.state.start("enterPage");
            // }
        }
    }

    game.state.add("enterPage", enter_page);
}
