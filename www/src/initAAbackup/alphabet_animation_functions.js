getAlphabetData = function(alphabet, alphabetScale, animation_time, calledFromTraceGame, alphabet_data, segmentNumber) {
    console.log('in getAlphabetData ', alphabet, alphabetScale, animation_time, calledFromTraceGame, alphabet_data, segmentNumber);
    // var alphabet_data;

    // if (alphabet == 'D') {
    //     // alphabet_data = [
    //     //     [{
    //     //         "line": [
    //     //             [25, 28],
    //     //             [25, 200]
    //     //         ]
    //     //     }],
    //     //     [{
    //     //         "arc": [
    //     //             [25, 114],
    //     //             [28, 200]
    //     //         ]
    //     //     }]
    //     // ];
    //     alphabet_data = 2;
    // } else if (alphabet == 'B') {
    //     // alphabet_data = [[{"line":[[0,0],[0,400]]}],[{"arc":[[0,100,100],[-90,90],[0,0]]}],[{"arc":[[0,300,100],[-90,90],[0,200]]}]];
    //     alphabet_data = [
    //         [{
    //             "line": [
    //                 [24, 30],
    //                 [24, 199]
    //             ]
    //         }],
    //         [{
    //             "arc": [
    //                 [24, 68],
    //                 [30, 105]
    //             ]
    //         }],
    //         [{
    //             "arc": [
    //                 [24, 152],
    //                 [105, 199]
    //             ]
    //         }]
    //     ];
    //     alphabet_data = 1;
    // } else if (alphabet == 'A') {
    //     console.log('here 1');
    //     alphabet_data = [
    //         [{
    //             "line": [
    //                 [123, 59],
    //                 [40, 227]
    //             ]
    //         }]
    //         // ,
    //         // [{
    //         //     "line": [
    //         //         [123, 59],
    //         //         [209, 227]
    //         //     ]
    //         // }],
    //         // [{
    //         //     "line": [
    //         //         [64, 180],
    //         //         [185, 180]
    //         //     ]
    //         // }]
    //     ];
    //     alphabet_data = 3;

    // } else if (alphabet == 'E') {
    //     // alphabet_data = [[{"line":[[0,0],[0,400]]}],[{"arc":[[0,100,100],[-90,90],[0,0]]}],[{"arc":[[0,300,100],[-90,90],[0,200]]}]];
    //     alphabet_data = [
    //         [{
    //             "line": [
    //                 [34, 32],
    //                 [34, 212]
    //             ]
    //         }],
    //         [{
    //             "line": [
    //                 [34, 32],
    //                 [143, 32]
    //             ]
    //         }],
    //         [{
    //             "line": [
    //                 [34, 122],
    //                 [143, 122]
    //             ]
    //         }],
    //         [{
    //             "line": [
    //                 [34, 212],
    //                 [143, 212]
    //             ]
    //         }]

    //     ];
    //     alphabet_data = 4;
    // } else if (alphabet == 'C') {
    //     // alphabet_data = [[{"line":[[0,0],[0,400]]}],[{"arc":[[0,100,100],[-90,90],[0,0]]}],[{"arc":[[0,300,100],[-90,90],[0,200]]}]];
    //     alphabet_data = [
    //         [{
    //             "line": [
    //                 [34, 32],
    //                 [34, 212]
    //             ]
    //         }]

    //     ];
    //     alphabet_data = 1;
    // }

    console.log('alphabet ', alphabet);

    alpha_tween_data = {};
    var alphabetTweenManager = [];
    var iStart = 0;

    if(typeof(segmentNumber) != "undefined" && segmentNumber != null){
        console.log('chaning iStart');
        iStart = segmentNumber - 1;
        alphabet_data = segmentNumber;
    }

    for (var i = iStart; i < alphabet_data; i++) {
        var g = game.add.graphics(0, 0);
        graphics_group.add(g);

        if (typeof(calledFromTraceGame) == "undefined") {
            var g_drag = game.add.graphics(0, 0);
            drag_graphics_group.add(g_drag);
        }

        console.log('i ', i);
        var pointsArray = [];
        // var pointsArray = getAlphabetPoints(alphabet, i, alphabetScale);

        // console.log('pointsArray ', i, pointsArray.length, pointsArray[0].length);

        alpha_tween_data[i] = {
            "pointsArray": pointsArray,
            "dragIndex": 0,
            "dispIndex": 0
        };

        if (typeof(calledFromTraceGame) == "undefined") {
            var tween = game.add.tween(alpha_tween_data[i]).to({
                dispIndex: alpha_tween_data[i].pointsArray.length - 1
            }, animation_time, Phaser.Easing.Default, false, (i * animation_time));
            alphabetTweenManager.push(tween);
        }
    }

    if (typeof(calledFromTraceGame) == "undefined") {
        for (var i = 0; i < alphabetTweenManager.length; i++) {
            alphabetTweenManager[i].start();
        }
    }

    if (typeof(calledFromTraceGame) == "undefined") {
        game.time.events.add(alphabet_data * animation_time, function() {
            dia.visible = true;
            animate_diamond(current_dia);

            if (typeof(gameHintTimer) != "undefined") {
                gameHintTimer.destroy();
            }
            gameHintTimer = game.time.create(true);
            stopHints();
            gameHintTimer.loop(5000, function() {
                traceHint(finger);
            }, this);
            gameHintTimer.start();
        }, this);
    }








    // for (var i = 0; i < alphabet_data.length; i++) {

    //     var array_data = alphabet_data[i];
    //     var json_data = array_data[0];

    //     for (var shape in json_data) {

    //         if (shape == "line") {

    //             var pointsArray = straightLine({
    //                 x: Math.round(json_data["line"][0][0]),
    //                 y: Math.round(json_data["line"][0][1])
    //             }, {
    //                 x: Math.round(json_data["line"][1][0]),
    //                 y: Math.round(json_data["line"][1][1])
    //             });

    //             var g = game.add.graphics(0, 0);
    //             graphics_group.add(g);

    //             if (typeof(calledFromTraceGame) == "undefined") {
    //                 var g_drag = game.add.graphics(0, 0);
    //                 drag_graphics_group.add(g_drag);
    //             }


    //             pointsArray = getAlphabetPoints();
    //             // pointsArray = getAlphabetPointsDepricated1();

    //             alpha_tween_data[i] = {
    //                 "pointsArray": pointsArray,
    //                 "dragIndex": 0,
    //                 "dispIndex": 0
    //             };

    //             if (typeof(calledFromTraceGame) == "undefined") {
    //                 var tween = game.add.tween(alpha_tween_data[i]).to({
    //                     dispIndex: alpha_tween_data[i].pointsArray.length - 1
    //                 }, animation_time, Phaser.Easing.Default, true, (i * animation_time));
    //             }
    //             // animateTweenManager.push(tween);

    //         } else if (shape == 'arc') {

    //             var pointsArray = ellipse({
    //                 x: Math.round(json_data["arc"][0][0]),
    //                 y: Math.round(json_data["arc"][0][1])
    //             }, Math.round(json_data["arc"][1][0]), Math.round(json_data["arc"][1][1]));

    //             var g = game.add.graphics(0, 0);
    //             graphics_group.add(g);

    //             if (typeof(calledFromTraceGame) == "undefined") {
    //                 var g_drag = game.add.graphics(0, 0);
    //                 drag_graphics_group.add(g_drag);
    //             }

    //             alpha_tween_data[i] = {
    //                 "pointsArray": pointsArray,
    //                 "dragIndex": 0,
    //                 "dispIndex": 0
    //             };

    //             if (typeof(calledFromTraceGame) == "undefined") {
    //                 var tween = game.add.tween(alpha_tween_data[i]).to({
    //                     dispIndex: alpha_tween_data[i].pointsArray.length - 1
    //                 }, animation_time, Phaser.Easing.Default, true, (i * animation_time));

    //                 // tween.start();
    //             }
    //             // animateTweenManager.push(tween);

    //         }

    //     }
    // }

    // if (typeof(calledFromTraceGame) == "undefined") {
    //     game.time.events.add(alphabet_data.length * animation_time, function() {
    //         dia.visible = true;
    //         animate_diamond(current_dia);

    //         if (typeof(gameHintTimer) != "undefined") {
    //             gameHintTimer.destroy();
    //         }
    //         gameHintTimer = game.time.create(true);
    //         stopHints();
    //         gameHintTimer.loop(5000, function() {
    //             traceHint(finger);
    //         }, this);
    //         gameHintTimer.start();
    //     }, this);
    // }

}

ellipse = function(center, y_start, y_end) {

    // console.log('center ', JSON.stringify(center));

    // center.x += Game.graphics_x_offset;
    // center.x += 50;
    center.y = Game.graphics_y_offset + Math.round(center.y * alphabetScale.y);
    center.x = Math.round(center.x * alphabetScale.x);

    y_start = Game.graphics_y_offset + Math.round(y_start * alphabetScale.y);
    y_end = Game.graphics_y_offset + Math.round(y_end * alphabetScale.y);

    // console.log('y centre', y_start, y_end, center, 'GR ', Game.graphics_x_offset, Game.graphics_y_offset);

    var tween_points = {};

    var two_b = Math.abs(y_end - y_start);
    // console.log('two_b ',two_b);
    var two_a = 2 * two_b;
    // console.log('two_a ',two_a);

    var max = 0;
    var min = game.width;
    var pointsArray = [];

    for (var y = y_start; y <= y_end; y++) {
        tween_points[y] = Game.graphics_x_offset + Math.round(Math.sqrt(((1 - (Math.pow((y - center.y), 2) / Math.pow((two_b / 2), 2))) * Math.pow((two_a / 2), 2)) + Math.pow((center.x), 2)));
        if (isNaN(tween_points[y])) {
            tween_points[y] = Game.graphics_x_offset + center.x;
        }
        if (min > tween_points[y]) {
            min = tween_points[y];
        }
        if (max < tween_points[y]) {
            max = tween_points[y];
        }
    }

    var setArcOffset = Math.round((max - min) / 10);

    for (var y = y_start; y <= y_end; y++) {
        tween_points[y] -= setArcOffset;
        pointsArray.push([tween_points[y], y]);
    }

    return pointsArray;
}

straightLine = function(start_point, end_point) {

    start_point.x = Math.round(Game.graphics_x_offset + (start_point.x * alphabetScale.x));
    start_point.y = Math.round(Game.graphics_y_offset + (start_point.y * alphabetScale.y));

    end_point.x = Math.round(Game.graphics_x_offset + (end_point.x * alphabetScale.x));
    end_point.y = Math.round(Game.graphics_y_offset + (end_point.y * alphabetScale.y));

    var m = (end_point.y - start_point.y) / (end_point.x - start_point.x);

    var pointsArray = [];

    if (m == Infinity) {
        for (var y = start_point.y; y <= end_point.y; y++) {
            pointsArray.push([start_point.x, y]);
        }
    } else if (m == -Infinity) {
        for (var y = start_point.y; y >= end_point.y; y--) {
            pointsArray.push([start_point.x, y]);
        }
    } else if (m == 0) {
        for (var x = start_point.x; x <= end_point.x; x++) {
            pointsArray.push([x, start_point.y]);
        }
    } else {
        var c = start_point.y - (m * start_point.x);
        for (var y = start_point.y; y <= end_point.y; y++) {
            pointsArray.push([(y - c) / m, y]);
        }
    }

    return pointsArray;

}

traceHint = function(finger) {
    playLetterSound(draw_alphabet, 1);
    highlightDragEggOnly(dia, 0.05, 300, 0, 1, current_dia, 2, 1, finger);
}

drawUpdate = function(graphics, strokeWidth, strokeColor, alphabet_tween_data, index) {

    graphics.clear();
    graphics.lineStyle(strokeWidth, strokeColor);

    graphics.moveTo(alphabet_tween_data["pointsArray"][0][0], alphabet_tween_data["pointsArray"][0][1]);

    for (var j = 0; j < alphabet_tween_data[index]; j++) {
        graphics.lineTo(alphabet_tween_data["pointsArray"][j][0], alphabet_tween_data["pointsArray"][j][1]);
    }
}

animateAlphabet = function() {
    for (var i = 0; i < graphics_group.children.length; i++) {
        drawUpdate(graphics_group.children[i], Game.animationStrokeWidth, Game.animationStrokeColor, alpha_tween_data[i], "dispIndex");
        // drawCircle(graphics_group.children[i], Game.animationStrokeWidth, Game.animationStrokeColor, alpha_tween_data[i], "dispIndex");


    }
}

animate_diamond = function(current_dia_value) {

    Game.all_animation_stopped = 0;
    game.add.tween(dia).to({
        x: alpha_tween_data[current_dia_value]["pointsArray"][0][0],
        y: alpha_tween_data[current_dia_value]["pointsArray"][0][1]
    }, 500, Phaser.Easing.Default, true).onComplete.add(function() {
        Game.all_animation_stopped = 1;
    }, this);

}

getAlphabetPoints = function(alphabet, num, alphabetScale) {

    var spriteCheck = game.make.sprite(0, 0, alphabet + '_' + num.toString());
    bmdWidth = spriteCheck.width;
    bmdHeight = spriteCheck.height;
    spriteCheck.destroy();
    bmd = game.make.bitmapData(bmdWidth, bmdHeight);
    bmd.draw(game.cache.getImage(alphabet + '_' + num.toString()), 0, 0);
    bmd.update();

    // sad = dhf;

    var allPoints = [];
    var noOfLinks = {};

    letterMat = [];

    console.log('step 1');

    for (var j = 0; j < bmdHeight; j++) {
        var row = [];
        for (var i = 0; i < bmdWidth; i++) {
            var hex = bmd.getPixelRGB(i, j);
            if (hex.r == 0 && hex.g == 0 && hex.b == 0) {
                row.push(0);
            } else {
                row.push(1);
            }
        }
        letterMat.push(row);
    }

    // console.log('step 2 ', letterMat);


    var top_row;
    var left_col;
    var right_col;
    var bottom_row;

    for (var i = 0; i < letterMat.length; i++) {
        var first_one_at_col;
        var last_one_at_col;
        var one_at_row;
        for (var j = 0; j < letterMat[i].length; j++) {
            if (letterMat[i][j] == 1) {
                if (typeof top_row == 'undefined') {
                    // console.log('top');
                    top_row = i;
                }

                if ((typeof first_one_at_col == 'undefined') || (first_one_at_col > j)) {
                    first_one_at_col = j;
                }

                if ((typeof last_one_at_col == 'undefined') || (last_one_at_col < j)) {
                    last_one_at_col = j;
                }

                if ((typeof one_at_row == 'undefined') || (one_at_row < i)) {
                    one_at_row = i;
                }
            }
        }

        if ((typeof left_col == 'undefined') || (left_col > first_one_at_col)) {
            left_col = first_one_at_col;
        }

        if ((typeof right_col == 'undefined') || (right_col < last_one_at_col)) {
            right_col = last_one_at_col;
        }

        if ((typeof bottom_row == 'undefined') || (bottom_row < one_at_row)) {
            bottom_row = one_at_row;
        }
    }

    console.log([top_row, left_col, right_col, bottom_row]);

    letterWidth = right_col - left_col;
    letterHeight = bottom_row - top_row;








    // var linkPointsSorted = Object.keys(noOfLinks).sort(function(a, b) {
    //     return noOfLinks[a] - noOfLinks[b];
    // });

    // var startPoint = allPoints[linkPointsSorted[0]];
    // var endPoint = allPoints[linkPointsSorted[1]];


    // var letter = [
    //     [0, 0, 1, 1, 1, 0, 0, 0],
    //     [0, 0, 1, 1, 1, 0, 0, 0],
    //     [0, 0, 1, 1, 1, 0, 0, 0],
    //     [0, 0, 1, 1, 1, 0, 0, 0],
    //     [0, 0, 1, 1, 1, 0, 0, 0]
    // ];

    // var letter = cArray;


    // letterMat = [
    //     [0,1,1,1,0,0],
    //     [0,0,1,1,1,0],
    //     [0,0,0,1,1,1],
    //     [0,0,0,1,1,1],
    //     [0,0,1,1,1,0],
    //     [0,1,1,1,0,0],
    //     [1,1,1,0,0,0]
    // ];





    // letter = letterMat;
    // pointsArray = [];
    // pointsArrayReversed = [];

    // var transposed = 1;

    // letter = math.transpose(letter);

    // function findPath(mat) {
    //     var paths = [];
    //     // var transposed = 0;

    //     // if (letterWidth > letterHeight) {
    //     //     console.log('transposing');
    //     //     mat = math.transpose(mat);
    //     //     transposed = 1;
    //     // }

    //     var myLetterWidth;

    //     for (var i = 0; i < mat.length; i++) {
    //         var begin = 0;
    //         var end = 0;
    //         for (var j = 0; j < mat[i].length; j++) {
    //             if (mat[i][j] === 1) {
    //                 mat[i][j] = 0;
    //                 begin = j;
    //                 for (var k = j + 1; k < mat[i].length; k++) {
    //                     if (mat[i][k] === 0) {
    //                         end = k - 1;
    //                         if (i === 0) {
    //                             myLetterWidth = end - begin + 1;
    //                             console.log('myLetterWidth ', myLetterWidth);
    //                         }
    //                         console.log(i + " " + begin + " " + end);
    //                         console.log(end - begin);

    //                         // var middle = (end - begin) / 2 + begin;
    //                         var middle = begin;

    //                         if (transposed == 0 && myLetterWidth < 4) {
    //                             pointsArray.push([middle, i]);
    //                         } else if (transposed == 1 && myLetterWidth < 4) {
    //                             pointsArray.push([i, middle]);
    //                         }

    //                         j = mat[i].length;

    //                         break;
    //                     }
    //                     // else if (mat[i][j + myLetterWidth] === 0) {
    //                     //     mat[i][k] = 0;
    //                     // } 
    //                     else {
    //                         mat[i][k] = 0;

    //                         // i = i - 1;
    //                     }
    //                 }
    //             }
    //         }
    //         paths.push({
    //             "row": i,
    //             "start": begin,
    //             "end": end
    //         })
    //     }

    //     // if (transposed == 1) {
    //     //     mat = math.transpose(mat);
    //     // }
    //     transposed = 0;

    //     // pointsArray.pop();
    //     // pointsArray.pop();
    //     // pointsArray.pop();
    //     // pointsArray.pop();

    //     paths.pop();

    //     return mat;
    // }

    // function findPath2(mat) {
    //     var paths = [];

    //     var myLetterWidth;

    //     for (var i = mat.length - 1; i > 0; i--) {
    //         var begin = 0;
    //         var end = 0;
    //         for (var j = 0; j < mat[i].length; j++) {
    //             if (mat[i][j] === 1) {
    //                 mat[i][j] = 0;
    //                 begin = j;
    //                 for (var k = j; k < mat[i].length; k++) {
    //                     if (mat[i][k] === 0) {
    //                         end = k;
    //                         if (i === 0) {
    //                             myLetterWidth = end - begin;
    //                             console.log('myLetterWidth ', myLetterWidth);
    //                         }
    //                         console.log(i + " " + begin + " " + end);
    //                         console.log(end - begin);

    //                         var middle = (end - begin) / 2 + begin;

    //                         if (transposed == 0) {
    //                             pointsArray.push([middle, i]);
    //                         } else {
    //                             pointsArray.push([i, middle]);
    //                         }

    //                         j = mat[i].length;

    //                         break;
    //                     }
    //                     // else if (mat[i][j + myLetterWidth] === 0) {
    //                     //     mat[i][k] = 0;
    //                     // } 
    //                     else {
    //                         mat[i][k] = 0;
    //                         // i = i + 1;
    //                     }
    //                 }
    //             }
    //         }
    //         paths.push({
    //             "row": i,
    //             "start": begin,
    //             "end": end
    //         })
    //     }

    //     // if (transposed == 1) {
    //     //     mat = math.transpose(mat);
    //     // }
    //     transposed = 0;

    //     return mat;
    // }





    // function findPath3(mat) {
    //     var paths = [];
    //     // var transposed = 0;

    //     // if (letterWidth > letterHeight) {
    //     //     console.log('transposing');
    //     //     mat = math.transpose(mat);
    //     //     transposed = 1;
    //     // }

    //     var myLetterWidth;

    //     for (var i = 0; i < mat.length; i++) {
    //         var begin = 0;
    //         var end = 0;
    //         for (var j = 0; j < mat[i].length; j++) {
    //             if (mat[i][j] === 1) {
    //                 mat[i][j] = 0;
    //                 begin = j;
    //                 for (var k = j + 1; k < mat[i].length; k++) {
    //                     if (mat[i][k] === 0) {
    //                         end = k - 1;
    //                         if (i === 0) {
    //                             myLetterWidth = end - begin + 1;
    //                             console.log('myLetterWidth ', myLetterWidth);
    //                         }
    //                         console.log(i + " " + begin + " " + end);
    //                         console.log(end - begin);

    //                         // var middle = (end - begin) / 2 + begin;
    //                         var middle = end;

    //                         if (transposed == 0 && myLetterWidth < 4) {
    //                             pointsArrayReversed.push([middle, i]);
    //                         } else if (transposed == 1 && myLetterWidth < 4) {
    //                             pointsArrayReversed.push([i, middle]);
    //                         }

    //                         j = mat[i].length;

    //                         break;
    //                     }
    //                     // else if (mat[i][j + myLetterWidth] === 0) {
    //                     //     mat[i][k] = 0;
    //                     // } 
    //                     else {
    //                         mat[i][k] = 0;

    //                         // i = i - 1;
    //                     }
    //                 }
    //             }
    //         }
    //         paths.push({
    //             "row": i,
    //             "start": begin,
    //             "end": end
    //         })
    //     }

    //     // if (transposed == 1) {
    //     //     mat = math.transpose(mat);
    //     // }
    //     transposed = 0;

    //     // pointsArrayReversed.pop();
    //     // pointsArrayReversed.pop();
    //     // pointsArrayReversed.pop();

    //     return mat;
    // }

    // // letter = findPath(letter);

    // letterMatNew = findPath(letter);
    // transposed = 1;

    // // pointsArray = [];

    // letterMatNew1 = findPath3(letterMatNew);

    // // pointsArrayReversed.pop();
    // // pointsArrayReversed.pop();

    // pointsArrayReversed = pointsArrayReversed.reverse();

    // pointsArray = pointsArray.concat(pointsArrayReversed);



    letter = letterMat;



    paths = [];

    // function findPathExtreme(array) {
    //     var widths = [];
    //     for (var i = 0; i < array.length; i++) {
    //         for (var j = 0; j < array[i].length; j++) {
    //             var w = 0;
    //             if (array[i][j] === 1) {
    //                 w = w + 1;
    //             }
    //         }
    //         console.log("width " + w);
    //         widths.push(w);
    //     }
    //     for (var i = 0; i < array.length; i++) {
    //         if (widths[i] > 10) {
    //             for (var j = 0; j < array[i].length; j++) {
    //                 if (array[i][j] === 1) {
    //                     paths.push([i, j]);
    //                 }
    //             }
    //         } else {
    //             for (var j = 0; j < array[i].length; j++) {
    //                 if (array[i][j] === 1) {

    //                     for (var k = j; k < array[i].length; k++) {
    //                         if (array[i][k] === 0) {
    //                             paths.push([i, k - 1]);

    //                         }
    //                     }
    //                 }
    //             }
    //         }
    //     }

    // }


    function findPathExtreme(array) {
        var widths = [];
        for (var i = 0; i < array.length; i++) {
            var previousRow = 0;
            var w = 0;

            for (var j = 0; j < array[i].length; j++) {
                if (array[i][j] === 1) {
                    w = w + 1;
                }
            }
            console.log("width " + w);
            widths.push(w);
        }
        for (var i = 0; i < array.length; i++) {
            if (widths[i] > 10) {
                if (previousRow === 0) {
                    for (var j = 0; j < array[i].length; j++) {
                        if (array[i][j] === 1) {
                            paths.push([i, j]);
                            console.log("Pushed " + i + " " + j);
                        }
                    }
                    previousRow = 1;
                } else {
                    for (var j = 0; j < array[i].length; j++) {
                        if (array[i][j] === 1) {

                            for (var k = j; k < array[i].length; k++) {
                                if (array[i][k] === 0) {
                                    paths.push([i, k - 1]);
                                    console.log("Pushed " + i + " " + j);

                                    break;
                                }
                            }
                        }
                    }
                }
            } else {
                previousRow = 0;
                for (var j = 0; j < array[i].length; j++) {
                    if (array[i][j] === 1) {

                        for (var k = j; k < array[i].length; k++) {
                            if (array[i][k] === 0) {
                                paths.push([i, k - 1]);
                                console.log("Pushed " + i + " " + j);

                                break;

                            }
                        }
                    }
                }
            }
        }

    }



    findPathExtreme(math.transpose(letterMat));
    // findPathExtreme(letterMat);
    var circles = game.add.graphics(game.world.centerX, game.world.centerY);

    function drawC(paths) {
        for (var i = 0; i < paths.length; i++) {
            circles.lineStyle(1, 0xff0000);
            circles.drawCircle(paths[i][0], paths[i][1], 1);
        }

    }

       drawC(paths)

    pointsArray = paths;









    // letter = letterMat;
    // pointsArray = [];
    // // pointsArrayReversed = [];

    // var transposed = 1;
    // var continueIndex = 1;

    // letter = math.transpose(letter);

    // function findPath(mat) {
    //     // console.log('colledjwenfj');
    //     var paths = [];
    //     myLetterWidth = 0;
    //     heightWise = 0;
    //     for (var i = 0; i < mat.length; i++) {
    //         var begin = 0;
    //         var end = 0;
    //         for (var j = 0; j < mat[i].length; j++) {
    //             // console.log('wdjgvcdj');
    //             if (mat[i][j] === 1) {
    //                 // console.log('wdjgvcdj');
    //                 mat[i][j] = 0;
    //                 begin = j;
    //                 for (var k = j + 1; k < mat[i].length; k++) {
    //                     if (mat[i][k] === 0) {
    //                         // console.log('here 111');
    //                         end = k - 1;
    //                         break;
    //                     }
    //                 }
    //                 if (myLetterWidth == 0) {
    //                     myLetterWidth = end - begin + 1;
    //                     // console.log('myLetterWidth ', myLetterWidth);
    //                 }
    //                 // console.log(i + " " + begin + " " + end);
    //                 // console.log(end - begin);
    //                 var middle = begin;

    //                 if (transposed == 0 && myLetterWidth < 5) {
    //                     pointsArray.push([middle, i]);
    //                 } else if (myLetterWidth < 5) {
    //                     pointsArray.push([i, middle]);
    //                 }

    //                 for (var k = j; k < j + myLetterWidth; k++) {
    //                     if (myLetterWidth < 5) {
    //                         mat[i][k] = 0;
    //                     }
    //                 }
    //                 for (var k = j; k < mat[i].length; k++) {
    //                     if (myLetterWidth < 5 && mat[i][k] === 1) {
    //                         heightWise = 1;
    //                     }
    //                 }
    //                 // j = mat[i].length;
    //             }
    //         }
    //     }
    //     if (heightWise === 1) {
    //         if (continueIndex < 3) {
    //             continueIndex++;
    //             mat = findPath(mat);
    //         }
    //     } else {
    //         if (continueIndex < 3) {
    //             continueIndex++;
    //             mat = math.transpose(findPath(mat));
    //         }


    //     }
    //     transposed = 0;
    //     return mat;
    // }


    // findPath(letter);

    // finalArray = [];
    // var tempPointsArray = pointsArray;
    // tempPointsArray.splice(0, 1);
    // finalArray.push(pointsArray[0]);

    // console.log('*********************************** ', pointsArray[0][0], pointsArray[0][1]);





    function returnStartPoint() {
        var minDist = 10000;
        var minIndex;
        for (var i = 0; i < pointsArray.length; i++) {
            var dist = Math.sqrt((pointsArray[i][0]) * (pointsArray[i][0]) + (pointsArray[i][1]) * (pointsArray[i][1]));
            if (dist < minDist) {
                minDist = dist;
                minIndex = i;
            }
        }
        return pointsArray[minIndex];
    }

    function deletePointFromArray(point) {
        for (var i = 0; i < pointsArray.length; i++) {
            if (pointsArray[i][0] == point[0] && pointsArray[i][1] == point[1]) {
                pointsArray.splice(i, 1);
                // break;
            }
        }
    }


    function getNearestPoint() {
        var consi = consideredPoint;
        deletePointFromArray(consideredPoint);
        var minDist = 10000;
        var minIndex;
        for (var i = 0; i < pointsArray.length; i++) {
            var dist = Math.sqrt((pointsArray[i][0] - consi[0]) * (pointsArray[i][0] - consi[0]) + (pointsArray[i][1] - consi[1]) * (pointsArray[i][1] - consi[1]));
            if (dist < minDist) {
                minDist = dist;
                minIndex = i;
            }
        }
        if (pointsArray.length > 0) {
            myFinalPointsArray.push(pointsArray[minIndex]);
            consideredPoint = pointsArray[minIndex];
            console.log('consideredPoint ', consi, 'nearest point', consideredPoint);
        }
    }

    myStartPoint = returnStartPoint();

    consideredPoint = myStartPoint;

    myFinalPointsArray = [];

    while (pointsArray.length > 0) {
        getNearestPoint();
    }

    pointsArray = myFinalPointsArray;









    // minNext(pointsArray[0], tempPointsArray);

    function minNext(point, array) {
        // console.log("inside Minnext " + point[0] )
        // console.log("inside Minnext "  + array[0][0] )
        // console.log("inside Minnext "  + tempPointsArray.length)
        if (tempPointsArray.length > 2) {
            var minX = 100;
            var minY = 100;
            // console.log("min " + minX + " " + minY)
            var index = 0;
            // console.log("min index before "+min + index);
            for (var i = 0; i < array.length; i++) {
                // console.log(point)
                // console.log(array[i])
                var distX = Math.abs(point[0] - array[i][0]);
                // console.log("Distance X" + distX)
                if (distX <= minX) {
                    minX = distX;
                    // console.log("min point X")

                }

            }

            for (var i = 1; i < array.length; i++) {
                if (Math.abs(point[0] - array[i][0]) === minX) {
                    var distY = Math.abs(point[1] - array[i][1]);
                    // console.log("Distance Y" + distY)

                    if (distY <= minY) {
                        minY = distY;
                        index = i;
                        // console.log("min point Y " + array[index])
                    }
                }
            }
            finalArray.push(array[index]);
            tempPointsArray.splice(index, 1);
            minNext(array[index], tempPointsArray);
        } else {
            console.log("sorting finished");
            pointsArray = finalArray;

            // return array[index];
        }
    }









    // letterMatNew = math.transpose(letterMatNew);

    // findPath2(letter);


    // var startPoint = [112, 1];
    // var endPoint = [112, 188];

    // console.log('startPoint ', startPoint);
    // console.log('endPoint ', endPoint);

    // if (Math.sqrt(((startPoint[0]) * (startPoint[0])) + ((startPoint[1]) * (startPoint[1]))) > Math.sqrt(((endPoint[0]) * (endPoint[0])) + ((endPoint[1]) * (endPoint[1])))) {
    //     var temp = startPoint;
    //     startPoint = endPoint;
    //     endPoint = temp;
    // }


    function checkArrayInArray(point) {
        for (var i = 0; i < pointsArray.length; i++) {
            if (point[0] == pointsArray[i][0] && point[1] == pointsArray[i][1]) {
                return 1;
            }
        }
    }

    function getNeighbours() {
        var point = pendingPoints[0];
        // var returnPoints = [];
        for (var j = point[1] - 1; j <= point[1] + 1; j++) {
            for (var i = point[0] - 1; i <= point[0] + 1; i++) {
                if (i == point[0] && j == point[1]) {
                    continue;
                }
                if (i >= 0 && i < cArray[0].length) {
                    if (j >= 0 && j < cArray.length) {
                        // var hex = bmd.getPixelRGB(i, j);
                        var hex = cArray[j][i];
                        // if (hex.a == 255) {
                        // if (hex.r == 0 && hex.g == 0 && hex.b == 0) {
                        if (hex == 1) {
                            var check = checkArrayInArray([i, j]);
                            if (check != 1) {
                                pointsArray.push([i, j]);
                                pendingPoints.push([i, j]);
                                // returnPoints.push([i, j]);
                            }
                        }
                    }
                }
            }
        }

        pendingPoints.splice(0, 1);
        // return returnPoints;
        // for (var i = 0; i < returnPoints.length; i++) {
        //     getNeighbours(returnPoints[i]);
        // }
    }

    // console.log('step 3');


    // pointsArray = [];
    // pendingPoints = [startPoint];
    // // getNeighbours(startPoint);

    // while (pendingPoints.length != 0) {
    //     getNeighbours();
    // }


    var scaledPointsArray = [];

    for (var i = 0; i < pointsArray.length; i++) {
        var point = pointsArray[i];
        scaledPointsArray.push([(Game.graphics_x_offset + (point[0] * alphabetScale.x)), (Game.graphics_y_offset + (point[1] * alphabetScale.y))]);
    }

    // console.log('step 4');



    return scaledPointsArray;
    // return pointsArray;

}
