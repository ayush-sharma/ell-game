drawPath = function() {
    var circles = game.add.graphics(w/2-w/4, h/2-h/4);
    var index = 0;
    drawC(pointsArray[index]);

    function drawC(path) {
        circles.lineStyle(1, 0xff0000);
        circles.drawCircle(path[0], path[1], 1);
        circles.alpha = 0;
        var tween = game.add.tween(circles).to({
            alpha: 1
        }, 20, "Linear", true);
        tween.onComplete.add(function() {
                if (index < pointsArray.length - 1) {
                    index = index + 1;
                    // console.log("inside if " + pointsArray[index])
                    drawC(pointsArray[index]);
                }
            })
    }
}
