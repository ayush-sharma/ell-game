nonRedundantPoints = function() {
    var nonRedundant = [];
    nonRedundant.push(pointsArray[0]);
    for (var i = 0; i < pointsArray.length - 3; i++) {
        if (pointsArray[i][0] === pointsArray[i + 1][0] && pointsArray[i][1] === pointsArray[i + 1][1]) {} else {
            nonRedundant.push(pointsArray[i + 1]);
        }
    }
    return nonRedundant;
}

smoothedPath = function() {
    var smoothed = [];
    for (var i = 0; i < pointsArray.length - 1; i++) {
        smoothed.push(pointsArray[i]);
        // if (Math.abs(pointsArray[i + 1][0] - pointsArray[i][0]) === 0 && Math.abs(pointsArray[i + 1][1] - pointsArray[i][1]) > 25) {
        //     for (var j = pointsArray[i][1]; j < pointsArray[i + 1][1]; j++) {
        //         smoothed.push([pointsArray[i][0], j])
        //     }
        // } else if (Math.abs(pointsArray[i + 1][1] - pointsArray[i][1]) === 0 && Math.abs(pointsArray[i + 1][0] - pointsArray[i][0]) > 5) {
        //     for (var j = pointsArray[i][0]; j < pointsArray[i + 1][0]; j++) {
        //         smoothed.push([j, pointsArray[i][1]])
        //     }
        // } else 
        if (Math.abs(pointsArray[i + 1][1] - pointsArray[i][1]) + Math.abs(pointsArray[i + 1][0] - pointsArray[i][0]) > 2) {
            var height = Math.abs(pointsArray[i + 1][1] - pointsArray[i][1]);
            var width = Math.abs(pointsArray[i + 1][0] - pointsArray[i][0]);
            var propor = width / height;
            var left = 0;
            var up = 0;

            if (pointsArray[i][0] - pointsArray[i + 1][0] < 0) {
                left = 1;
            } else {
                left = -1;
            }

            if (pointsArray[i][1] - pointsArray[i + 1][1] > 0) {
                up = -1;
            } else {
                up = 1;
            }
            for (var j = 1; j < height; j++) {
                var widthToAdd = Math.floor(j * propor);
                smoothed.push([pointsArray[i][0] + (left * widthToAdd), pointsArray[i][1] + (up * j)])
            }
        }
    }
    return smoothed;
}
inverseSmooth = function() {
    var invSmArray = []
    for (var i = 0; i < pointsArray.length - 2; i = i + 3) {
        invSmArray.push(pointsArray[i]);
    }
    invSmArray.push(pointsArray[pointsArray.length - 2]);
    invSmArray.push(pointsArray[pointsArray.length - 1]);

    return invSmArray;
}
