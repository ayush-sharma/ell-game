GameEgg = function(game, skillBased) {
    // console.log("GameEgg")
    gameEgg = function() {}
    var result;
    var animations = [];

    var toTweenX = {};
    var shakeSide = 1;
    var quaking = 1;
    var letterHeight = 0;
    var letterWidth = 0;
    var correctlyPlaced = 0;
    // var thisGameCounter = 0;

    var eggBreakHintDelay = 5000;
    var tileHighlightDragHintDelay = 5000;

    var startTime;
    var touchTimeArray;

    // function gameEggGenerateLevel() {

    //     var letterVSCount = user.data.letterStates["speak"];
    //     letterVSCount = getMenuAlphabets(letterVSCount, 1);

    //     var lettersSorted = Object.keys(letterVSCount).sort(function(a, b) {
    //         return letterVSCount[a] - letterVSCount[b]
    //     });

    //     postUserData();

    //     var threshold = user.data.threshholdMulti["puzzle"];

    //     // if (thisGameCounter >= threshold - 3) {
    //     // if (thisGameCounter >= threshold) {
    //     if (user.data.gameCounters.GameEgg >= threshold) {
    //         GameSelect(game);
    //         game.state.start("GameSelect", true, true);
    //         return 0;
    //     }

    //     var letter = lettersSorted[0];
    //     // thisGameCounter += 1;
    //     user.data.gameCounters.GameEgg++;

    //     // user.data.letterStates["speak"][letter] += 1;
    //     // saveUserData(user.data);

    //     return letter;

    // }

    // function saveLevel() {
    //     user.data.gameCounters.GameEgg++;
    //     postUserData();
    //     user.data.letterStates["speak"][letterToSolve] += 1;
    //     saveUserData(user.data);
    // }

    function tileHighlight() {
        // console.log('tileHighlight');
        for (var i = 0; i < tilesGroup.children.length; i++) {
            if (tilesGroup.children[i].inputEnabled == true) {
                var selectedTile = tilesGroup.children[i];
                hintTween = highlightDrag(selectedTile, 0.1, 300, 0, 1, selectedTile.xPosition, selectedTile.yPosition, 1000, 350, finger);
                break;
            }
        }
    }


    var letterMouthMap = {
        "A": [2, 1, 1, 2],
        "B": [2, 3, 5, 2],
        "C": [2, 3, 5, 2],
        "D": [2, 3, 5, 2],
        "E": [2, 3, 3, 2],
        "F": [2, 6, 6, 2],
        "G": [4, 3, 5, 2],
        "H": [2, 3, 4, 2],
        "I": [1, 3, 5, 2],
        "J": [5, 1, 3, 2],
        "K": [5, 1, 3, 2],
        "L": [7, 11, 11, 11],
        "M": [1, 2, 2, 2],
        "N": [1, 7, 7],
        "O": [1, 8, 9],
        "P": [2, 1, 5, 2],
        "Q": [2, 12, 9],
        "R": [1, 10, 10],
        "S": [1, 3, 5],
        "T": [2, 3, 5],
        "U": [2, 12, 9],
        "V": [6, 3, 5],
        "W": [1, 2, 7, 8],
        "X": [1, 3, 5],
        "Y": [9, 1, 3],
        "Z": [3, 1, 7]
    };


    function startMouth(letter) {
        letter = letter.toUpperCase();
        mouthSprites.visible = true;
        microphone.visible = true;
        game.add.tween(microphone).to({
            angle: -30
        }, 100, Phaser.Easing.Default, true, 0, 4, true);

        for (var i = 0; i < letterMouthMap[letter].length; i++) {
            var index = letterMouthMap[letter][i].toString();

            function changeTexture(i, index) {
                game.time.events.add((i + 1) * 300, function() {
                    mouthSprites.loadTexture('mouth' + index);
                    mouthSprites.update();
                    // mouthSprites.scale.setTo(0.2*w/mouthSprites.width, 0.2*w/mouthSprites.width);
                    mouthSprites.width = 0.2 * w;
                    mouthSprites.height = 0.2 * h;
                    mouthSprites.bringToTop();
                }, this);
            }(i, index);
            changeTexture(i, index);
        }
    }

    function success() {
        var tween = displayAlphabetFor(letterToSolve, 1);
        tween.onComplete.add(function() {
            letterImage.alpha = 1;
            tilesGroup.destroy();
            AlphabetForOutline.destroy();

            // saveLevel();
            controlCenterSaveLevel(skillBased);

            var finalLetterWidth = (0.8 * letterImage.width);
            var finalLetterHeight = (0.8 * letterImage.height);

            // console.log('finalHeight #######', finalHeight);
            // console.log('finalWidth #######', finalWidth);

            var finalAlphabetForWidth;
            var finalAlphabetForHeight;

            if (AlphabetFor.width > AlphabetFor.height) {
                finalAlphabetForWidth = finalLetterWidth;
                finalAlphabetForHeight = AlphabetFor.height * (finalAlphabetForWidth / AlphabetFor.width);
            } else {
                finalAlphabetForHeight = finalLetterHeight;
                finalAlphabetForWidth = AlphabetFor.width * (finalAlphabetForHeight / AlphabetFor.height);
            }

            // var tweenFinish = game.add.tween(letterImage.scale).to({
            //     x: (0.35 * w) / letterImage.width,
            //     y: (0.35 * w) / letterImage.width
            // }, 1000, Phaser.Easing.Default, true);

            var tweenFinish = game.add.tween(letterImage).to({
                width: finalLetterWidth,
                height: finalLetterHeight
            }, 1000, Phaser.Easing.Default, true);

            game.add.tween(letterImage.anchor).to({
                x: 0.5,
                y: 0.5
            }, 1000, Phaser.Easing.Default, true);

            game.add.tween(letterImage).to({
                x: w / 4,
                y: h / 2
            }, 1000, Phaser.Easing.Default, true);

            // game.add.tween(AlphabetFor.scale).to({
            //     x: (0.35 * w) / letterImage.width,
            //     y: (0.35 * w) / letterImage.width
            // }, 1000, Phaser.Easing.Default, true);

            game.add.tween(AlphabetFor).to({
                width: finalAlphabetForWidth,
                height: finalAlphabetForHeight
            }, 1000, Phaser.Easing.Default, true);

            game.add.tween(AlphabetFor.anchor).to({
                x: 0.5,
                y: 0.5
            }, 1000, Phaser.Easing.Default, true);

            game.add.tween(AlphabetFor).to({
                x: (3 * w) / 4,
                y: h / 2
            }, 1000, Phaser.Easing.Default, true);

            console.log('before tweenFinish');

            tweenFinish.onComplete.add(function() {
                console.log('tweenFinish onComplete');
                var previousAudio = playLetterSound(letterToSolve, 1);
                previousAudio.onStop.add(function() {
                    playLetterForSound(letterToSolve, 1);
                }, this);
                game.time.events.add(1500, function() {
                    console.log('call next level');
                    playPuzzle();
                }, this);
            }, this);

        }, this);
    }


    function listenW3C() {
        console.log('called letterToSolve ', letterToSolve);
        startMouth(letterToSolve);
        recognize++;

        var spoken = 1;

        // if (recognize == 5) {
        //     console.log('after 5 tries');
        //     recognize = 1;
        //     // startMouth(letterToSolve);
        //     // call mouth animation
        //     // oncomplete
        //     game.time.events.add(1500, function() {
        //         startMouth(letterToSolve);
        //         if (recognize > 0 && recognize < 5) {
        //             listenW3C(letterToSolve);
        //         }
        //     }, this);
        // }

        recognition.start();
        recognition.onresult = function(event) {
            spoken = 0;
            console.log('in result event');
            console.log('result ', JSON.stringify(event.results));
            if (event.results.length > 0) {
                if (event.results[0][0].transcript.toUpperCase() === letterToSolve.toUpperCase()) {
                    recognize = 0;
                    console.log("correct");
                    positiveAudio.play();
                    mouthSprites.visible = false;
                    microphone.visible = false;

                    success();

                    // var tween = displayAlphabetFor(letterToSolve, 1);
                    // tween.onComplete.add(function() {
                    //     letterImage.alpha = 1;
                    //     tilesGroup.destroy();
                    //     AlphabetForOutline.destroy();

                    //     // saveLevel();
                    //     controlCenterSaveLevel();

                    //     var tweenFinish = game.add.tween(letterImage.scale).to({
                    //         x: (0.5 * w) / letterImage.width,
                    //         y: (0.5 * w) / letterImage.width
                    //     }, 1000, Phaser.Easing.Default, true);

                    //     game.add.tween(letterImage.anchor).to({
                    //         x: 0.5,
                    //         y: 0.5
                    //     }, 1000, Phaser.Easing.Default, true);

                    //     game.add.tween(letterImage).to({
                    //         x: w / 4,
                    //         y: h / 2
                    //     }, 1000, Phaser.Easing.Default, true);

                    //     game.add.tween(AlphabetFor.scale).to({
                    //         x: (0.5 * w) / letterImage.width,
                    //         y: (0.5 * w) / letterImage.width
                    //     }, 1000, Phaser.Easing.Default, true);

                    //     game.add.tween(AlphabetFor.anchor).to({
                    //         x: 0.5,
                    //         y: 0.5
                    //     }, 1000, Phaser.Easing.Default, true);

                    //     game.add.tween(AlphabetFor).to({
                    //         x: (3 * w) / 4,
                    //         y: h / 2
                    //     }, 1000, Phaser.Easing.Default, true);

                    //     console.log('before tweenFinish');

                    //     tweenFinish.onComplete.add(function() {
                    //         console.log('tweenFinish onComplete');
                    //         playLetterSound(letterToSolve, 1);
                    //         game.time.events.add(1000, function() {
                    //             console.log('call next level');
                    //             playPuzzle();
                    //         }, this);
                    //     }, this);

                    // }, this);

                } else {
                    console.log('wrong');
                    negativeAudio.play();
                    mouthSprites.visible = false;
                    microphone.visible = false;
                    game.time.events.add(1500, function() {
                        // startMouth(letterToSolve);
                        if (recognize > 0) {
                            listenW3C(letterToSolve);
                        }
                    }, this);
                }
                console.log(event.results[0][0].transcript);
            } else {
                console.log('no result found');
                negativeAudio.play();
                mouthSprites.visible = false;
                microphone.visible = false;
                game.time.events.add(1500, function() {
                    // startMouth(letterToSolve);
                    if (recognize > 0) {
                        listenW3C(letterToSolve);
                    }
                }, this);
            }
        }


        if (recognize > 0) {
            game.time.events.add(5000, function() {
                // if (spoken == 1) {
                //     recognition.stop();
                //     listenW3C(letterToSolve);
                // }

                recognition.onerror = function() {
                    console.log('on error');
                    // recognition.stop();
                    negativeAudio.play();
                    mouthSprites.visible = false;
                    microphone.visible = false;
                    game.time.events.add(1500, function() {
                        // startMouth(letterToSolve);
                        listenW3C(letterToSolve);
                    }, this);
                };

            }, this);
        }

        // recognition.onnomatch = function() {
        //     console.log('no match found');
        //     negativeAudio.play();
        //     mouthSprites.visible = false;
        // microphone.visible = false;
        //     game.time.events.add(1500, function() {
        //         // startMouth(letterToSolve);
        //         if (recognize > 0) {
        //             listenW3C(letterToSolve);
        //         }
        //     }, this);
        // };

        // recognition.onerror = function() {
        //     console.log('on error');
        //     negativeAudio.play();
        //     mouthSprites.visible = false;
        //     microphone.visible = false;
        //     game.time.events.add(1500, function() {
        //         // startMouth(letterToSolve);
        //         if (recognize > 0) {
        //             listenW3C(letterToSolve);
        //         }
        //     }, this);
        // };

    }










































    function dragStop(tile) {
        stopHints();
        gameHintTimer.loop(tileHighlightDragHintDelay, function() {
            tileHighlight();
        }, this);
        gameHintTimer.start();
        if (Math.abs(tile.x - (tile.xPosition)) < 40) {
            if (Math.abs(tile.y - (tile.yPosition)) < 40) {

                var touchTime = (Date.now() - startTime) / 1000;
                touchTimeArray.push([touchTime, tile.index]);
                startTime = Date.now();
                console.log('touchTime ', touchTime);

                positiveAudio.play();
                tile.x = tile.xPosition;
                tile.y = tile.yPosition;
                tile.inputEnabled = false;
                console.log('correct');
                correctlyPlaced = correctlyPlaced + 1;
                displayAlphabetFor(letterToSolve, correctlyPlaced / 10);

                if (correctlyPlaced == 4) {
                    var transaction = {
                        "gameName": game.state.current,
                        "letterToSolve": letterToSolve,
                        "touchTime": touchTimeArray
                    };
                    postTransaction(transaction);

                    stopHints();

                    // success();

                    listenW3C(letterToSolve);

                }
            } else {
                negativeAudio.play()
                tile.x = tile.initialX;
                tile.y = tile.initialY;
                console.log('incorrect' + tile.x + " " + tile.initialX);
            }
        } else {
            negativeAudio.play()
            tile.x = tile.initialX;
            tile.y = tile.initialY;
            console.log('incorrect' + tile.x + " " + tile.initialX);
        }
    }


    function deviceMotionHandler(eventData) {
        if (cracked == 0) {
            var acceleration = eventData.accelerationIncludingGravity;

            var yAcceleration = acceleration.y;
            game.add.tween(egg).to({
                angle: ((yAcceleration)).toFixed(0) * 10
            }, 200, Phaser.Easing.Default, true);
        }
    }


    function playPuzzle() {

        letterToSolve = controlCenterGenerateLevel(skillBased);

        // cursors = game.input.keyboard.createCursorKeys();


        // if (typeof(back) != "undefined") {
        //     back.destroy();
        // }
        if (typeof(letterImage) != "undefined") {
            letterImage.destroy();
        }
        // if (typeof(backButton) != "undefined") {
        //     backButton.destroy();
        // }
        if (typeof(AlphabetFor) != "undefined") {
            AlphabetFor.destroy();
            AlphabetFor.height = 0;
        }

        var loader = loadLetterAssets(letterToSolve);
        loader.onLoadComplete.addOnce(function() {
            playPuzzleCreate();
        });

    }



    function playPuzzleCreate() {
        var letterBroken = 0;
        toTweenX = {};
        shakeSide = 1;
        quaking = 1;
        correctlyPlaced = 0;

        recognize = 0;

        letterImage = game.add.sprite(w / 2, h / 2, letterToSolve);
        if (typeof(egg) != "undefined") {
            egg.destroy();
        }
        egg = game.add.sprite(w / 2, 0, 'egg', 0);
        // scale = egg.width / w;
        scale = (0.35 * w) / egg.width;
        console.log(scale)
        back.scale.setTo(w / back.width, h / back.height);
        egg.scale.setTo(scale, scale);
        letterImage.scale.setTo(scale + 0.2, scale + 0.2);
        letterImage.x = egg.x - letterImage.width / 2;
        letterImage.y = h / 2 - letterImage.height / 2 + 50;
        letterImage.alpha = 0;
        letterWidth = letterImage.width;
        letterHeight = letterImage.height;

        egg.anchor.setTo(0.5, 0.75)

        cracked = 0;

        toTweenX.x = 0;
        toTweenX.obj = letterImage;
        for (var i = 1; i < 6; i++) {
            egg.animations.add('walk' + i, [i]);
        };
        var bounce = game.add.tween(egg);
        bounce.to({
            y: h / 2 + (egg.height / 4)
        }, 1000, Phaser.Easing.Bounce.Out);

        var quake = game.add.tween(toTweenX).to({
            x: 12
        }, 1000, Phaser.Easing.Exponential.Out);
        bounce.onComplete.add(function() {
            // letterImage.alpha = 1;
            if (window.DeviceMotionEvent) {
                window.addEventListener('devicemotion', deviceMotionHandler, false);
            }
        })
        bounce.start();

        if (typeof(finger) != "undefined") {
            finger.destroy();
        }
        finger = game.add.sprite(0, 0, 'finger');
        finger.visible = false;
        // finger.scale.setTo(0.2, 0.2);
        finger.width = 0.07 * w;
        finger.height = 0.19 * h;
        finger.alpha = 0.8;
        finger.anchor.setTo(0.25, 0);

        if (typeof(gameHintTimer) != "undefined") {
            gameHintTimer.destroy();
        }
        gameHintTimer = game.time.create(true);
        stopHints();
        gameHintTimer.loop(10000, function() {
            stopHints();
            hintTween = highlight(egg, 0.05, 300, 0, 1, finger, 1000);
            gameHintTimer.loop(eggBreakHintDelay, function() {
                hintTween = highlight(egg, 0.05, 300, 0, 1, finger, 1000);
            }, this);
            gameHintTimer.start();
        }, this);
        gameHintTimer.start();

        startTime = Date.now();
        touchTimeArray = [];

        // hintEggQuake = game.time.events.loop(20000, function() {
        //     hintEggQuake.delay = 5000;
        //     hintEggHighlight.stop();
        //     egg.scale = initEggScale;
        //     game.add.tween(egg).to({
        //         x: egg.x + 10
        //     }, 100, Phaser.Easing.Default, true, 0, 5, true);
        // }, this);

        c = 1;

        // function breakingLetter() {

        //     displayAlphabetFor(letterToSolve, 0);


        //     if (letterBroken === 0) {
        //         letterBroken = 1;

        //         letterImage.alpha = 0.25;
        //         tilesGroup = game.add.group();
        //         var randomLocations = [
        //             [-2, -1],
        //             [-2, 0],
        //             [-2, 1],
        //             [1, -1],
        //             [1, 0],
        //             [1, 1]
        //         ];
        //         randomLocations = shuffle(randomLocations);
        //         for (var i = 0; i < 6; i++) {
        //             var randomX = w / 2 + (randomLocations[i][0] * letterImage.width / 2.5);
        //             var randomY = h / 2 + (randomLocations[i][1] * letterImage.height / 3) - 50;
        //             console.log(randomY + " " + randomX)
        //             var tile = tilesGroup.create(randomX, randomY, letterToSolve);
        //             var xpos = (i % 2) * tile.width / 2;
        //             var ypos = (Math.floor(i / 2)) * tile.height / 3;
        //             var cropRect = new Phaser.Rectangle(xpos, ypos, tile.width / 2, tile.height / 3);
        //             tile.crop(cropRect);
        //             tile.inputEnabled = true;
        //             tile.input.enableDrag();
        //             tile.index = i;
        //             scaleY = (letterImage.height / 3) / tile.height;
        //             scaleX = (letterImage.width / 2) / tile.width;

        //             tile.scale.setTo(scaleX, scaleY);
        //             shakeSide = shakeSide * -1;
        //             tile.xPosition = (scaleX * xpos) + letterImage.x;
        //             tile.yPosition = (scaleY * ypos) + letterImage.y;
        //             // tile.xPosition = xpos + letterImage.x;
        //             // tile.yPosition = ypos + letterImage.y;
        //             tile.initialX = tile.x;
        //             tile.initialY = tile.y;
        //             tile.events.onInputUp.add(dragStop, this);
        //             tile.events.onInputDown.add(function() {
        //                 stopHints();
        //                 playLetterSound(letterToSolve, 1);
        //             }, this);
        //         }

        //         gameHintTimer.loop(eggBreakHintDelay, function() {
        //             tileHighlight();
        //         }, this);
        //         gameHintTimer.start();


        //     }
        // }

        function breakingLetter() {

            displayAlphabetFor(letterToSolve, 0);


            if (letterBroken === 0) {
                letterBroken = 1;

                letterImage.alpha = 0.25;
                tilesGroup = game.add.group();
                var randomLocations = [
                    [-2, -1],
                    [-2, 0],
                    [-2, 1],
                    [1, -1],
                    [1, 0],
                    [1, 1]
                ];
                randomLocations = shuffle(randomLocations);
                for (var i = 0; i < 4; i++) {
                    var randomX = w / 2 + (randomLocations[i][0] * letterImage.width / 2.5);
                    var randomY = h / 2 + (randomLocations[i][1] * letterImage.height / 2) - 50;
                    console.log(randomY + " " + randomX);
                    var tile = tilesGroup.create(randomX, randomY, letterToSolve);
                    var xpos = (i % 2) * tile.width / 2;
                    var ypos = (Math.floor(i / 2)) * tile.height / 2;
                    var cropRect = new Phaser.Rectangle(xpos, ypos, tile.width / 2, tile.height / 2);
                    tile.crop(cropRect);
                    tile.inputEnabled = true;
                    tile.input.enableDrag();
                    tile.index = i;
                    scaleY = (letterImage.height / 2) / tile.height;
                    scaleX = (letterImage.width / 2) / tile.width;

                    tile.scale.setTo(scaleX, scaleY);
                    shakeSide = shakeSide * -1;
                    tile.xPosition = (scaleX * xpos) + letterImage.x;
                    tile.yPosition = (scaleY * ypos) + letterImage.y;
                    // tile.xPosition = xpos + letterImage.x;
                    // tile.yPosition = ypos + letterImage.y;
                    tile.initialX = tile.x;
                    tile.initialY = tile.y;
                    tile.events.onInputUp.add(dragStop, this);
                    tile.events.onInputDown.add(function() {
                        stopHints();
                        playLetterSound(letterToSolve, 1);
                    }, this);
                }

                gameHintTimer.loop(eggBreakHintDelay, function() {
                    tileHighlight();
                }, this);
                gameHintTimer.start();


            }
        }

        function breakEgg() {
            crackAudio.play();

            if (egg.animations.currentFrame != null) {
                if (egg.animations.currentFrame.index < 5) {
                    egg.animations.play('walk' + c, 5, false);
                    c++;
                }
                // c = c + 1;
                // return 0;
            }
            if (egg.animations.currentFrame != null) {
                if (egg.animations.currentFrame.index == 5) {
                    // c = null;
                    egg.animations.currentFrame = NaN;

                    egg.destroy();

                    // letterImageInitWidth = letterImage.width;
                    // letterImageInitHeight = letterImage.height;

                    // scale = h / letterImage.height * 0.5;

                    // // scale = (0.8 * h) / letterImageInitHeight;

                    // letterImageFinalWidth = letterImageInitWidth * scale;
                    // letterImageFinalHeight = letterImageInitHeight * scale;

                    // var expand = game.add.tween(letterImage.scale).to({
                    //     x: scale,
                    //     y: scale
                    // }, 1000, Phaser.Easing.Default, true);
                    // var move = game.add.tween(letterImage).to({
                    //     x: (w / 2) - (letterImage.width / 2) - 50,
                    //     y: 10
                    //     // x: (w / 2) - (letterImageFinalWidth / 2),
                    //     // y: (h / 2) - (letterImageFinalHeight / 2)
                    // }, 1000, Phaser.Easing.Default, true);

                    var letterImageInitWidth = letterImage.width;
                    var letterImageInitHeight = letterImage.height;
                    scaleProportionOfHeight = 0.7;

                    // scale = h * scaleProportionOfHeight;

                    scale = 0.7 * h / letterImage.height;
                    newWidth = scale * letterImage.width;
                    newHeight = scale * letterImage.height;

                    console.log('height scale ', letterImage.height, scale, newWidth, newHeight);

                    var letterImageFinalWidth = letterImageInitWidth * scale;
                    var letterImageFinalHeight = letterImageInitHeight * scale;

                    var expand = game.add.tween(letterImage);

                    expand.to({
                        width: newWidth,
                        height: newHeight
                    }, 1000, Phaser.Easing.Bounce.Out).start();
                    var move = game.add.tween(letterImage);
                    // scale = h / letterImage.height * 0.5;
                    move.to({
                        x: (w / 2) - (letterImage.width),
                        y: (h * (1 - scaleProportionOfHeight)) / 2
                            // x: w / 2 - letterImageFinalWidth / 2,
                            // y: h / 2 - letterImageFinalHeight / 2
                    }, 1000, Phaser.Easing.Linear.None).start();

                    move.onComplete.add(function() {
                        quakeAudio.play();
                        playLetterSound(letterToSolve, 1);

                        quake.start();
                        quake.onComplete.add(function() {
                            quaking = 0;
                            egg.inputEnabled = false;
                            breakingLetter();
                        })
                    });
                }

            }
        }

        egg.inputEnabled = true;
        egg.events.onInputDown.add(function() {
            // c++;
            stopHints();
            cracked = 1;
            if (egg.angle != 0) {
                game.add.tween(egg).to({
                    angle: 0
                }, 200, Phaser.Easing.Default, true).onComplete.add(function() {
                    letterImage.alpha = 1;
                    if (c < 7) {
                        breakEgg();
                    }
                }, this);
            } else {
                letterImage.alpha = 1;
                if (c < 7) {
                    breakEgg();
                }
            }
        }, this);
        // shower(game);
    }

    gameEgg.prototype = {
        preload: function() {
            // game.load.image('A', 'asset/A.png');
            // game.load.image('AFor', 'asset/AFor.png');
            // game.load.image('AForOutline', 'asset/AForOutline.png');
            // game.load.image('B', 'asset/B.png');
            // game.load.image('BFor', 'asset/BFor.png');
            // game.load.image('BForOutline', 'asset/BForOutline.png');
            // game.load.image('C', 'asset/C.png');
            // game.load.image('D', 'asset/D.png');
            // game.load.image('E', 'asset/E.png');
            // game.load.image('CFor', 'asset/CFor.png');
            // game.load.image('CForOutline', 'asset/CForOutline.png');
            // game.load.image('DFor', 'asset/DFor.png');
            // game.load.image('DForOutline', 'asset/DForOutline.png');
            // game.load.image('EFor', 'asset/EFor.png');
            // game.load.image('EForOutline', 'asset/EForOutline.png');
            game.load.image('finger', 'asset/finger.png');
            game.load.spritesheet('egg', 'asset/3834-updated.png', 639, 1080, 6);
            // game.load.spritesheet('mouthSprites', 'asset/mouthSprites.png', 300, 300, 5);

            game.load.image('mouth1', 'asset/mouth1.png');
            game.load.image('mouth2', 'asset/mouth2.png');
            game.load.image('mouth3', 'asset/mouth3.png');
            game.load.image('mouth4', 'asset/mouth4.png');
            game.load.image('mouth5', 'asset/mouth5.png');
            game.load.image('mouth6', 'asset/mouth6.png');
            game.load.image('mouth7', 'asset/mouth7.png');
            game.load.image('mouth8', 'asset/mouth8.png');
            game.load.image('mouth9', 'asset/mouth9.png');
            game.load.image('mouth10', 'asset/mouth10.png');
            game.load.image('mouth11', 'asset/mouth11.png');
            game.load.image('mouth12', 'asset/mouth12.png');

            game.load.image('back', 'asset/crumpled_white_paper.jpg');
            game.load.audio('earthquake', 'asset/earthquake.wav');
            game.load.audio('crack', 'asset/crack.wav');
            game.load.audio('positive', 'asset/positive2.wav');
            game.load.audio('negative', 'asset/negativeAudio.wav');
            game.load.image('backButton', 'asset/backButton.png');
            game.load.image('microphone', 'asset/microphone.png');

        },
        create: function() {
            crackAudio = game.add.audio('crack');
            quakeAudio = game.add.audio('earthquake');
            negativeAudio = game.add.audio('negative');
            positiveAudio = game.add.audio('positive');

            back = game.add.sprite(0, 0, 'back');

            mouthSprites = game.add.sprite(w / 6, h / 2, 'mouth2');
            mouthSprites.anchor.setTo(0.5, 0.5);
            // mouthSprites.scale.setTo(0.2*w/mouthSprites.width, 0.2*w/mouthSprites.width);
            mouthSprites.width = 0.2 * w;
            mouthSprites.height = 0.2 * h;
            // mouthSprites.animations.play('walk', 20, true);
            mouthSprites.visible = false;

            microphone = game.add.sprite((5 * w) / 6, h / 2, 'microphone');
            microphone.anchor.setTo(0.5, 0.5);
            // microphone.scale.setTo(-(0.15 * w / microphone.width), 0.84 * h / microphone.height);
            microphone.scale.setTo(-(0.5 * h / microphone.height), 0.5 * h / microphone.height);
            microphone.visible = false;

            backButton = game.add.sprite((0.1 * w), (0.12 * h), 'backButton');
            backButton.inputEnabled = true;
            backButton.anchor.set(0.5);
            backButton.scale.setTo((0.08 * w) / backButton.width, (0.08 * h) / backButton.height);
            backButton.events.onInputDown.add(function() {
                if (user.data.goneBackward == 1) {
                    user.data.flow[user.data.menuClicked] = ["GameEgg", "GameSelect", "alphabetTracingTraining", "alphabetTracingTest"];
                }
                Menu(game);
                game.state.start('Menu');
            }, this);

            playPuzzle();

            // recognize = 0;

            // listenW3C("A");
        },
        update: function() {
            if (quaking === 1) {
                shakeSide = shakeSide * -1;
                if (typeof(toTweenX) != "undefined") {
                    if (typeof(toTweenX.obj) != "undefined") {
                        toTweenX.obj.x = toTweenX.obj.x - (toTweenX.x * shakeSide);
                    }
                }
            }
            if (typeof(AlphabetFor) != "undefined" && AlphabetFor._frame != null) {
                AlphabetFor.updateCrop();
            }
            // ADrawUpdate();
        },
        render: function() {
            // game.debug.cameraInfo(game.camera, 32, 32);

        }
    }
    game.state.add("GameEgg", gameEgg);
}
