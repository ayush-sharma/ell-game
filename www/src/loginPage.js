loginPage = function(game) {
    console.log('loginPage called');
    login_page = function() {};

    function clearText(game) {
        usernameBox.destroy;
        passwordBox.destroy;
        submitBox.destroy;
        usernameText.destroy;
        passwordText.destroy;
    }

    login_page.prototype = {

        preload: function() {

            // Here we load the assets required for our preloader (in game case a 
            // background and a loading bar)
            game.load.image('logo', 'asset/phaser.png');
            game.load.image('textInput', 'asset/textInput.png');
            game.load.image('submit', 'asset/submit.png');
        },

        create: function() {
            // Add logo to the center of the stage
            backGroundImage = game.add.sprite(
                game.world.centerX, // (centerX, centerY) is the center coordination
                game.world.centerY,
                'logo');
            // backGroundImage = game.logo;
            // Set the anchor to the center of the sprite
            backGroundImage.anchor.setTo(0.5, 0.5);
            backGroundImage.width = w;
            backGroundImage.height = h;

            usernameBox = game.add.sprite(10, 10, 'textInput');
            usernameBox.scale.setTo(0.75, 0.75);

            passwordBox = game.add.sprite(10, 100, 'textInput');
            passwordBox.scale.setTo(0.75, 0.75);

            submitBox = game.add.sprite(100, 200, 'submit');
            // submitBox.scale.setTo(0.75, 0.75);

            var usernameInput = document.createElement("input");
            usernameInput.id = "username"
            usernameInput.type = "text";
            usernameInput.style.cssText = "position:absolute; left:-1px; top: -1px; width:1px; height:1px; opacity:0";
            document.body.appendChild(usernameInput);

            var passwordInput = document.createElement("input");
            passwordInput.id = "password";
            passwordInput.type = "password";
            passwordInput.style.cssText = "position:absolute; left:-1px; top: -1px; width:1px; height:1px; opacity:0";
            document.body.appendChild(passwordInput);

            usernameBox.inputEnabled = true;
            usernameBox.events.onInputDown.add(function() {
                usernameInput.focus();
            }, this);

            passwordBox.inputEnabled = true;
            passwordBox.events.onInputDown.add(function() {
                passwordInput.focus();
            }, this);

            backGroundImage.inputEnabled = true;
            backGroundImage.events.onInputDown.add(function() {
                usernameInput.blur();
                passwordInput.blur();
            }, this);

            usernameText = game.add.text(10, 10, "", {
                font: "32px Arial",
                fill: "#ff0044",
                wordWrap: true,
                align: "center"
            });

            passwordText = game.add.text(10, 100, "", {
                font: "32px Arial",
                fill: "#ff0044",
                wordWrap: true,
                align: "center"
            });

            loginResult = game.add.text(w / 2, h / 2, "", {
                font: "32px Arial",
                fill: "#ff0044",
                wordWrap: true,
                align: "center"
            });

            submitBox.inputEnabled = true;
            submitBox.events.onInputDown.add(function() {
                usernameInput.blur();
                passwordInput.blur();
                var username = document.getElementById("username").value;
                var password = document.getElementById("password").value;
                var res = validateUser(false, 'logIn', username, password);
                console.log('res val', res);

                if (res == "validation failed") {
                    console.log('validation failed');
                    clearText(game);
                    loginResult.text = "validation failed";
                    document.getElementById("username").value = '';
                    document.getElementById("password").value = '';
                } else {
                    if (res == "0") {
                        console.log('invalid password');

                    } else if (res == "-1") {
                        console.log('username does not exist');
                        clearText(game);
                        loginResult.text = "username does not exist";
                    } else if (res == "") {
                        clearText(game);
                        loginResult.text = "error while signing in";
                    } else if (res == "Unexpected Error") {
                        clearText(game);
                        loginResult.text = "Unexpected Error.. Try Again !!";
                    }else {
                        console.log('logged in successfully');
                        clearText(game);
                        loginResult.text = "logged in successfully";
                        saveUserData(res);
                        Menu(game);
                        game.state.start("Menu");
                    }

                    document.getElementById("username").value = '';
                    document.getElementById("password").value = '';
                }

            }, this);

        },

        gameResized: function(width, height) {

            // game could be handy if you need to do any extra processing if the 
            // game resizes. A resize could happen if for example swapping 
            // orientation on a device or resizing the browser window. Note that 
            // game callback is only really useful if you use a ScaleMode of RESIZE 
            // and place it inside your main game state.

        },

        update: function() {
            usernameText.text = document.getElementById("username").value;
            passwordText.text = document.getElementById("password").value.replace(/./g, "*");

        }
    }

    game.state.add("loginPage", login_page);
}
