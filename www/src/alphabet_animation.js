alphabetAnimation = function(game, letter) {

    var thisGameCounter = 0;
    var startTime;
    var touchTimeArray;

    // function alphabetAnimationGenerateLevel() {

    //     var letterVSCount = user.data.letterStates["write"];
    //     letterVSCount = getMenuAlphabets(letterVSCount, 1);

    //     var lettersSorted = Object.keys(letterVSCount).sort(function(a, b) {
    //         return letterVSCount[a] - letterVSCount[b]
    //     });

    //     postUserData();

    //     var threshold = user.data.threshholdMulti["alphabetAnimation"];

    //     if (thisGameCounter >= threshold) {
    //         alphabetTraceRef(game);
    //         game.state.start("alphabetTraceRef", true, true);
    //         return 0;
    //     }

    //     var letter = lettersSorted[0];
    //     thisGameCounter += 1;

    //     // G K L M N O P Q Z

    //     // if (letter == "G" || letter == "K" || letter == "L" || letter == "M" || letter == "N" || letter == "O" || letter == "P" || letter == "Q" || letter == "Z") {
    //     //     user.data.letterStates["write"][letter] += 1;
    //     //     saveUserData(user.data);
    //     //     alphabetAnimationGenerateLevel();
    //     // }

    //     // user.data.letterStates["write"][letter] += 1;
    //     // saveUserData(user.data);

    //     return letter;

    // }

    // function saveLevel() {
    //     // console.log('&&&&&&&&&&&&& ', draw_alphabet);
    //     user.data.letterStates["write"][draw_alphabet] += 1;
    //     saveUserData(user.data);
    // }


    Game = {};

    alphabet_animation = function() {};

    function drawUpdate(graphics, strokeWidth, strokeColor, alphabet_tween_data, index) {

        graphics.clear();
        graphics.lineStyle(strokeWidth, strokeColor);

        graphics.moveTo(alphabet_tween_data["pointsArray"][0][0], alphabet_tween_data["pointsArray"][0][1]);

        for (var j = 0; j < alphabet_tween_data[index]; j++) {
            graphics.lineTo(alphabet_tween_data["pointsArray"][j][0], alphabet_tween_data["pointsArray"][j][1]);
        }
    }

    function changeDiamondPosSetHist(index, alphabet_tween_data) {
        alphabet_tween_data["dragIndex"] = index;
        dia.position.setTo(alphabet_tween_data["pointsArray"][index][0], alphabet_tween_data["pointsArray"][index][1]);
    }

    function findClosestPoint(input_x, input_y, alphabet_tween_data) {
        var allDistace = [];
        for (var i = alphabet_tween_data["dragIndex"]; i < alphabet_tween_data["pointsArray"].length; i++) {
            var x = alphabet_tween_data["pointsArray"][i][0];
            var y = alphabet_tween_data["pointsArray"][i][1];

            var distance = Math.sqrt(((input_x - x) * (input_x - x)) + ((input_y - y) * (input_y - y)));
            allDistace.push(distance);
        }
        var min = allDistace[0];
        var minIndex = 0;

        for (var i = 1; i < allDistace.length; i++) {
            if (allDistace[i] < min) {
                minIndex = i;
                min = allDistace[i];
            }
        }
        return [min, minIndex + alphabet_tween_data["dragIndex"]];
    }

    function diamondBeingDragged(alphabet_tween_data) {

        // alphabet_tween_data["dragIndex"]++;
        // dia.position.setTo(alphabet_tween_data["pointsArray"][alphabet_tween_data["dragIndex"]][0], alphabet_tween_data["pointsArray"][alphabet_tween_data["dragIndex"]][1]);

        var input_x = game.input.x;
        var input_y = game.input.y;

        var closestData = findClosestPoint(input_x, input_y, alphabet_tween_data);
        var minDistace = closestData[0];
        var minIndex = closestData[1];

        if (minDistace < 100) {
            changeDiamondPosSetHist(minIndex, alphabet_tween_data);
        } else {
            dragged = 0;
            negativeAudio.play();
        }
    }

    function completePath(graphics, alphabet_tween_data) {

        if (alphabet_tween_data["dragIndex"] > alphabet_tween_data["pointsArray"].length - 4) {
            changeDiamondPosSetHist(alphabet_tween_data["pointsArray"].length - 1, alphabet_tween_data);
            drawUpdate(graphics, Game.dragStrokeWidth, Game.dragStrokeColor, alphabet_tween_data, "dragIndex");
            positiveAudio.play();
            dragged = 0;
            var touchTime = (Date.now() - startTime) / 1000;
            startTime = Date.now();
            touchTimeArray.push(touchTime);
            current_dia++;
            alphabetForTween = displayAlphabetFor(draw_alphabet, current_dia / graphics_group.children.length);
            if (current_dia <= graphics_group.children.length - 1) {
                animate_diamond(current_dia);
            }
        }
    }

    function scaffoldDrawUpdate() {

        animateAlphabet();

        if (Game.all_animation_stopped == 1 && current_dia > -1 && current_dia < drag_graphics_group.children.length) {
            var i = current_dia;

            drawUpdate(drag_graphics_group.children[i], Game.dragStrokeWidth, Game.dragStrokeColor, alpha_tween_data[i], "dragIndex");

            if (dragged == 1) {
                diamondBeingDragged(alpha_tween_data[i]);
                completePath(drag_graphics_group.children[i], alpha_tween_data[i]);
            }

            if (Game.all_animation_stopped == 1 && current_dia == drag_graphics_group.children.length) {

                success();

                // stopHints();

                // Game.all_animation_stopped = 0;
                // dia.inputEnabled = false;
                // dia.visible = false;
                // graphics_group.removeAll();
                // var transaction = {
                //     "gameName": game.state.current,
                //     "draw_alphabet": draw_alphabet,
                //     "touchTime": touchTimeArray
                // };
                // postTransaction(transaction);

                // saveLevel();

                // alphabetForTween.onComplete.add(function() {
                //     alphabetWhole.alpha = 1;
                //     graphics_group.destroy();
                //     drag_graphics_group.destroy();
                //     AlphabetForOutline.destroy();

                //     var tweenFinish = game.add.tween(alphabetWhole.scale).to({
                //         x: (0.6 * h) / alphabetWhole.height,
                //         y: (0.6 * h) / alphabetWhole.height
                //     }, 1000, Phaser.Easing.Default, true);

                //     game.add.tween(alphabetWhole.anchor).to({
                //         x: 0.5,
                //         y: 0.5
                //     }, 1000, Phaser.Easing.Default, true);

                //     game.add.tween(alphabetWhole).to({
                //         x: w / 4,
                //         y: h / 2
                //     }, 1000, Phaser.Easing.Default, true);



                //     game.add.tween(AlphabetFor.scale).to({
                //         x: (0.6 * h) / alphabetWhole.height,
                //         y: (0.6 * h) / alphabetWhole.height
                //     }, 1000, Phaser.Easing.Default, true);

                //     game.add.tween(AlphabetFor.anchor).to({
                //         x: 0.5,
                //         y: 0.5
                //     }, 1000, Phaser.Easing.Default, true);

                //     game.add.tween(AlphabetFor).to({
                //         x: (3 * w) / 4,
                //         y: h / 2
                //     }, 1000, Phaser.Easing.Default, true);

                //     // console.log('before tweenFinish');

                //     tweenFinish.onComplete.add(function() {
                //         // console.log('tweenFinish onComplete');
                //         playLetterSound(draw_alphabet, 1);
                //         game.time.events.add(1500, function() {
                //             // console.log('game over');
                //             // game.time.events.add(2000, function() {
                //             // alphabetTraceRef(game);
                //             // game.state.start('alphabetTraceRef');
                //             // alphabetAnimation(game);
                //             // game.state.start("alphabetAnimation");
                //             stopHints();
                //             draw_alphabet = 0;
                //             alphabetAnimationCreate();
                //             // }, this);
                //         }, this);
                //     }, this);

                // }, this);








                // game.time.events.add(1000, function() {
                //     console.log('game over');
                //     speakTTS("very good");
                //     game.time.events.add(2000, function() {
                //         game.state.start('alphabetAnimation');
                //     }, this);
                // }, this);

            }
        }
    }

    function success() {
        stopHints();

        Game.all_animation_stopped = 0;
        dia.inputEnabled = false;
        dia.visible = false;
        graphics_group.removeAll();
        var transaction = {
            "gameName": game.state.current,
            "draw_alphabet": draw_alphabet,
            "touchTime": touchTimeArray
        };
        postTransaction(transaction);

        // saveLevel();

        alphabetForTween.onComplete.add(function() {
            alphabetWhole.alpha = 1;
            graphics_group.destroy();
            drag_graphics_group.destroy();
            AlphabetForOutline.destroy();



            var finalLetterWidth = (0.8*alphabetWhole.width);
            var finalLetterHeight = (0.8*alphabetWhole.height);

            // console.log('finalHeight #######', finalHeight);
            // console.log('finalWidth #######', finalWidth);

            var finalAlphabetForWidth;
            var finalAlphabetForHeight;

            if(AlphabetFor.width > AlphabetFor.height){
                finalAlphabetForWidth = finalLetterWidth;
                finalAlphabetForHeight = AlphabetFor.height * (finalAlphabetForWidth/AlphabetFor.width);
            }else{
                finalAlphabetForHeight = finalLetterHeight;
                finalAlphabetForWidth = AlphabetFor.width * (finalAlphabetForHeight/AlphabetFor.height);
            }



            // var tweenFinish = game.add.tween(alphabetWhole.scale).to({
            //     x: (0.6 * h) / alphabetWhole.height,
            //     y: (0.6 * h) / alphabetWhole.height
            // }, 1000, Phaser.Easing.Default, true);

            var tweenFinish = game.add.tween(alphabetWhole).to({
                width: finalLetterWidth,
                height: finalLetterHeight
            }, 1000, Phaser.Easing.Default, true);

            game.add.tween(alphabetWhole.anchor).to({
                x: 0.5,
                y: 0.5
            }, 1000, Phaser.Easing.Default, true);

            game.add.tween(alphabetWhole).to({
                x: w / 4,
                y: h / 2
            }, 1000, Phaser.Easing.Default, true);



            // game.add.tween(AlphabetFor.scale).to({
            //     x: (0.6 * h) / alphabetWhole.height,
            //     y: (0.6 * h) / alphabetWhole.height
            // }, 1000, Phaser.Easing.Default, true);

            game.add.tween(AlphabetFor).to({
                width: finalAlphabetForWidth,
                height: finalAlphabetForHeight
            }, 1000, Phaser.Easing.Default, true);

            game.add.tween(AlphabetFor.anchor).to({
                x: 0.5,
                y: 0.5
            }, 1000, Phaser.Easing.Default, true);

            game.add.tween(AlphabetFor).to({
                x: (3 * w) / 4,
                y: h / 2
            }, 1000, Phaser.Easing.Default, true);

            // console.log('before tweenFinish');

            tweenFinish.onComplete.add(function() {
                // console.log('tweenFinish onComplete');
                playLetterSound(draw_alphabet, 1);
                game.time.events.add(1500, function() {
                    // console.log('game over');
                    // game.time.events.add(2000, function() {
                    // alphabetTraceRef(game);
                    // game.state.start('alphabetTraceRef');
                    // alphabetAnimation(game);
                    // game.state.start("alphabetAnimation");
                    // stopHints();
                    // draw_alphabet = 0;
                    // alphabetTraceRef(game, letter);
                    // game.state.start('alphabetTraceRef');

                    myCounter++;
                    alphabetAnimationCreate();
                    // }, this);
                }, this);
            }, this);

        }, this);
    }

    function alphabetAnimationCreate() {

        // if (draw_alphabet == "G" || draw_alphabet == "K" || draw_alphabet == "L" || draw_alphabet == "N" || draw_alphabet == "Z") {
        //     alphabetTraceRef(game, letter);
        //     game.state.start('alphabetTraceRef');
        //     // alphabetAnimationCreate();
        //     // alphabetAnimationGenerateLevel();
        // }

        // draw_alphabet = alphabetAnimationGenerateLevel();

        draw_alphabet = Object.keys(inferringParametersGroup)[myCounter + 1];

        totalSegments = inferringParametersGroup[draw_alphabet][0];
        segmentNumber = null;

        var loader = new Phaser.Loader(game);
        loader.image(draw_alphabet, 'asset/' + draw_alphabet + '.png');
        loader.image(draw_alphabet + 'For', 'asset/' + draw_alphabet + 'For.png');
        loader.image(draw_alphabet + 'ForOutline', 'asset/' + draw_alphabet + 'ForOutline.png');
        for (var i = 0; i < totalSegments; i++) {
            loader.image(draw_alphabet + '_' + i.toString(), 'asset/' + draw_alphabet + '_' + i.toString() + '.png');
        }
        loader.audio('letter' + draw_alphabet, 'asset/Name' + draw_alphabet + '.wav');
        loader.start();

        loader.onLoadComplete.addOnce(function() {
            if (draw_alphabet != 0) {
                playLetterSound(draw_alphabet, 1);
                ScaffoldDrawCreate(draw_alphabet, 700);
            }
        });
    }


    function setStage(animateTime) {
        animation_no = 0;
        animation_time = animateTime;

        breakPoints = [];

        Game.all_animation_stopped = 0;

        if (typeof(tracingBack) != "undefined") {
            tracingBack.destroy();
        }
        if (typeof(alphabetWhole) != "undefined") {
            alphabetWhole.destroy();
        }
        if (typeof(graphics_group) != "undefined") {
            graphics_group.destroy();
        }
        if (typeof(dia) != "undefined") {
            dia.destroy();
        }
        if (typeof(finger) != "undefined") {
            finger.destroy();
        }
        if (typeof(backButton) != "undefined") {
            backButton.destroy();
        }
        if (typeof(repeatAudioButton) != "undefined") {
            repeatAudioButton.destroy();
        }
        if (typeof(AlphabetFor) != "undefined") {
            AlphabetFor.destroy();
            AlphabetFor.height = 0;
        }

        tracingBack = game.add.sprite(0, 0, 'tracingBack');
        tracingBack.scale.setTo(w / tracingBack.width, h / tracingBack.height);

        alphabetWhole = game.add.sprite(0, 0, draw_alphabet);

        backButton = game.add.sprite((0.1 * w), (0.12 * h), 'backButton');
        backButton.inputEnabled = true;
        backButton.anchor.set(0.5);
        backButton.scale.setTo((0.08 * w) / backButton.width, (0.08 * h) / backButton.height);
        backButton.events.onInputDown.add(function() {
            Menu(game);
            game.state.start('Menu');
        }, this);

        // repeatAudioButton = game.add.sprite((0.9 * w), (0.1 * h), 'repeatAudioButton');
        // repeatAudioButton.inputEnabled = true;
        // repeatAudioButton.anchor.set(0.5);
        // repeatAudioButton.scale.setTo((0.2 * w) / repeatAudioButton.width, (0.25 * h) / repeatAudioButton.height);
        // repeatAudioButton.events.onInputDown.add(function() {
        //     playLetterSound(draw_alphabet, 1);
        // }, this);

        repeatAudioButton = game.add.sprite((0.9 * w), (0.1 * h), 'repeatAudioButton');
        repeatAudioButton.inputEnabled = true;
        repeatAudioButton.anchor.set(0.5);
        // repeatAudioButton.scale.setTo((0.1 * w) / repeatAudioButton.width, (0.1 * h) / repeatAudioButton.height);
        repeatAudioButton.scale.setTo((0.08 * w) / repeatAudioButton.width, (0.08 * w) / repeatAudioButton.width);
        repeatAudioButton.events.onInputDown.add(function() {
            playLetterSound(draw_alphabet, 1);
        }, this);


        // forwardButton = game.add.sprite((0.9 * w), (0.3 * h), 'backButton');
        // forwardButton.inputEnabled = true;
        // forwardButton.anchor.set(0.5);
        // forwardButton.scale.setTo(-(0.08 * w) / forwardButton.width, (0.08 * h) / forwardButton.height);
        // forwardButton.events.onInputDown.add(function() {
        //     success();
        // }, this);

        alphabetScale = {
            x: ((0.8 * h) / alphabetWhole.height),
            y: ((0.8 * h) / alphabetWhole.height)
        };

        alphabetWhole.scale.setTo(alphabetScale.x, alphabetScale.y);
        alphabetWhole.alpha = 0.5;

        Game.graphics_x_offset = Math.round(w / 2 - (alphabetWhole.width / 2));
        Game.graphics_y_offset = Math.round(h / 2 - (alphabetWhole.height / 2));
        alphabetWhole.position.setTo(Game.graphics_x_offset, Game.graphics_y_offset);

        // if (typeof(f1) != "undefined") {
        //     f1.destroy();
        // }
        // if (typeof(f2) != "undefined") {
        //     f2.destroy();
        // }
        // if (typeof(f3) != "undefined") {
        //     f3.destroy();
        // }

        // if (segmentNumber == null) {
        //     f1 = game.add.sprite(Game.graphics_x_offset, Game.graphics_y_offset, draw_alphabet + '_0');
        //     f1.scale.setTo(alphabetScale.x, alphabetScale.y);
        //     f2 = game.add.sprite(Game.graphics_x_offset, Game.graphics_y_offset, draw_alphabet + '_1');
        //     f2.scale.setTo(alphabetScale.x, alphabetScale.y);
        //     f3 = game.add.sprite(Game.graphics_x_offset, Game.graphics_y_offset, draw_alphabet + '_2');
        //     f3.scale.setTo(alphabetScale.x, alphabetScale.y);
        // } else {
        //     f1 = game.add.sprite(Game.graphics_x_offset, Game.graphics_y_offset, draw_alphabet + '_' + (segmentNumber - 1).toString());
        //     f1.scale.setTo(alphabetScale.x, alphabetScale.y);
        // }

        graphics_group = game.add.group();
        drag_graphics_group = game.add.group();

        graphics_group.alpha = 0.2;

        current_dia = 0;
        if (segmentNumber != null) {
            current_dia = segmentNumber - 1;
        }
        dragged = 0;

        dia = game.add.button(0, 0, 'diamond');
        dia.visible = false;
        dia.inputEnabled = true;
        // dia.scale.set(0.05);
        dia.scale.setTo((0.16 * w) / dia.width, (0.16 * h) / dia.height);

        dia.anchor.set(0.5);

        if (typeof(finger) != "undefined") {
            finger.destroy();
        }

        finger = game.add.sprite(0, 0, 'finger');
        finger.visible = false;
        // finger.scale.setTo(0.2, 0.2);
        finger.width = 0.07 * w;
        finger.height = 0.19 * h;
        finger.alpha = 0.8;
        finger.anchor.setTo(0.25, 0);

        dia.events.onInputDown.add(function() {
            dragged = 1;
            stopHints();
        }, this);

        dia.events.onInputUp.add(function() {
            dragged = 0;
            stopHints();
            gameHintTimer.loop(5000, function() {
                traceHint(finger);
            }, this);
            gameHintTimer.start();
        }, this);

        dia.events.onInputOut.add(function() {
            dragged = 0;
            stopHints();
            gameHintTimer.loop(5000, function() {
                traceHint(finger);
            }, this);
            gameHintTimer.start();
        }, this);

        startTime = Date.now();
        touchTimeArray = [];

        displayAlphabetFor(draw_alphabet, 0);
    }

    function ScaffoldDrawCreate(draw_alphabet, animateTime) {

        Game.dragStrokeWidth = 3;
        Game.dragStrokeColor = 0x000000;
        Game.animationStrokeWidth = 2;
        Game.animationStrokeColor = 0xffffff;

        setStage(animateTime);
        var inferringParameters = inferringParametersGroup[draw_alphabet];

        if (typeof draw_alphabet != 'undefined') {
            getAlphabetData(draw_alphabet, alphabetScale, animation_time, true, totalSegments, segmentNumber, inferringParameters);

        }

    }




    alphabet_animation.prototype = {
        preload: function() {

            game.load.image('finger', 'asset/finger.png');
            game.load.image('diamond', 'asset/Egg0.png');

            game.load.image('tracingBack', 'asset/tracingbg.png');

            game.load.audio('positive', 'asset/positive2.wav');
            game.load.audio('negative', 'asset/negativeAudio.wav');
            game.load.image('backButton', 'asset/backButton.png');
            game.load.image('repeatAudioButton', 'asset/repeatAudioButton.png');

        },

        create: function() {
            AAudio = game.add.audio('letterA');
            BAudio = game.add.audio('letterB');
            CAudio = game.add.audio('letterC');
            DAudio = game.add.audio('letterD');
            EAudio = game.add.audio('letterE');
            negativeAudio = game.add.audio('negative');
            positiveAudio = game.add.audio('positive');

            // prev_draw_alphabet = 0;

            // prev_draw_alphabet = Object.keys(inferringParametersGroup)[Object.keys(inferringParametersGroup).length - 2];

            // K O Q

            // P Z N L G K

            // draw_alphabet = letter;

            myCounter = -1;

            alphabetAnimationCreate();


            // draw_alphabet = alphabetAnimationGenerateLevel();

            // totalSegments = 3;
            // segmentNumber = null;

            // if (draw_alphabet == "C") {
            //     totalSegments = 1;
            // } else if (draw_alphabet == "D") {
            //     totalSegments = 2;
            // } else if (draw_alphabet == "E") {
            //     totalSegments = 4;
            // }

            // var loader = new Phaser.Loader(game);
            // loader.image(draw_alphabet, 'asset/' + draw_alphabet + '.png');
            // loader.image(draw_alphabet + 'For', 'asset/' + draw_alphabet + 'For.png');
            // loader.image(draw_alphabet + 'ForOutline', 'asset/' + draw_alphabet + 'ForOutline.png');
            // for (var i = 0; i < totalSegments; i++) {
            //     loader.image(draw_alphabet + '_' + i.toString(), 'asset/' + draw_alphabet + '_' + i.toString() + '.png');
            // }
            // loader.audio('letter' + draw_alphabet, 'asset/Name' + draw_alphabet + '.wav');
            // loader.start();

            // loader.onLoadComplete.addOnce(function() {
            //     if (draw_alphabet != 0) {
            //         playLetterSound(draw_alphabet, 1);
            //         ScaffoldDrawCreate(draw_alphabet, 2000);
            //     }
            // });

            // if (draw_alphabet != "C" && draw_alphabet != 0) {
            // if (draw_alphabet != 0) {
            //     playLetterSound(draw_alphabet, 1);
            //     ScaffoldDrawCreate(draw_alphabet, 2000);
            // }
            // qw = we;

        },

        update: function() {

            if (typeof(draw_alphabet) != "undefined" && draw_alphabet != 0) {
                scaffoldDrawUpdate();
            }
            if (typeof(AlphabetFor) != "undefined" && AlphabetFor._frame != null) {
                AlphabetFor.updateCrop();
            }
        }


    }

    game.state.add("alphabetAnimation", alphabet_animation);
}

inferringParametersGroup = {
    // //number of segments
    // //transpose for each segment
    // //smoothing for each segment
    // // Invert axis
    // // starting point
    // "Dnew": [2, [1, 1],
    //     [3, 5],
    //     [0, 0],
    //     [0, 0]
    // ],
    "A": [3, [1, 1, 1],
        [3, 3, 3],
        [0, 0, 0],
        [0, 0, 0]
    ],
    "B": [3, [1, 1, 1],
        [3, 3, 5],
        [0, 0, 0],
        [0, 0, 0]
    ],
    "C": [1, [1],
        [6],
        [0],
        [1]
    ],
    "D": [2, [1, 1],
        [3, 5],
        [0, 0],
        [0, 0]
    ],
    "E": [4, [1, 1, 1, 1],
        [3, 3, 5, 3],
        [0, 0, 0, 0],
        [0, 0, 0, 0]
    ],
    "F": [3, [1, 1, 1],
        [3, 3, 3],
        [0, 0, 0],
        [0, 0, 0]
    ],
    "G": [3, [1, 0, 1],
        [5, 3, 3],
        [0, 1, 0],
        [5, 3, 0]
    ],
    "H": [3, [1, 1, 1],
        [3, 3, 3],
        [0, 0, 0],
        [0, 0, 0]
    ],
    "I": [1, [1],
        [3],
        [0],
        [0]
    ],
    "J": [1, [1],
        [5],
        [0],
        [0]
    ],
    "K": [3, [1, 1, 1],
        [3, 3, 3],
        [0, 0, 0],
        [0, 1, 0]
    ],
    "L": [2, [1, 1],
        [3, 3],
        [0, 0],
        [0, 0]
    ],
    "M": [4, [0, 1, 1, 1],
        [3, 3, 3, 3],
        [1, 0, 0, 0],
        [0, 0, 3, 0]
    ],
    "N": [3, [1, 1, 1],
        [3, 3, 3],
        [0, 0, 0],
        [0, 1, 2]
    ],
    "O": [1, [0],
        [3],
        [0],
        [0]
    ],
    "P": [2, [0, 1],
        [5, 5],
        [1, 0],
        [0, 0]
    ],
    "Q": [2, [1, 1],
        [5, 3],
        [0, 0],
        [0, 0]
    ],
    "R": [3, [1, 1, 1],
        [5, 3, 3],
        [0, 0, 0],
        [0, 0, 0]
    ],
    "S": [1, [1],
        [3],
        [0],
        [1]
    ],
    "T": [2, [1, 1],
        [6, 3],
        [0, 0],
        [0, 0]
    ],
    "U": [1, [0],
        [3],
        [1],
        [0]
    ],
    "V": [2, [1, 1],
        [3, 3],
        [0, 0],
        [0, 3]
    ],
    "W": [4, [1, 1, 1, 1],
        [3, 3, 3, 3],
        [0, 0, 0, 0],
        [0, 3, 0, 3]
    ],
    "X": [2, [1, 1],
        [3, 3],
        [0, 0],
        [0, 1]
    ],
    "Y": [3, [1, 1, 0],
        [3, 3, 3],
        [0, 0, 1],
        [0, 1, 0]
    ],
    "Z": [3, [1, 1, 1],
        [3, 3, 3],
        [0, 0, 0],
        [0, 1, 3]
    ],
    "nBulky": [1, [0],
        [3],
        [1],
        [3]
    ],
    "n": [2, [1, 0],
        [3, 3],
        [0, 1],
        [0, 3]
    ]
}
