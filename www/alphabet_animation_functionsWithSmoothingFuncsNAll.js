getAlphabetData = function(alphabet, alphabetScale, animation_time, calledFromTraceGame, alphabet_data, segmentNumber, inferringParameters) {
    // console.log('in getAlphabetData %5', alphabet, alphabetScale, animation_time, calledFromTraceGame, alphabet_data, segmentNumber);

    // console.log('alphabet ', alphabet);

    alpha_tween_data = {};
    var alphabetTweenManager = [];
    var iStart = 0;

    if (typeof(segmentNumber) != "undefined" && segmentNumber != null) {
        // console.log('chaning iStart');
        iStart = segmentNumber - 1;
        alphabet_data = segmentNumber;
    }

    for (var i = iStart; i < alphabet_data; i++) {
        // console.log('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`');
        var g = game.add.graphics(0, 0);
        graphics_group.add(g);

        if (calledFromTraceGame != false) {
            var g_drag = game.add.graphics(0, 0);
            drag_graphics_group.add(g_drag);
        }

        if (typeof(segmentNumber) != "undefined" && segmentNumber != null) {
            var g_drag = game.add.graphics(0, 0);
            drag_graphics_group.children.push(g_drag);
        }

        // console.log('i ', i);
        var pointsArray = [];
        var pointsArray = getAlphabetPoints(alphabet, i, alphabetScale, inferringParameters);

        // console.log('pointsArray ', i, pointsArray.length, pointsArray[0].length);

        alpha_tween_data[i] = {
            "pointsArray": pointsArray,
            "dragIndex": 0,
            "dispIndex": 0
        };

        if (calledFromTraceGame != false) {
            var tween = game.add.tween(alpha_tween_data[i]).to({
                dispIndex: alpha_tween_data[i].pointsArray.length - 1
            }, animation_time, Phaser.Easing.Default);
            if (alphabetTweenManager.length > 0) {
                alphabetTweenManager[alphabetTweenManager.length - 1].chain(tween);
            }
            alphabetTweenManager.push(tween);
        }
    }


    if (calledFromTraceGame != false) {

        alphabetTweenManager[alphabetTweenManager.length - 1].onComplete.add(function() {
            dia.visible = true;
            animate_diamond(current_dia);

            if (typeof(gameHintTimer) != "undefined") {
                gameHintTimer.destroy();
            }
            gameHintTimer = game.time.create(true);
            stopHints();
            gameHintTimer.loop(5000, function() {
                traceHint(finger);
            }, this);
            gameHintTimer.start();
        }, this);

        alphabetTweenManager[0].start();

    }


}

traceHint = function(finger) {
    playLetterSound(draw_alphabet, 1);
    highlightDragEggOnly(dia, 0.05, 300, 0, 1, current_dia, 2, 1, finger);
}

drawUpdate = function(graphics, strokeWidth, strokeColor, alphabet_tween_data, index) {

    graphics.clear();
    graphics.lineStyle(strokeWidth, strokeColor);

    if (typeof(alphabet_tween_data) != "undefined") {

        graphics.moveTo(alphabet_tween_data["pointsArray"][0][0], alphabet_tween_data["pointsArray"][0][1]);

        for (var j = 0; j < alphabet_tween_data[index]; j++) {
            graphics.lineTo(alphabet_tween_data["pointsArray"][j][0], alphabet_tween_data["pointsArray"][j][1]);
        }
    }
}

drawCircle = function(graphics, strokeWidth, strokeColor, alphabet_tween_data, index) {
    graphics.clear();
    // graphics.fillStyle = "#FF0000";
    graphics.beginFill();

    // graphics.moveTo(alphabet_tween_data["pointsArray"][index][0], alphabet_tween_data["pointsArray"][index][1]);

    graphics.arc(alphabet_tween_data["pointsArray"][index][0], alphabet_tween_data["pointsArray"][index][1], 100, 0, 2 * Math.PI);
    graphics.fill();


    // for (var j = 0; j < alphabet_tween_data[index]; j++) {
    //     graphics.lineTo(alphabet_tween_data["pointsArray"][j][0], alphabet_tween_data["pointsArray"][j][1]);
    // }
}

animateAlphabet = function() {
    if (typeof(graphics_group) != "undefined") {
        for (var i = 0; i < graphics_group.children.length; i++) {
            drawUpdate(graphics_group.children[i], Game.animationStrokeWidth, Game.animationStrokeColor, alpha_tween_data[i], "dispIndex");
            // drawCircle(graphics_group.children[i], Game.animationStrokeWidth, Game.animationStrokeColor, alpha_tween_data[i], "dispIndex");


        }
    }
}

animate_diamond = function(current_dia_value) {

    Game.all_animation_stopped = 0;
    game.add.tween(dia).to({
        x: alpha_tween_data[current_dia_value]["pointsArray"][0][0],
        y: alpha_tween_data[current_dia_value]["pointsArray"][0][1]
    }, 500, Phaser.Easing.Default, true).onComplete.add(function() {
        Game.all_animation_stopped = 1;
    }, this);

}

getAlphabetPoints = function(alphabet, num, alphabetScale, inferringParameters) {

    var spriteCheck = game.make.sprite(0, 0, alphabet + '_' + num.toString());
    bmdWidth = spriteCheck.width;
    bmdHeight = spriteCheck.height;
    spriteCheck.destroy();
    bmd = game.make.bitmapData(bmdWidth, bmdHeight);
    bmd.draw(game.cache.getImage(alphabet + '_' + num.toString()), 0, 0);
    bmd.update();

    // sad = dhf;

    var allPoints = [];
    var noOfLinks = {};
    letterMat = [];

    //    console.log('step 1');

    for (var j = 0; j < bmdHeight; j++) {
        var row = [];
        for (var i = 0; i < bmdWidth; i++) {
            var hex = bmd.getPixelRGB(i, j);
            if (hex.r == 0 && hex.g == 0 && hex.b == 0) {
                row.push(0);
            } else {
                row.push(1);
            }
        }
        letterMat.push(row);
    }

    // letterMat = [];

    // for (var j = 0; j < bmdHeight; j++) {
    //     var row = [];
    //     for (var i = 0; i < bmdWidth; i++) {
    //         var hex = bmdNew.getPixelRGB(i, j);
    //         if (hex.a == 255) {
    //             row.push(1);
    //         } else {
    //             row.push(0);
    //         }
    //     }
    //     letterMat.push(row);
    // }

    // console.log('step 2 ', letterMat);

    checkInitLetterMat = letterMat;


    var top_row;
    var left_col;
    var right_col;
    var bottom_row;

    for (var i = 0; i < letterMat.length; i++) {
        var first_one_at_col;
        var last_one_at_col;
        var one_at_row;
        for (var j = 0; j < letterMat[i].length; j++) {
            if (letterMat[i][j] == 1) {
                if (typeof top_row == 'undefined') {
                    // console.log('top');
                    top_row = i;
                }

                if ((typeof first_one_at_col == 'undefined') || (first_one_at_col > j)) {
                    first_one_at_col = j;
                }

                if ((typeof last_one_at_col == 'undefined') || (last_one_at_col < j)) {
                    last_one_at_col = j;
                }

                if ((typeof one_at_row == 'undefined') || (one_at_row < i)) {
                    one_at_row = i;
                }
            }
        }

        if ((typeof left_col == 'undefined') || (left_col > first_one_at_col)) {
            left_col = first_one_at_col;
        }

        if ((typeof right_col == 'undefined') || (right_col < last_one_at_col)) {
            right_col = last_one_at_col;
        }

        if ((typeof bottom_row == 'undefined') || (bottom_row < one_at_row)) {
            bottom_row = one_at_row;
        }
    }

    //    console.log([top_row, left_col, right_col, bottom_row]);

    letterWidth = right_col - left_col;
    letterHeight = bottom_row - top_row;

    // console.log('############ ', num, letterWidth, letterHeight);
    var transposed = 0;

    // if (letterWidth > letterHeight) {
    //     // console.log('%5', num);
    //     letterMat = math.transpose(letterMat);
    //     transposed = 1;
    // }

    // console.log("inferringParameters")

    // console.log("inferringParameters[1][i] " + num + " " + inferringParameters[1][num])
    // console.log("inferringParameters[2][i] " + num + " " + inferringParameters[2][num])
    // console.log("inferringParameters[3][i] " + num + " " + inferringParameters[3][num])

    if (inferringParameters[1][num] === 1) {
        letterMat = math.transpose(letterMat);
    }



    // console.log(JSON.stringify(letterMat))
    pointsArray = getPoints(letterMat);

    if (inferringParameters[3][num] === 1) {
        pointsArray = invertAxis();
    }

    pointsArray = sortPoints(inferringParameters[4][num]);
    pointsArray = nonRedundantPoints();
    pointsArray = inverseSmooth(inferringParameters[2][num]);
    pointsArray = smoothedPath();
    // drawPath();

    var scaledPointsArray = [];

    var testScale = alphabetScale;

    if (alphabet == "nBulky") {
        var testImg = game.make.sprite(0, 0, 'nBulky_0');
        console.log('testImg ', testImg.width, testImg.height);
        testScale = {
            x: ((0.8 * h) / testImg.height),
            y: ((0.8 * h) / testImg.height)
        };
    }

    // console.log('testScale ', testScale);

    for (var i = 0; i < pointsArray.length; i++) {
        var point = pointsArray[i];
        scaledPointsArray.push([(Game.graphics_x_offset + (point[0] * testScale.x)), (Game.graphics_y_offset + (point[1] * testScale.y))]);
    }
    return scaledPointsArray;
    // return pointsArray;

}

invertAxis = function() {
    var tempArray = [];
    for (var i = 0; i < pointsArray.length; i++) {
        tempArray.push([pointsArray[i][1], pointsArray[i][0]]);
    }
    return tempArray;
}




























































































// getAlphabetData = function(alphabet, alphabetScale, animation_time, calledFromTraceGame, alphabet_data, segmentNumber) {
//     // console.log('in getAlphabetData %5', alphabet, alphabetScale, animation_time, calledFromTraceGame, alphabet_data, segmentNumber);

//     // console.log('alphabet ', alphabet);

//     alpha_tween_data = {};
//     var alphabetTweenManager = [];
//     var iStart = 0;

//     if (typeof(segmentNumber) != "undefined" && segmentNumber != null) {
//         // console.log('chaning iStart');
//         iStart = segmentNumber - 1;
//         alphabet_data = segmentNumber;
//     }

//     for (var i = iStart; i < alphabet_data; i++) {
//         // console.log('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`');
//         // var g = game.add.graphics(0, 0);
//         // graphics_group.add(g);

//         // if (calledFromTraceGame != false) {
//         //     var g_drag = game.add.graphics(0, 0);
//         //     drag_graphics_group.add(g_drag);
//         // }

//         // if (typeof(segmentNumber) != "undefined" && segmentNumber != null) {
//         //     var g_drag = game.add.graphics(0, 0);
//         //     drag_graphics_group.children.push(g_drag);
//         // }

//         // console.log('i ', i);
//         var pointsArray = [];
//         var pointsArray = getAlphabetPoints(alphabet, i, alphabetScale);

//         // console.log('pointsArray ', i, pointsArray.length, pointsArray[0].length);

//         alpha_tween_data[i] = {
//             "pointsArray": pointsArray,
//             "dragIndex": 0,
//             "dispIndex": 0
//         };

//         if (calledFromTraceGame != false) {
//             var tween = game.add.tween(alpha_tween_data[i]).to({
//                 dispIndex: alpha_tween_data[i].pointsArray.length - 1
//             }, animation_time, Phaser.Easing.Default);
//             if (alphabetTweenManager.length > 0) {
//                 alphabetTweenManager[alphabetTweenManager.length - 1].chain(tween);
//             }
//             alphabetTweenManager.push(tween);
//         }
//     }


//     if (calledFromTraceGame != false) {

//         alphabetTweenManager[alphabetTweenManager.length - 1].onComplete.add(function() {
//             dia.visible = true;
//             animate_diamond(current_dia);

//             // if (typeof(gameHintTimer) != "undefined") {
//             //     gameHintTimer.destroy();
//             // }
//             // gameHintTimer = game.time.create(true);
//             // stopHints();
//             // gameHintTimer.loop(5000, function() {
//             //     traceHint(finger);
//             // }, this);
//             // gameHintTimer.start();
//         }, this);

//         alphabetTweenManager[0].start();

//     }


// }

// traceHint = function(finger) {
//     playLetterSound(draw_alphabet, 1);
//     highlightDragEggOnly(dia, 0.05, 300, 0, 1, current_dia, 2, 1, finger);
// }

// drawUpdate = function(graphics, strokeWidth, strokeColor, alphabet_tween_data, index) {

//     graphics.clear();
//     graphics.lineStyle(strokeWidth, strokeColor);

//     if (typeof(alphabet_tween_data) != "undefined") {

//         graphics.moveTo(alphabet_tween_data["pointsArray"][0][0], alphabet_tween_data["pointsArray"][0][1]);

//         for (var j = 0; j < alphabet_tween_data[index]; j++) {
//             graphics.lineTo(alphabet_tween_data["pointsArray"][j][0], alphabet_tween_data["pointsArray"][j][1]);
//         }
//     }
// }

// drawCircle = function(graphics, strokeWidth, strokeColor, alphabet_tween_data, index) {
//     graphics.clear();
//     // graphics.fillStyle = "#FF0000";
//     graphics.beginFill();

//     // graphics.moveTo(alphabet_tween_data["pointsArray"][index][0], alphabet_tween_data["pointsArray"][index][1]);

//     graphics.arc(alphabet_tween_data["pointsArray"][index][0], alphabet_tween_data["pointsArray"][index][1], 100, 0, 2 * Math.PI);
//     graphics.fill();


//     // for (var j = 0; j < alphabet_tween_data[index]; j++) {
//     //     graphics.lineTo(alphabet_tween_data["pointsArray"][j][0], alphabet_tween_data["pointsArray"][j][1]);
//     // }
// }

// animateAlphabet = function() {
//     if (typeof(graphics_group) != "undefined") {
//         for (var i = 0; i < graphics_group.children.length; i++) {
//             drawUpdate(graphics_group.children[i], Game.animationStrokeWidth, Game.animationStrokeColor, alpha_tween_data[i], "dispIndex");
//             // drawCircle(graphics_group.children[i], Game.animationStrokeWidth, Game.animationStrokeColor, alpha_tween_data[i], "dispIndex");


//         }
//     }
// }

// animate_diamond = function(current_dia_value) {

//     Game.all_animation_stopped = 0;
//     game.add.tween(dia).to({
//         x: alpha_tween_data[current_dia_value]["pointsArray"][0][0],
//         y: alpha_tween_data[current_dia_value]["pointsArray"][0][1]
//     }, 500, Phaser.Easing.Default, true).onComplete.add(function() {
//         Game.all_animation_stopped = 1;
//     }, this);

// }

// getAlphabetPoints = function(alphabet, num, alphabetScale) {

//     var spriteCheck = game.make.sprite(0, 0, alphabet + '_' + num.toString());
//     bmdWidth = spriteCheck.width;
//     bmdHeight = spriteCheck.height;
//     spriteCheck.destroy();
//     bmd = game.make.bitmapData(bmdWidth, bmdHeight);
//     bmd.draw(game.cache.getImage(alphabet + '_' + num.toString()), 0, 0);
//     bmd.update();

//     function returnStartPoint(x, y) {
//         var minDist = 10000;
//         var minIndex;
//         for (var i = 0; i < pointsArray.length; i++) {
//             var dist = Math.sqrt((pointsArray[i][0] - x) * (pointsArray[i][0] - x) + (pointsArray[i][1] - y) * (pointsArray[i][1] - y));
//             if (dist < minDist) {
//                 minDist = dist;
//                 minIndex = i;
//             }
//         }
//         return pointsArray[minIndex];
//     }


//     function checkArrayInArray(point) {
//         for (var i = 0; i < pointsArray.length; i++) {
//             if (point[0] == pointsArray[i][0] && point[1] == pointsArray[i][1]) {
//                 return 1;
//             }
//         }
//     }

//     // function getNeighbours(point) {
//     //     var returnPoints = [];
//     //     for (var j = point[1] - 1; j <= point[1] + 1; j++) {
//     //         for (var i = point[0] - 1; i <= point[0] + 1; i++) {
//     //             if ((i == point[0] && j == point[1]) || i < 0 || i >= bmdWidth || j < 0 || j >= bmdHeight) {
//     //                 continue;
//     //             }
//     //             var hex = bmd.getPixelRGB(i, j);
//     //             if (hex.r == 0 && hex.g == 0 && hex.b == 0) {
//     //                 var check = checkArrayInArray([i, j]);
//     //                 if (check != 1) {
//     //                     pointsArray.push([i, j]);
//     //                     returnPoints.push([i, j]);
//     //                 }
//     //             }
//     //         }
//     //     }
//     //     // return returnPoints;
//     //     for (var i = 0; i < returnPoints.length; i++) {
//     //         getNeighbours(returnPoints[i]);
//     //     }
//     // }

//     function deletePointFromArray(point) {
//         for (var i = 0; i < pointsArray.length; i++) {
//             if (pointsArray[i][0] == point[0] && pointsArray[i][1] == point[1]) {
//                 pointsArray.splice(i, 1);
//                 // break;
//             }
//         }
//     }


//     function getNearestPoint() {
//         var consi = consideredPoint;
//         deletePointFromArray(consideredPoint);
//         var minDist = 10000;
//         var minIndex;
//         for (var i = 0; i < pointsArray.length; i++) {
//             var dist = Math.sqrt((pointsArray[i][0] - consi[0]) * (pointsArray[i][0] - consi[0]) + (pointsArray[i][1] - consi[1]) * (pointsArray[i][1] - consi[1]));
//             if (dist < minDist) {
//                 minDist = dist;
//                 minIndex = i;
//             }
//         }
//         if (pointsArray.length > 0) {
//             myFinalPointsArray.push(pointsArray[minIndex]);
//             consideredPoint = pointsArray[minIndex];
//             console.log('consideredPoint ', consi, 'nearest point', consideredPoint);
//         }
//     }

//     function getNeighbours() {
//         var point = pendingPoints[0];
//         // var returnPoints = [];
//         for (var j = point[1] - 1; j <= point[1] + 1; j++) {
//             for (var i = point[0] - 1; i <= point[0] + 1; i++) {
//                 if (i == point[0] && j == point[1]) {
//                     continue;
//                 }
//                 if (i >= 0 && i < cArray[0].length) {
//                     if (j >= 0 && j < cArray.length) {
//                         // var hex = bmd.getPixelRGB(i, j);
//                         var hex = cArray[j][i];
//                         // if (hex.a == 255) {
//                         // if (hex.r == 0 && hex.g == 0 && hex.b == 0) {
//                         if (hex == 1) {
//                             var check = checkArrayInArray([i, j]);
//                             if (check != 1) {
//                                 pointsArray.push([i, j]);
//                                 pendingPoints.push([i, j]);
//                                 // returnPoints.push([i, j]);
//                             }
//                         }
//                     }
//                 }
//             }
//         }

//         pendingPoints.splice(0, 1);
//     }












//     // qw = we;


//     letterMat = [];
//     pointsArray = [];
//     // qw = we;

//     for (var j = 0; j < bmdHeight; j++) {
//         var row = [];
//         for (var i = 0; i < bmdWidth; i++) {
//             var hex = bmd.getPixelRGB(i, j);
//             if (hex.r == 0 && hex.g == 0 && hex.b == 0) {
//                 // if (hex.a == 255) {
//                 console.log('got red');
//                 row.push(1);
//                 pointsArray.push([i, j]);
//             } else {
//                 row.push(0);
//             }
//         }
//         letterMat.push(row);
//     }

//     cArray = letterMat;

//     // startPoint = [112, 1];
//     // endPoint = [112, 189];


//     // startPoint = returnStartPoint(bmdWidth - 1, 0);
//     // pointsArray = [];
//     // getNeighbours(startPoint);

//     startPoint = returnStartPoint(bmdWidth - 1, 0);
//     pointsArray = [];
//     pendingPoints = [startPoint];
//     // getNeighbours(startPoint);

//     while (pendingPoints.length != 0) {
//         getNeighbours();
//     }




//     // myStartPoint = returnStartPoint(bmdWidth-1, 0);
//     // consideredPoint = myStartPoint;

//     // myFinalPointsArray = [];

//     // while (pointsArray.length > 0) {
//     //     getNearestPoint();
//     // }

//     // pointsArray = myFinalPointsArray;


//     var scaledPointsArray = [];

//     for (var i = 0; i < pointsArray.length; i++) {
//         var point = pointsArray[i];
//         scaledPointsArray.push([Math.round(Game.graphics_x_offset + (point[0] * alphabetScale.x)), Math.round(Game.graphics_y_offset + (point[1] * alphabetScale.y))]);
//     }



//     return scaledPointsArray;
//     // return pointsArray;

// }

